echo off

del .\builds\DAQ.zip
del .\builds\TDC.zip
del .\builds\PSI_installer.zip

"C:\Program Files\WinRAR\"WinRAR.exe a -afzip -r .\builds\DAQ.zip .\builds\DAQ\*.* 
"C:\Program Files\WinRAR\"WinRAR.exe a -afzip -r .\builds\TDC.zip .\builds\TDC\*.* 
"C:\Program Files\WinRAR\"WinRAR.exe a -afzip -r .\builds\PSI_installer.zip .\builds\PSI_installer\*.* 

del X:\public_html\data\PSI_Praktikum_Wiedner_7xADC_TDC\DAQ.zip
del X:\public_html\data\PSI_Praktikum_Wiedner_7xADC_TDC\TDC.zip
del X:\public_html\data\PSI_Praktikum_Wiedner_7xADC_TDC\PSI_installer.zip

copy  .\builds\DAQ.zip X:\public_html\data\PSI_Praktikum_Wiedner_7xADC_TDC\DAQ.zip
copy  .\builds\TDC.zip X:\public_html\data\PSI_Praktikum_Wiedner_7xADC_TDC\TDC.zip
copy  .\builds\PSI_installer.zip X:\public_html\data\PSI_Praktikum_Wiedner_7xADC_TDC\PSI_installer.zip