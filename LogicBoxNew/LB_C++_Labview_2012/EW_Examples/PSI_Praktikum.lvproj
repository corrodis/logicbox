﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="LogicBox" Type="Folder"/>
		<Item Name="Test_DAQ.vi" Type="VI" URL="../Test_DAQ.vi"/>
		<Item Name="Test_TDC.vi" Type="VI" URL="../Test_TDC.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
				<Item Name="NI_MAPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MAPro.lvlib"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
			</Item>
			<Item Name="daq set Command Register.vi" Type="VI" URL="../Subvis/daq set Command Register.vi"/>
			<Item Name="daq set Event Size.vi" Type="VI" URL="../Subvis/daq set Event Size.vi"/>
			<Item Name="daq set Configuration Register.vi" Type="VI" URL="../Subvis/daq set Configuration Register.vi"/>
			<Item Name="daq get All Fpga Events.vi" Type="VI" URL="../Subvis/daq get All Fpga Events.vi"/>
			<Item Name="daq get Missed Events.vi" Type="VI" URL="../Subvis/daq get Missed Events.vi"/>
			<Item Name="daq read Memory Event.vi" Type="VI" URL="../Subvis/daq read Memory Event.vi"/>
			<Item Name="daq set Event Read Order.vi" Type="VI" URL="../Subvis/daq set Event Read Order.vi"/>
			<Item Name="init ADCs.vi" Type="VI" URL="../Subvis/init ADCs.vi"/>
			<Item Name="daq get Number Events.vi" Type="VI" URL="../Subvis/daq get Number Events.vi"/>
			<Item Name="open Device Serial Number.vi" Type="VI" URL="../Subvis/open Device Serial Number.vi"/>
			<Item Name="Error Converter (ErrCode or Status).vi" Type="VI" URL="../Subvis/Error Converter (ErrCode or Status).vi"/>
			<Item Name="get Compilation Date.vi" Type="VI" URL="../Subvis/get Compilation Date.vi"/>
			<Item Name="tdc enable.vi" Type="VI" URL="../Subvis/tdc enable.vi"/>
			<Item Name="tdc set IO Mask.vi" Type="VI" URL="../Subvis/tdc set IO Mask.vi"/>
			<Item Name="tdc set termination Mask.vi" Type="VI" URL="../Subvis/tdc set termination Mask.vi"/>
			<Item Name="tdc get number events in fpga buffer.vi" Type="VI" URL="../Subvis/tdc get number events in fpga buffer.vi"/>
			<Item Name="tdc get All Fpga Events.vi" Type="VI" URL="../Subvis/tdc get All Fpga Events.vi"/>
			<Item Name="tdc read Memory Event.vi" Type="VI" URL="../Subvis/tdc read Memory Event.vi"/>
			<Item Name="tdc get read events.vi" Type="VI" URL="../Subvis/tdc get read events.vi"/>
			<Item Name="tdc get stored events.vi" Type="VI" URL="../Subvis/tdc get stored events.vi"/>
			<Item Name="tdc get IO Mask.vi" Type="VI" URL="../Subvis/tdc get IO Mask.vi"/>
			<Item Name="tdc get termination Mask.vi" Type="VI" URL="../Subvis/tdc get termination Mask.vi"/>
			<Item Name="tdc software trigger.vi" Type="VI" URL="../Subvis/tdc software trigger.vi"/>
			<Item Name="tdc reset buffer.vi" Type="VI" URL="../Subvis/tdc reset buffer.vi"/>
			<Item Name="tdc get fpga status.vi" Type="VI" URL="../Subvis/tdc get fpga status.vi"/>
			<Item Name="close Device.vi" Type="VI" URL="../Subvis/close Device.vi"/>
			<Item Name="Error Converter(ErrCheck).vi" Type="VI" URL="../Subvis/Error Converter(ErrCheck).vi"/>
			<Item Name="daq get All Fpga Events DMA.vi" Type="VI" URL="../Subvis/daq get All Fpga Events DMA.vi"/>
			<Item Name="daq set dma.vi" Type="VI" URL="../Subvis/daq set dma.vi"/>
			<Item Name="daq find best adc phase.vi" Type="VI" URL="../Subvis/daq find best adc phase.vi"/>
			<Item Name="FFT+ENOB.vi" Type="VI" URL="../Subvis/FFT+ENOB.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="Histo.vi" Type="VI" URL="../Subvis/Histo.vi"/>
			<Item Name="tdc get All Fpga Events DMA.vi" Type="VI" URL="../Subvis/tdc get All Fpga Events DMA.vi"/>
			<Item Name="tdc set dma.vi" Type="VI" URL="../Subvis/tdc set dma.vi"/>
			<Item Name="LogicBox.dll" Type="Document" URL="../../../LB_C++/Release/LogicBox.dll"/>
			<Item Name="daq set ADC Resolution Register.vi" Type="VI" URL="../Subvis/daq set ADC Resolution Register.vi"/>
			<Item Name="daq get ADC Resolution Register.vi" Type="VI" URL="../Subvis/daq get ADC Resolution Register.vi"/>
			<Item Name="daq set ADC Randomizer Reg.vi" Type="VI" URL="../Subvis/daq set ADC Randomizer Reg.vi"/>
			<Item Name="daq get Randomizer Mask.vi" Type="VI" URL="../Subvis/daq get Randomizer Mask.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="TDC" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{9E1700C6-B2B7-4A4A-A614-85F03E6442B0}</Property>
				<Property Name="App_INI_GUID" Type="Str">{5C14107E-7F51-4CEF-B1B1-37918196B9D8}</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{143C77C9-214F-4B90-A537-65DFDD7A3966}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">TDC</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/TDC</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{097C3D6A-23EB-49A5-BE8B-6E0B7B820FDA}</Property>
				<Property Name="Destination[0].destName" Type="Str">TDC.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/TDC/TDC.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/TDC/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{71D6320E-F164-4F6B-9992-61715E05D108}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Test_TDC.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">TDC</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">TDC</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 </Property>
				<Property Name="TgtF_productName" Type="Str">TDC</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{35ADEB00-AC00-4DBD-9342-69EDF6B3CF91}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">TDC.exe</Property>
			</Item>
			<Item Name="DAQ" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{9B6AF96A-52DC-4DCC-A3FC-8D5EE5A6F48B}</Property>
				<Property Name="App_INI_GUID" Type="Str">{1D5FCBB4-727F-455C-BE37-96C5288F9C02}</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{02EE2874-8647-4242-BE14-01DAB029AFED}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DAQ</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/DAQ</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToProject</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{18DC3508-1659-42A5-9DB0-3782758EBB91}</Property>
				<Property Name="Destination[0].destName" Type="Str">DAQ.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/DAQ/DAQ.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">relativeToProject</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/DAQ/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">relativeToProject</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{51F1B54D-7E1A-405E-8369-7A316B26A94D}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Test_DAQ.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_companyName" Type="Str">Physikalisches Institut Universität Heidelberg</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DAQ</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">DAQ</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 Physikalisches Institut Universität Heidelberg</Property>
				<Property Name="TgtF_productName" Type="Str">DAQ</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{BF008D91-9548-489F-8434-56AC2F3D8A6E}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DAQ.exe</Property>
			</Item>
			<Item Name="PSI_Praktikum" Type="Installer">
				<Property Name="Destination[0].name" Type="Str">PSI_Praktikum</Property>
				<Property Name="Destination[0].parent" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="Destination[0].tag" Type="Str">{95C24DFF-B5D8-46B5-825D-2D8092E1D519}</Property>
				<Property Name="Destination[0].type" Type="Str">userFolder</Property>
				<Property Name="DestinationCount" Type="Int">1</Property>
				<Property Name="INST_author" Type="Str">Physikalisches Institut Universität Heidelberg</Property>
				<Property Name="INST_autoIncrement" Type="Bool">true</Property>
				<Property Name="INST_buildLocation" Type="Path">../EW_Examples/builds/PSI_installer</Property>
				<Property Name="INST_buildLocation.type" Type="Str">relativeToCommon</Property>
				<Property Name="INST_buildSpecName" Type="Str">PSI_Praktikum</Property>
				<Property Name="INST_defaultDir" Type="Str">{95C24DFF-B5D8-46B5-825D-2D8092E1D519}</Property>
				<Property Name="INST_productName" Type="Str">PSI_Praktikum</Property>
				<Property Name="INST_productVersion" Type="Str">1.0.15</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">12008024</Property>
				<Property Name="MSI_arpCompany" Type="Str">Physikalisches Institut Universität Heidelberg</Property>
				<Property Name="MSI_distID" Type="Str">{6342789D-EB6A-48BF-8190-9AF58F271524}</Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="MSI_upgradeCode" Type="Str">{83007208-0B4D-423D-A58C-AD45CCD2CC52}</Property>
				<Property Name="RegDest[0].dirName" Type="Str">Software</Property>
				<Property Name="RegDest[0].dirTag" Type="Str">{DDFAFC8B-E728-4AC8-96DE-B920EBB97A86}</Property>
				<Property Name="RegDest[0].parentTag" Type="Str">2</Property>
				<Property Name="RegDestCount" Type="Int">1</Property>
				<Property Name="Source[0].dest" Type="Str">{95C24DFF-B5D8-46B5-825D-2D8092E1D519}</Property>
				<Property Name="Source[0].File[0].dest" Type="Str">{95C24DFF-B5D8-46B5-825D-2D8092E1D519}</Property>
				<Property Name="Source[0].File[0].name" Type="Str">TDC.exe</Property>
				<Property Name="Source[0].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[0].File[0].Shortcut[0].name" Type="Str">TDC</Property>
				<Property Name="Source[0].File[0].Shortcut[0].subDir" Type="Str">PSI_Praktikum</Property>
				<Property Name="Source[0].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[0].File[0].tag" Type="Str">{35ADEB00-AC00-4DBD-9342-69EDF6B3CF91}</Property>
				<Property Name="Source[0].FileCount" Type="Int">1</Property>
				<Property Name="Source[0].name" Type="Str">TDC</Property>
				<Property Name="Source[0].tag" Type="Ref">/My Computer/Build Specifications/TDC</Property>
				<Property Name="Source[0].type" Type="Str">EXE</Property>
				<Property Name="Source[1].dest" Type="Str">{95C24DFF-B5D8-46B5-825D-2D8092E1D519}</Property>
				<Property Name="Source[1].File[0].dest" Type="Str">{95C24DFF-B5D8-46B5-825D-2D8092E1D519}</Property>
				<Property Name="Source[1].File[0].name" Type="Str">DAQ.exe</Property>
				<Property Name="Source[1].File[0].Shortcut[0].destIndex" Type="Int">0</Property>
				<Property Name="Source[1].File[0].Shortcut[0].name" Type="Str">DAQ</Property>
				<Property Name="Source[1].File[0].Shortcut[0].subDir" Type="Str">PSI_Praktikum</Property>
				<Property Name="Source[1].File[0].ShortcutCount" Type="Int">1</Property>
				<Property Name="Source[1].File[0].tag" Type="Str">{BF008D91-9548-489F-8434-56AC2F3D8A6E}</Property>
				<Property Name="Source[1].FileCount" Type="Int">1</Property>
				<Property Name="Source[1].name" Type="Str">DAQ</Property>
				<Property Name="Source[1].tag" Type="Ref">/My Computer/Build Specifications/DAQ</Property>
				<Property Name="Source[1].type" Type="Str">EXE</Property>
				<Property Name="Source[2].dest" Type="Str">{95C24DFF-B5D8-46B5-825D-2D8092E1D519}</Property>
				<Property Name="Source[2].name" Type="Str">LogicBox.dll</Property>
				<Property Name="Source[2].tag" Type="Ref"></Property>
				<Property Name="Source[2].type" Type="Str">File</Property>
				<Property Name="Source[3].dest" Type="Str">{D2272923-C63E-4A06-9729-85B1AD1CFB2A}</Property>
				<Property Name="Source[3].name" Type="Str">LogicBox.dll</Property>
				<Property Name="Source[3].tag" Type="Ref"></Property>
				<Property Name="Source[3].type" Type="Str">File</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
			</Item>
		</Item>
	</Item>
</Project>
