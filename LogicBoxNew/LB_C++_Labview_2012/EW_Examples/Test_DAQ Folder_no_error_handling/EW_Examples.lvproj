﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Test_DAQ.vi" Type="VI" URL="../Test_DAQ.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="close Device.vi" Type="VI" URL="../../Subvis/close Device.vi"/>
			<Item Name="daq get All Fpga Events.vi" Type="VI" URL="../Subvis/daq get All Fpga Events.vi"/>
			<Item Name="daq get Missed Events.vi" Type="VI" URL="../Subvis/daq get Missed Events.vi"/>
			<Item Name="daq get Number Events.vi" Type="VI" URL="../Subvis/daq get Number Events.vi"/>
			<Item Name="daq read Memory Event.vi" Type="VI" URL="../Subvis/daq read Memory Event.vi"/>
			<Item Name="daq set Command Register.vi" Type="VI" URL="../Subvis/daq set Command Register.vi"/>
			<Item Name="daq set Configuration Register.vi" Type="VI" URL="../Subvis/daq set Configuration Register.vi"/>
			<Item Name="daq set Event Read Order.vi" Type="VI" URL="../Subvis/daq set Event Read Order.vi"/>
			<Item Name="daq set Event Size.vi" Type="VI" URL="../Subvis/daq set Event Size.vi"/>
			<Item Name="daq set Eventpresamples.vi" Type="VI" URL="../../Subvis/daq set Eventpresamples.vi"/>
			<Item Name="Error Converter (ErrCode or Status).vi" Type="VI" URL="../../Subvis/Error Converter (ErrCode or Status).vi"/>
			<Item Name="Error Converter(ErrCheck).vi" Type="VI" URL="../../Subvis/Error Converter(ErrCheck).vi"/>
			<Item Name="get Compilation Date.vi" Type="VI" URL="../../Subvis/get Compilation Date.vi"/>
			<Item Name="init ADCs.vi" Type="VI" URL="../Subvis/init ADCs.vi"/>
			<Item Name="LogicBox.dll" Type="Document" URL="../../../../LB_C++/Debug/LogicBox.dll"/>
			<Item Name="open Device Serial Number.vi" Type="VI" URL="../../Subvis/open Device Serial Number.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="DAQ" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{671F0358-0FB7-44E3-81B6-FF2247A09B72}</Property>
				<Property Name="App_INI_GUID" Type="Str">{68D05FDA-8074-43E5-92E9-960E2F7A1A34}</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F98D71CD-8734-4386-9C9B-089607E96234}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">DAQ</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/DAQ</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B9400F5E-B32F-4A4A-98D4-BE9B546BA2C4}</Property>
				<Property Name="Destination[0].destName" Type="Str">DAQ.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/DAQ/DAQ.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/DAQ/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{4FCD0652-F947-4EE2-B0D2-8BE3EA04B05F}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Test_DAQ.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">DAQ</Property>
				<Property Name="TgtF_fileVersion.major" Type="Int">1</Property>
				<Property Name="TgtF_internalName" Type="Str">DAQ</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2013 </Property>
				<Property Name="TgtF_productName" Type="Str">DAQ</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{EFA16414-ABBA-448F-8F90-209398485E29}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">DAQ.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
