::
:: File Name:	install.bat
:: Abstract:	batch file for updating the Logic Box Driver
::


@ECHO OFF

	
:: -----------------------------------------------------------------------------------
:: Check Windows version
IF "%OS%"=="Windows_NT" SETLOCAL

:: -----------------------------------------------------------------------------------
:: set some local variables
SET TOOLNAME=install.bat
SET TOOLVER=Version 1.03
SET SUCCESS_MSG=                                  ...done successfully

:: -----------------------------------------------------------------------------------
:: for non NT based operating systems show help message and quit
IF NOT "%OS%"=="Windows_NT" GOTO HelpMe

:: -----------------------------------------------------------------------------------
:: get windows version (2000, XP, Vista)
VER >C:\uyrd.dat
FOR /F "tokens=1*" %%A IN (C:\uyrd.dat) DO SET OSVER_TMP=%%A %%B
IF EXIST C:\uyrd.dat del C:\uyrd.dat

SET OSVER=%OSVER_TMP:~0,20%
SET OSEXT=
IF "%OSVER%" == "Microsoft Windows [V" SET OSEXT=_vista 
IF "%OSVER%" == "Microsoft Windows XP" SET OSEXT=_xp
IF "%OSVER%" == "Microsoft Windows 20" SET OSEXT=_w2k
IF "%OSVER%" == "Windows NT Version 4" goto HelpMe
IF "%OSEXT%" == "" goto HelpMe


:: -----------------------------------------------------------------------------------
:: Check command line arguments -- none allowed
IF NOT "%~1"=="" GOTO HelpMe

:: -----------------------------------------------------------------------------------
:: set a variable to \Windows\system32 directory
SET WINSYSDIR=%WINDIR%\System32

:: -----------------------------------------------------------------------------------
:: ask user whether he want's to proceed or not
GOTO ask_to_run


:start_tool
CLS
ECHO.
ECHO. Starting update procedure for Logic Box %TOOLVER%:
ECHO.

goto get_processor_type

:start_tool_continue_1

IF %OSEXT% == _vista goto WIN_VISTA_7
IF %OSEXT% == _xp    goto WIN_XP
IF %OSEXT% == _w2k   goto WIN_2K

:: -----------------------------------------------------------------------------------
:: copy the device drivers to the appropriate system directories

:start_tool_continue_2

ECHO.
ECHO. Copying driver files...


:: if target file exists then use xcopy to overwrite read only files
SET COPYFILETARGET="%WINSYSDIR%\DL700_FX2.dll"
IF EXIST %COPYFILETARGET% (
	"%WINSYSDIR%\xcopy" "%SRCFILES%\"DL700_FX2.dll	%COPYFILETARGET% /R /Y >NUL 2>&1
) ELSE (
	"%WINSYSDIR%\cmd" /A /C copy "%SRCFILES%\"DL700_FX2.dll	%COPYFILETARGET% >NUL 2>&1
)
if ERRORLEVEL 1 goto copy_error_dll
IF NOT EXIST %COPYFILETARGET% goto copy_error_dll
:: clear the encrypted flag
cipher.exe /d "%COPYFILETARGET%\"DL700_FX2.dll

SET COPYFILETARGET="%WINDIR%\inf\DL700_FX2.inf"
IF EXIST %COPYFILETARGET% (
	"%WINSYSDIR%\xcopy" %SRCFILES%\DL700_FX2.inf		%COPYFILETARGET% /R /Y >NUL 2>&1
) ELSE (
	"%WINSYSDIR%\cmd" /A /C copy %SRCFILES%\DL700_FX2.inf		%COPYFILETARGET% >NUL 2>&1
)
if ERRORLEVEL 1 goto copy_error_inf
IF NOT EXIST %COPYFILETARGET% goto copy_error_inf
:: clear the encrypted flag
cipher.exe /d "%COPYFILETARGET%\"DL700_FX2.inf


SET COPYFILETARGET="%WINSYSDIR%\Drivers\CyUSB.sys"
IF EXIST %COPYFILETARGET% (
	"%WINSYSDIR%\xcopy" %SRCFILES%\CyUSB.sys	%COPYFILETARGET% /R /Y >NUL 2>&1
) ELSE (
	"%WINSYSDIR%\cmd" /A /C   copy %SRCFILES%\CyUSB.sys	%COPYFILETARGET% >NUL 2>&1
)
if ERRORLEVEL 1 goto copy_error_sys
IF NOT EXIST %COPYFILETARGET% goto copy_error_sys
:: clear the encrypted flag
cipher.exe /d "%COPYFILETARGET%\"CyUSB.sys

SET COPYFILETARGET="%WINDIR%\inf\CyUSB.sys"
IF EXIST %COPYFILETARGET% (
	"%WINSYSDIR%\xcopy" %SRCFILES%\CyUSB.sys	%COPYFILETARGET% /R /Y >NUL 2>&1
) ELSE (
	"%WINSYSDIR%\cmd" /A /C   copy %SRCFILES%\CyUSB.sys	%COPYFILETARGET% >NUL 2>&1
)
if ERRORLEVEL 1 goto copy_error_sys
IF NOT EXIST %COPYFILETARGET% goto copy_error_sys
:: clear the encrypted flag
cipher.exe /d "%COPYFILETARGET%\"CyUSB.sys

ECHO.%SUCCESS_MSG%

:: if a 64-bit system copy the 32-bit dll in folder SysWOW64
IF %OSEXT% == _vista goto check_x64

goto end_tool_success

:check_x64
IF %PROTYPE%==X64 goto copy_x86
goto end_tool_success
:copy_x86
set WINSYSDIR=%WINDIR%\SysWOW64
set SRCFILES=whl\x86

:: if target file exists then use xcopy to overwrite read only files
SET COPYFILETARGET="%WINSYSDIR%\DL700_FX2.dll"
IF EXIST %COPYFILETARGET% (
	"%WINSYSDIR%\xcopy" "%SRCFILES%\"DL700_FX2.dll	%COPYFILETARGET% /R /Y >NUL 2>&1
) ELSE (
	"%WINSYSDIR%\cmd" /A /C copy "%SRCFILES%\"DL700_FX2.dll	%COPYFILETARGET% >NUL 2>&1
)
if ERRORLEVEL 1 goto copy_error_dll
IF NOT EXIST %COPYFILETARGET% goto copy_error_dll
:: clear the encrypted flag
cipher.exe /d "%COPYFILETARGET%\"DL700_FX2.dll

goto end_tool_success

:reg_error
ECHO.
ECHO. ---------------------------------------------------
ECHO.
ECHO. !!! ATTENTION !!!
ECHO.
ECHO. ERROR: This batch file requires Microsoft's REG.EXE
ECHO         utility which should be available in
ECHO.        '%WINSYSDIR%'.
ECHO.
ECHO. ---------------------------------------------------
ECHO.
goto end_tool

:findstr_error
ECHO.
ECHO. -------------------------------------------------------
ECHO.
ECHO. !!! ATTENTION !!!
ECHO.
ECHO. ERROR: This batch file requires Microsoft's FINDSTR.EXE
ECHO         utility which should be available in
ECHO.        '%WINSYSDIR%'.
ECHO.
ECHO. -------------------------------------------------------
ECHO.
goto end_tool

:copy_error
ECHO.
ECHO. ------------------------------------------------
ECHO.
ECHO. !!! ATTENTION !!!
ECHO.
ECHO. There were some errors during the copy process.
ECHO.
ECHO. ------------------------------------------------
ECHO.
IF %OSEXT% == _vista echo. To run the install under Windows Vista/7 please open a command window 
IF %OSEXT% == _vista echo. as administrator and run the install again. 
IF %OSEXT% == _vista echo. 
IF %OSEXT% == _vista echo. Do not use the command run as administrator from the context menu!!!
IF %OSEXT% == _vista echo.
IF %OSEXT% == _vista echo.
goto end_tool

:copy_error_sys
ECHO.
ECHO. ------------------------------------------------
ECHO.
ECHO. !!! ATTENTION !!!
ECHO.
ECHO. Error copying the sys file.
ECHO.
ECHO. ------------------------------------------------
ECHO.
IF %OSEXT% == _vista echo. To run the install under Windows Vista/7 please open a command window 
IF %OSEXT% == _vista echo. as administrator and run the install again. 
IF %OSEXT% == _vista echo. 
IF %OSEXT% == _vista echo. Do not use the command run as administrator from the context menu!!!
IF %OSEXT% == _vista echo.
IF %OSEXT% == _vista echo.
goto end_tool

:copy_error_dll
ECHO.
ECHO. ------------------------------------------------
ECHO.
ECHO. !!! ATTENTION !!!
ECHO.
ECHO. Error copying the dll file.
ECHO.
ECHO. ------------------------------------------------
ECHO.
IF %OSEXT% == _vista echo. To run the install under Windows Vista/7 please open a command window 
IF %OSEXT% == _vista echo. as administrator and run the install again. 
IF %OSEXT% == _vista echo. 
IF %OSEXT% == _vista echo. Do not use the command run as administrator from the context menu!!!
IF %OSEXT% == _vista echo.
IF %OSEXT% == _vista echo.
goto end_tool


:copy_error_inf
ECHO.
ECHO. ------------------------------------------------
ECHO.
ECHO. !!! ATTENTION !!!
ECHO.
ECHO. Error copying the inf file.
ECHO.
ECHO. ------------------------------------------------
ECHO.
IF %OSEXT% == _vista echo. To run the install under Windows Vista/7 please open a command window 
IF %OSEXT% == _vista echo. as administrator and run the install again. 
IF %OSEXT% == _vista echo. 
IF %OSEXT% == _vista echo. Do not use the command run as administrator from the context menu!!!
IF %OSEXT% == _vista echo.
IF %OSEXT% == _vista echo.
goto end_tool

:regrunonce_error
ECHO.
ECHO. ---------------------------------------------------
ECHO.
ECHO. !!! ATTENTION !!!
ECHO.
ECHO. There was an error when updating the RunOnce
ECHO. registry key.
ECHO.
ECHO. ---------------------------------------------------
ECHO.
goto end_tool


:end_tool_success
ECHO.
ECHO. Logic Box software successfully updated !
ECHO.

:end_tool
pause
goto quit


:HelpMe
ECHO.
ECHO. %TOOLNAME%,  %TOOLVER% for Win2000/XP/Vista/7
ECHO.
ECHO. Usage: install
ECHO.
ECHO. Notes: [1] Do not specify any command line parameters
ECHO.        [2] This batch file requires the following command line tools:
ECHO.            b) Microsoft's REG.EXE which is distributed with Windows 2000
ECHO.               and Windows XP.
ECHO.            c) Microsoft's FINDSTR command line tools which is
ECHO.               also distributed with Windows 2000 and Windows XP.
ECHO.        [3] This batch does not run under Windows NT 4.0
ECHO.
ECHO.
ECHO.Press any key to quit.
pause >NUL
Goto quit


:ask_to_run
CLS
ECHO.
ECHO. %TOOLNAME%,  %TOOLVER% for Win2000/XP/Vista/7
ECHO.
ECHO. This batch utility will update the Logic Box device drivers.
ECHO. 
ECHO. Before you start the update procedure make sure all logic Box are
ECHO. disconnected from the system and all Logic Box software is closed.
ECHO.
ECHO. Notes: [1] Do not specify any command line parameters
ECHO.        [2] This batch file requires the following command line tools:
ECHO.            b) Microsoft's REG.EXE which is distributed with all supported
ECHO.				windows vesions
ECHO.            c) Microsoft's FINDSTR command line tools which is also
ECHO.               distributed with all supported windows versions.
ECHO.        [3] This batch does not run under Windows NT 4.0
ECHO.
ECHO. We recommend to create a full backup of your system prior using
ECHO. this utility. We point out that you use this batch file entirely
ECHO. at your own risk.
ECHo.
ECHO. U - Update Logic Box software
ECHO. Q - Quit immediately without doing anything.
ECHO.
:: SET /P prompts for input and sets the variable
:: to whatever the user types
SET Choice=
SET /P Choice= Make your choice and press Enter: 
:: The syntax in the next line extracts the substring
:: starting at 0 (the beginning) and 1 character long
IF NOT '%Choice%'=='' SET Choice=%Choice:~0,1%
ECHO.
:: /I makes the IF comparison case-insensitive
IF /I '%Choice%'=='U' GOTO start_tool
IF /I '%Choice%'=='Q' GOTO quit
GOTO ask_to_run


:: Get the processor type, 32 or 64 bits
:get_processor_type
IF EXIST "%ProgramFiles(x86)%" goto W64-Bits
goto W32-Bits
:W32-Bits
echo W32-Bits
SET PROTYPE=X86
goto start_tool_continue_1
:W64-Bits
echo W64-Bits
SET PROTYPE=X64
goto start_tool_continue_1

:: Set the path of the source files
:WIN_VISTA_7
IF %PROTYPE%==X64 set SRCFILES=whl\x64
IF %PROTYPE%==X86 set SRCFILES=whl\x86
echo.
echo Windows Vista/7 detected
echo.
IF %PROTYPE%==X64 echo 64-Bits
IF %PROTYPE%==X86 echo 32-Bits
IF %PROTYPE%==X64 echo. Please, after the installation you have
IF %PROTYPE%==X64 echo. to reboot the system !!!!!!

IF %PROTYPE%==X86 goto start_tool_continue_2
bcdedit.exe -set loadoptions DISABLE_INTEGRITY_CHECKS
bcdedit.exe -set TESTSIGNING YES


goto start_tool_continue_2
:WIN_XP
echo Windows Xp detected
IF %PROTYPE%==X64 set SRCFILES=wxp\x64
IF %PROTYPE%==X86 set SRCFILES=wxp\x86
goto start_tool_continue_2
:WIN_2K
echo Windows 2000 detected
IF %PROTYPE%==X64 set SRCFILES=w2k
IF %PROTYPE%==X86 set SRCFILES=w2k
goto start_tool_continue_2

:quit
ENDLOCAL
@echo.


