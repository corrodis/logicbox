#include "CDL709_7ADC_1TTL.h"

#ifndef FILE_CSU706_H
#define FILE_CSU706_H

#include "stdint.h"
#include <stdio.h>
#include <windows.h>
#include "CCBUS.h"
#include <iostream>
#include <fstream>
using namespace std;



#define ADC_SPI 0x00000000



class CSU706  
{
public:

	// My functions
	CSU706(CCBUS* pCBus);
	~CSU706();
	
	// reset ALL ADCs! Generate a 2 us reset pulse
	int32_t resetADCs(void);

	// test pattern 0..3, 0 is normal, 1 is all 0, 2 is all 1, 3 is toggle
	int32_t setTestPattern(uint16_t nADC, uint16_t tp);

	// ADC DLL on/off, must be on for > 60 MHz and off for < 60 MHz
	int32_t setDLL(uint16_t nADC, uint16_t on);
	
	// power down mode on/off
	int32_t powerDown(uint16_t nADC, uint16_t on);
	
	// write to the ADC SPI interface block, offset addres is the ADC #
	// ADC # > 7 is reset
	int32_t usb_wadc_spi(uint16_t nADC, uint32_t confreg);
	
	CCBUS* m_pCBus;
	ofstream* m_pFile;
};

#endif FILE_CSU706_H