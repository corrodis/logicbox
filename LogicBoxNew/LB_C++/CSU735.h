#include "CDL709_7ADC_1TTL.h"

#ifndef FILE_CSU735_H
#define FILE_CSU735_H

#include "stdint.h"
#include "register_map.h"
#include <stdio.h>
#include <windows.h>
#include "CCBUS.h"
#include <iostream>
#include <fstream>
using namespace std;




// slave address of SiLabs 5338 chip
#define SLAVE_ADDR 0x70
// masks
#define LOCK_MASK  0x15
#define LOS_MASK   0x04

// debug messages on/off
#define i2c_debug 0
#define spi_debug 0

// Addresses in the FPGA design in LogicBox
#define BASE_ADDR_SPI 0x00000
#define BASE_ADDR_I2C 0x00010
// ADCs : LED and Clock control
//
// Addresses
#define BA_LED_DCM   0x00000200
#define LED1N       (BA_LED_DCM + 0)
#define LED2N       (BA_LED_DCM + 1)
#define DCM_CONF    (BA_LED_DCM + 6)
//
// Data
//
// to increment the DCM phase just
// write DCM_CNTUP to DCM_CONF
#define DCM_CNTUP   ((1 <<  7) | 1)
//
// to decrement the DCM phase just
// write DCM_CNTDN to DCM_CONF
#define DCM_CNTDN    (1 <<  7)
//
// to set the div ratio and phase just
// write CLK_DIVRx | CLK_DIVPx to DCM_CONF
#define CLK_DIVR1    (1 << 15)


// ADCs : SPI
//
// Addresses
#define BA_SPI       0x00000000
#define SPI_ADCn     BA_SPI
#define SPI_ADC_RST (BA_SPI + 8)
//
// ADCs : Test pattern checker
//
// Addresses
#define BA_EV_BUFFER 0x00010000
#define BA_TP_CHECK  BA_EV_BUFFER
// read the test pattern error counters of
//      - channel 2*n   at Bits 15.. 0 of address BA_TP_CHECK + n
//      - channel 2*n+1 at Bits 31..16 of address BA_TP_CHECK + n

#define EVBUF_CMD    (BA_EV_BUFFER+8+3)
// start test pattern check
#define CMD_START_TP (1 << 3)

#define EB_EXT_CTRL (BA_EV_BUFFER+8+4)


// ADC sample frequency defines
#define DIV_100_MHZ 0
#define DIV_50_MHZ  1
#define DIV_33_MHZ  2
#define DIV_25_MHZ  3

// ADC internal register definition
#define LTC226X_RESET_REGISTER			0x00
#define LTC226X_POWER_DOWN_REGISTER		0x01
#define LTC226X_TIMING_REGISTER			0x02
#define LTC226X_OUTPUT_MODE_REGISTER	0x03
#define LTC226X_DATA_FORMAT_REGISTER	0x04

class CSU735
{
public:

        // My functions
        CSU735(CCBUS* pCBus);
        ~CSU735();
        //void setCBus(CCBUS* pCBus);
        int32_t initClockCleaner(uint8_t div_ratio);

        /* set ADC output test pattern
        ADC_TP_OFF                      // SU706 and SU735
        ADC_TP_ALL_ZEROS        // Only supported by SU706
        ADC_TP_ALL_ONES         // Only supported by SU706
        ADC_TP_TOGGLE           // SU706 and SU735
        ADC_TP_10_01            // Only supported by SU735
        ERROR_ADC_TEST_PATERN_NOT_SUPPORTED*/
		int32_t setTestPattern(uint8_t channel, uint8_t tp);
        // reset ALL ADCs! Generate a 2 us reset pulse
        int32_t resetADCs(void);

        int32_t check_pattern(uint8_t div_ratio);
		int32_t powerDown(uint8_t nADC, uint8_t enable);
		int32_t setRandomizer(uint8_t nADC, uint8_t enable);

        // Venelin functions
		REG_DATA GetRegisterCC(uint16_t reg_nr, uint8_t div_ratio);
        int32_t  print_usage(void);
        int32_t  SPI_ADC_print_reg(char prefix, uint8_t iaddr, uint8_t dat);
        uint32_t base_addr_i2c_cbus(uint8_t channel);
        uint32_t base_addr_spi_cbus(uint8_t channel);
        int32_t  I2C_status(uint32_t base_addr);
        int32_t  I2C_ByteWrite(uint32_t base_addr, uint16_t reg_addr, uint16_t dat);
        uint8_t  I2C_ByteRead(uint32_t base_addr, uint16_t reg_addr);
        int32_t  I2C_Init_CC(uint8_t channel, uint8_t div_ratio);
        int32_t  SPI_write(uint8_t channel, uint16_t dat);
        uint16_t SPI_write_read(uint8_t channel, uint16_t datin);
        int32_t  SPI_ADC_write(uint8_t channel, uint8_t iaddr, uint8_t dat);
        int32_t  SPI_ADC_read(uint8_t channel, uint8_t iaddr);

        CCBUS* m_pCBus;
        ofstream* m_pFile;
};



#endif FILE_CSU735_H
