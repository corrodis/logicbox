// $Id: CDL709_2TTL_TDC.h 42 2013-07-05 11:10:17Z rubio $

#ifndef CDL709_2TTL_TDC_H
#define CDL709_2TTL_TDC_H



#include "data_types.h"
#include "CCBUS.h"
#include "LogicBoxDefines.h"
#include <windows.h>
#include <stdio.h>


// move later to a specific to the project .h file
#define TDC_CNF 0x00010000
#define TDC_MEM 0x00010008

const char readEngineState2Text[5][24]={  "Reset", "Idle", "Header 1", "Header 2",
                                       "sending ram data"};

const char readMuxState2Text[6][24]={  "Header 1", "Header 2", "ch0 & #Events", "ch2 & ch1",
                                       "ch4 & ch3", "ch6 & ch5"};

//------------------ Register Map ------------------//
// eru registers
#define TDC_ADDR_MISSED_EVENTS_REGISTER          0
#define TDC_ADDR_MISSED_EVENTS_FULL_REGISTER     0


// end eru registers

#define BASE_ADDR_TDC_EVENT_CONTROL 0x10000

   #define TDC_ADDR_INP_TERMINATE_REGISTER  0x00 // rw
   #define TDC_ADDR_INP_DIS_INV_REGISTER    0x01 // rw
   #define TDC_ADDR_SM_COMMAND_REGISTER     0x02 // w
      #define TDC_EVENT_CLEAR         (0x01<<1)
      #define TDC_RESET_STATE_MACHINE (0x01<<2)
   #define TDC_ADDR_SOFT_TRIGGER_REGISTER     0x03 // w
      #define TDC_SOFT_TRIGGER_MASK           (0xFF)
      #define TDC_TIMESTAMP_CLEAR     (0x01<<8)
      #define TDC_SOFT_STOP           (0x01<<0)
//    #define SOFT_START_PATT     bits 1..7
   #define TDC_ADDR_STATUS_REGISTER         0x03 // r
   #define TDC_ADDR_TOTAL_EV_STORED         0x04 // read only
   #define TDC_ADDR_TOTAL_EV_READ           0x05 // read only
   #define TDC_BASE_ADDR_TDC_EVENT_DATA     0x10008


#pragma pack(push)
#pragma pack(1)



typedef struct struct_event_buffer_tdc
{
   uint8_t nChannels; // Number of active channels 1..7, not used
   uint8_t channelsMask; // If '1' channel is activated bit 6 = channel 6, bit 0 = channel 0
   uint16_t eventOffset; // Offset to get the next PARTICLE_EVENT_TDC
   uint16_t eventSize; // Event size  = 3 * 2 + [Nchannels/2]*2
   uint16_t nEvents; // Number of events in this buffer
   uint16_t currentPosWriteEvent; // indicate the position in the buffer of the last Written event
   uint16_t currentPosReadEvent; // indicate the position in the buffer of the last Written event
   uint16_t nTotalEvents; // Number of events that could be save in this buffer
   uint8_t *writeAddress; // Address to wite the next event
   uint8_t *readAddress;  // Address to read the next event
   uint8_t *endAddress;  // Address to read the next event

   uint8_t *pData; // Pointer to the buffer where all the particle events structures are saved
} PARTICLE_EVENT_BUFFER_TDC;

typedef struct struct_particle_event_tdc
{
   uint32_t eventID;       // FPGA particle event ID   
   uint32_t timeStamp;     // FPGA particle event time stamp of the stop signal   
   uint16_t nEvents;       // Number of particke events in the FPGA buffer, including the current one
   uint16_t dataCh0;       
   uint16_t dataCh1;       
   uint16_t dataCh2;       
   uint16_t dataCh3;       
   uint16_t dataCh4;       
   uint16_t dataCh5;       
   uint16_t dataCh6;       

} PARTICLE_EVENT_TDC;
#pragma pack(pop)


#define TDC_PARTICLE_EVENT_HEADER_SIZE        sizeof(PARTICLE_EVENT_TDC)


#define TDC_PARTICLE_EVENT_FPGA_MAX_CHANNELS (7)
#define TDC_PARTICLE_EVENT_FPGA_HEADER_SIZE  (10)
#define TDC_PARTICLE_EVENT_FPGA_DEFAULT_SIZE ((4*2)) // No header!!!
#define TDC_PARTICLE_EVENT_FPGA_MAX_NUMBER   8192
#define TDC_PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE   ((TDC_PARTICLE_EVENT_FPGA_HEADER_SIZE +(TDC_PARTICLE_EVENT_FPGA_DEFAULT_SIZE*TDC_PARTICLE_EVENT_FPGA_MAX_CHANNELS) + TDC_PARTICLE_EVENT_HEADER_SIZE) * TDC_PARTICLE_EVENT_FPGA_MAX_NUMBER)

class CDL709_2TTL_TDC
{
    public:
        CDL709_2TTL_TDC(CCBUS *pCBus);
        ~CDL709_2TTL_TDC();

		int32_t setDMA(bool ena);
        int32_t triggerSystem(uint16_t channelMask);  // bit 0 : stop, bit 1..7 : start0..6, bit 8 : timesync
        int32_t enableEventbuffer(bool ena);
        int32_t resetEventbuffer(void);
        int32_t getNumberEvents(uint16_t *nEvents);
        int32_t getFPGAEngineStatus(uint16_t *nEvents, uint8_t *readEngine, uint8_t *readMux,bool *bufferFull, bool *bufferEmpty);
        int32_t getIOMask(uint16_t *disableMask, uint16_t *invertMask); // bit 0 : stop, bit 1..7 : start0..6, bit 8 : timesync
        int32_t setIOMask(uint16_t disableMask, uint16_t invertMask); // bit 0 : stop, bit 1..7 : start0..6, bit 8 : timesync
        int32_t getTerminationMask(uint16_t *termination, uint16_t *standard); // bit 0 : stop, bit 1..7 : start0..6, bit 8 : timesync
        int32_t setTerminationMask(uint16_t termination, uint16_t standard); // bit 0 : stop, bit 1..7 : start0..6, bit 8 : timesync
        int32_t getNumberEventsStored(uint32_t *nEventsStored);
        int32_t getNumberEventsRead(uint32_t *nEventsRead);
		int32_t getFpgaEventsDMA(uint16_t *FpgaEvents);
		
                
        // ------ read event functions ------ //
        // run this function in a thread
        int32_t getAllFpgaEvents();
        //Run this function in a new thread
        int32_t readMemoryEvent(uint32_t *Id,uint32_t *timeStamp, uint16_t nData, uint16_t *pData);

        int32_t getMissedEvents(uint32_t *totalCounter, uint32_t *bufferFullCounter);



        // mask with the ADCs present or turned on
        int16_t tdc_present;
        int16_t inp_termination;
        int16_t inp_disabled;
        int16_t io_inverted;

        int32_t usb_write_long(uint32_t Address, uint32_t Data);
        int32_t usb_read_long(uint32_t Address, uint32_t* Data);

    private:
      bool m_enable;
      int32_t getFpgaEvents(uint16_t Events2Read, uint16_t *FpgaEvents, bool DMA);
	  

      CCBUS *m_pCBus;
      PARTICLE_EVENT_BUFFER_TDC m_parBuffer;
      uint8_t *m_pRawData;

};

#endif // CDL709_2TTL_TDC
