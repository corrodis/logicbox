#include <iostream>
using namespace std;
#include "data_types.h"
#include "LogicBoxDefines.h"
#include "CFX2.h"
#include "CFTDI.h"
#include "CCBUS.h"
#include "CStdPhyInt.h"

#define SUBV_AVAIL 1

class CLogicBox  : public CCBUS
{
   public:
      CLogicBox();
      ~CLogicBox();

      // General funcions
      LOGIC_BOX_LIST * getDevices();
      int32_t getCompilationDate(STRUCT_DATE *compilationDate);
      int32_t getSvnID(STRUCT_SVNID *svnID);
	  int32_t getCardInfo(STRUCT_CARDINFO* cardInfo);
      int32_t openDevice(uint16_t index);
      int32_t closeDevice();
      CCBUS * getCBusHandel();

      // CBus functions
      int32_t getCBusRef(CCBUS* pCBus);
      virtual int32_t write32(uint32_t Address, uint32_t Data);
                virtual int32_t read32(uint32_t Address, uint32_t *Data);
                virtual int32_t setAddress(uint32_t Address);
      virtual int32_t read32(uint32_t Address, uint32_t nData, uint8_t *pData);
      virtual int32_t getAvailableBytes(uint32_t *nAvaBytes);
	  virtual int32_t setDMA(bool ena);
	  virtual int32_t read32DMA(uint32_t nData, uint8_t *pData);
	  virtual int32_t readBurst(uint32_t addr, uint32_t *rdata, uint32_t nwords, uint32_t word_size, uint32_t aainc);
	  virtual int32_t writeBurst(uint32_t addr, uint32_t *wdata, uint32_t nwords, uint32_t word_size, uint32_t aainc);

	  

   private:
      STRUCT_DATE m_compilationDate;
      STRUCT_SVNID m_subverID;
	  STRUCT_CARDINFO m_cardInfo;
      bool m_isLogicBoxOpened;
      CFX2  m_FX2;
      CFTDI m_FTDI;
      CStdPhyInt *m_pInterface;
      LOGIC_BOX_LIST m_List;


};
