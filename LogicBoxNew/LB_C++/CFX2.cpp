#include "CFX2.h"
#include <iostream>

uint32_t CFX2::m_isLibraryLoaded = 0;
int32_t CFX2::m_lastError = 0;
HINSTANCE CFX2::m_hDLL = NULL;

// Function pointers
LPUSBBoxCountDevices             CFX2::DLL_USBBoxCountDevices;                    
LPUSBBoxGetDeviceSerialNumber    CFX2::DLL_USBBoxGetDeviceSerialNumber;  
LPUSBBoxOpenDevice               CFX2::DLL_USBBoxOpenDevice;                        
LPUSBBoxCloseDevice              CFX2::DLL_USBBoxCloseDevice;                      
LPUSBBoxSetPortDOutputEnable     CFX2::DLL_USBBoxSetPortDOutputEnable;    
LPUSBBoxSetPortDOutputs          CFX2::DLL_USBBoxSetPortDOutputs;              
LPUSBBoxGetPortDOutputs          CFX2::DLL_USBBoxGetPortDOutputs;              
LPUSBBoxSetPortDOutputsExt       CFX2::DLL_USBBoxSetPortDOutputsExt;        
LPUSBBoxSetConfFpgaMode          CFX2::DLL_USBBoxSetConfFpgaMode;
LPUSBBoxLoadFirmware             CFX2::DLL_USBBoxLoadFirmware;
LPUSBBoxReadEprom                CFX2::DLL_USBBoxReadEprom;
LPUSBBoxWriteDevice              CFX2::DLL_USBBoxWriteDevice;
LPUSBBoxReadDevice               CFX2::DLL_USBBoxReadDevice;
LPUSBBoxNumberOfBytesAvailable   CFX2::DLL_USBBoxNumberOfBytesAvailable;


CFX2::CFX2()
{

	int32_t ret;
	m_hDevice = NULL;
	
	// Load the lirbary if necesary
    if (m_isLibraryLoaded == 0)
    {
		ret = LoadLib();
        if (!ret)
                m_isLibraryLoaded++;
        else
        {
                m_lastError = ret;
                m_isLibraryLoaded=-1;
            }
    }
    else
            m_isLibraryLoaded++;

}

CFX2::~CFX2()
{
	closeDevice();
    if (m_isLibraryLoaded>0)
    {
        m_isLibraryLoaded--;
        if (m_isLibraryLoaded == 0)
            if (m_hDLL != NULL)
				FreeLibrary(m_hDLL);    
    }
	
}


int32_t CFX2::getNumberDevices(uint32_t *nDev)
{
	//std::cout << "Yeah!" << std::endl;
	return (int32_t) DLL_USBBoxCountDevices(nDev);
	
	
}
int32_t CFX2::getSerialNumber(uint32_t nDevice, int8_t *pSerial)
{
   int8_t *pTemp;   
   pTemp = (int8_t*)DLL_USBBoxGetDeviceSerialNumber(nDevice);
   memcpy(pSerial,pTemp,11);

   return LB_SUCCESS;

}

int32_t CFX2::openDevice(int32_t index)
{
   int32_t ret = 0;

   // Open the first LogicBox
   ret = DLL_USBBoxOpenDevice(index,&m_hDevice);

   if (ret)
      ret = LB_CANNOT_OPEN_DEVICE;

   return ret;
}

bool CFX2::isOpen()
{
   if (m_hDevice == NULL)
      return false;
   else
      return true;
}


int32_t CFX2::closeDevice()
{
	int32_t ret;
    if (m_hDevice != NULL)
	{
        ret = DLL_USBBoxCloseDevice(m_hDevice);
		if (!ret)
			m_hDevice = NULL;
	}
	else
        return LB_NO_DEVICE_OPEN;
    
	return ret;
}

int32_t CFX2::nBytesAvailable(uint32_t* nBytes)
{
    if (m_hDevice != NULL)
		return DLL_USBBoxNumberOfBytesAvailable(m_hDevice, nBytes);
	else
		return LB_NO_DEVICE_OPEN;
}

int32_t CFX2::write_bytes(uint32_t nBytes, void * buffer )
{
	if (m_hDevice != NULL)
		return DLL_USBBoxWriteDevice(m_hDevice,nBytes,buffer);
	else
		return LB_NO_DEVICE_OPEN;
}

int32_t CFX2::read_bytes(uint32_t nBytes, void * buffer )
{
	int32_t ret;
	if (m_hDevice != NULL)
		ret = DLL_USBBoxReadDevice(m_hDevice,nBytes,buffer);
	else
		return LB_NO_DEVICE_OPEN;

	return ret;
}


int32_t CFX2::LoadLib()
{
	

    INT  ReturnVal = 0;

	m_hDLL = LoadLibrary(L"DL700_FX2.dll");

    if (m_hDLL == NULL){
		std::cout << "Lib not loaded" << std::endl; 
        return LB_DLL_ERROR_DLL_NOT_FOUND;
	}

    DLL_USBBoxCountDevices              = (LPUSBBoxCountDevices)GetProcAddress(m_hDLL, "USBBoxCountDevices");
    DLL_USBBoxGetDeviceSerialNumber     = (LPUSBBoxGetDeviceSerialNumber)GetProcAddress(m_hDLL, "USBBoxGetDeviceSerialNumber");
    DLL_USBBoxOpenDevice                = (LPUSBBoxOpenDevice)GetProcAddress(m_hDLL, "USBBoxOpenDevice");
    DLL_USBBoxCloseDevice               = (LPUSBBoxCloseDevice)GetProcAddress(m_hDLL, "USBBoxCloseDevice");
    DLL_USBBoxSetPortDOutputEnable      = (LPUSBBoxSetPortDOutputEnable)GetProcAddress(m_hDLL, "USBBoxSetPortDOutputEnable");
    DLL_USBBoxSetPortDOutputs           = (LPUSBBoxSetPortDOutputs)GetProcAddress(m_hDLL, "USBBoxSetPortDOutputs");
    DLL_USBBoxGetPortDOutputs           = (LPUSBBoxGetPortDOutputs)GetProcAddress(m_hDLL, "USBBoxGetPortDOutputs");
    DLL_USBBoxSetPortDOutputsExt        = (LPUSBBoxSetPortDOutputsExt) GetProcAddress(m_hDLL, "USBBoxSetPortDOutputsExt");
    DLL_USBBoxSetConfFpgaMode           = (LPUSBBoxSetConfFpgaMode) GetProcAddress(m_hDLL, "USBBoxSetConfFpgaMode");
    DLL_USBBoxLoadFirmware              = (LPUSBBoxLoadFirmware) GetProcAddress(m_hDLL, "USBBoxLoadFirmware");
    DLL_USBBoxReadEprom                 = (LPUSBBoxReadEprom) GetProcAddress(m_hDLL, "USBBoxReadEprom");
    DLL_USBBoxWriteDevice               = (LPUSBBoxWriteDevice) GetProcAddress(m_hDLL, "USBBoxWriteDevice");
    DLL_USBBoxReadDevice                = (LPUSBBoxReadDevice) GetProcAddress(m_hDLL, "USBBoxReadDevice");
    DLL_USBBoxNumberOfBytesAvailable    = (LPUSBBoxNumberOfBytesAvailable) GetProcAddress(m_hDLL, "USBBoxNumberOfBytesAvailable");

    if (
            (!DLL_USBBoxCountDevices                )||
            (!DLL_USBBoxGetDeviceSerialNumber       )||
            (!DLL_USBBoxOpenDevice                  )||
            (!DLL_USBBoxCloseDevice                 )||
            (!DLL_USBBoxSetPortDOutputEnable        )||
            (!DLL_USBBoxSetPortDOutputs             )||
            (!DLL_USBBoxSetPortDOutputsExt          )||
            (!DLL_USBBoxGetPortDOutputs             )||
            (!DLL_USBBoxSetConfFpgaMode             )||
            (!DLL_USBBoxLoadFirmware                )||
            (!DLL_USBBoxReadEprom                   )||
            (!DLL_USBBoxWriteDevice                 )||
            (!DLL_USBBoxReadDevice                  )||
            (!DLL_USBBoxNumberOfBytesAvailable  )

            )
    {
            // handle the error
            FreeLibrary(m_hDLL);

            return LB_DLL_ERROR_LOADING_FUNCTION;
    }

    return 0;
	
	
}