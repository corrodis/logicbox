#include "CDL709_7ADC_1TTL.h"

CDL709_7ADC_1TTL::CDL709_7ADC_1TTL(CCBUS *pCBus, uint16_t DesignId, uint8_t revision)
{

   m_pSU735 = NULL;
   m_pSU706 = NULL;

   m_su  = 0;
   m_pCBus = pCBus;


   // try to allocate the memory for the particle event
   m_parBuffer.nChannels = 7;
   m_parBuffer.channelsMask = 0x7F;
   m_parBuffer.eventSize  = PARTICLE_EVENT_FPGA_DEFAULT_SIZE * m_parBuffer.nChannels +
                             PARTICLE_EVENT_FPGA_HEADER_SIZE; // 16 Samples x 2 bytes
    m_parBuffer.oneEventSize = 16;
   m_parBuffer.eventOffset  = m_parBuffer.eventSize + sizeof(PARTICLE_EVENT);

   m_parBuffer.nTotalEvents = PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE / m_parBuffer.eventOffset;

   m_parBuffer.nEvents = 0;
   m_parBuffer.currentPosWriteEvent = 0;
   m_parBuffer.currentPosReadEvent = 0;
   m_parBuffer.lostEvents = 0;
   m_parBuffer.pData = (uint8_t*) malloc(PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);
   m_pRawData = (uint8_t*) malloc(m_parBuffer.eventSize*m_parBuffer.nTotalEvents);

   m_parBuffer.writeAddress = m_parBuffer.pData ;
   m_parBuffer.readAddress = m_parBuffer.pData ;
   m_parBuffer.endAddress = (m_parBuffer.pData + PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);

   m_pFile = new ofstream("out_dcm.txt");

   // Check the version of the ADC, do i Have a SU735 or SU706 hardware
   // rubio: change to SU735 or SU706

        if ((DesignId == 1) && (revision == 0))
                m_su = SU706;
        else if ((DesignId == 1) && (revision == 1))
                m_su = SU735;

        if (m_su == SU735)
                m_pSU735 = new CSU735(m_pCBus);
        else if (m_su == SU706)
                m_pSU706 = new CSU706(m_pCBus);

        usb_adc_init(ADR_RORDER_FULL, 1);


}
CDL709_7ADC_1TTL::~CDL709_7ADC_1TTL()
{

        if (m_pSU735)
                delete m_pSU735;

        if (m_pSU706)
                delete m_pSU706;

        if (m_pFile->is_open())
                m_pFile->close();

        if (m_pFile != NULL)
                delete m_pFile;
}


int32_t CDL709_7ADC_1TTL::usb_write_long(uint32_t Address, uint32_t Data)
{
   return m_pCBus->write32(Address,Data);
}

int32_t CDL709_7ADC_1TTL::usb_read_long(uint32_t Address, uint32_t* Data)
{

   return m_pCBus->read32(Address,Data);
}





// measure the CLKOUT from the ADCs in the FPGA synchron with the system clock
// the transition from 0 to 127 is the best region to sample the ADC data
int32_t CDL709_7ADC_1TTL::usb_adc_DCM_scan(uint16_t dll_on)
{
    uint32_t L, led1, led2, adc;
    int32_t i, err;

    err = 0;
    led1 = 0xAA;
    led2 = 0x55;

    for (i=0; i<7; i++)
        if ((adc_present >> i) & 1)
                        err += m_pSU706->setDLL(i,dll_on); // dll
    // DCM reset
    #ifdef debug
    printf("reset DCM\n");
    #endif
    err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, 1+128);
    Sleep(100);

    for (i=-255; i<256; i++)
    {

        // clear & start test pattern
        err += m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_COMMAND_REGISTER, START_TESTPATERN);
        Sleep(1);

        // read the CLKOUT counters
        err += m_pCBus->read32(ADC_CLK, &L);
                #ifdef debug
        printf("%3d %3d %3d %3d %3d",i, L & 0xFF, (L >> 8) & 0xFF, (L >> 16) & 0xFF, (L >> 24) & 0xFF);
                #endif

        err += m_pCBus->read32(ADC_CLK+1, &L);
                #ifdef debug
        printf(" %3d %3d %3d %3d",L & 0xFF, (L >> 8) & 0xFF, (L >> 16) & 0xFF, (L >> 24) & 0xFF);
                #endif

        err += m_pCBus->read32(ADC_LED_DCM_CTRL+6, &L);
                #ifdef debug
        printf(" 0x%02x",L & 0xFF);
                #endif

        // read the error pattern counters
        for (adc=0; adc<7; adc++)
        {
            m_pCBus->read32(ADC_MEM+adc, &L);
                        #ifdef debug
            printf(" %10d",L);
                        #endif
        }
                #ifdef debug
        printf("\n");
                #endif

        // UP
        err += m_pCBus->write32(ADC_LED_DCM_CTRL+6,  2+128);
        // toglle LEDs
        err += m_pCBus->write32(ADC_LED_DCM_CTRL,   led1);
        err += m_pCBus->write32(ADC_LED_DCM_CTRL+1, led2);
        // reset the sync
        err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, (1 << 15) | (div_phase << 10) | (div_ratio << 8));
        err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, (1 << 15) | (div_phase << 10) | (div_ratio << 8));
        led1 = ~led1;
        led2 = ~led2;
//        Sleep(10);
    }
    return err;
}


// turn on and configure the adc_present
int32_t CDL709_7ADC_1TTL::usb_adc_init(uint32_t new_adc_rorder,
                                   uint16_t new_adc_dll_on,
                                   uint16_t new_adc_tp,
                                   int16_t new_dcm_phase,
                                   uint16_t new_div_ratio,
                                   uint16_t new_div_phase)
{
    int16_t i, err;
    uint32_t L, mask3b;

    if (new_div_ratio>4) new_div_ratio=4;
    if (new_div_ratio<1) new_div_ratio=1;
    adc_tp = new_adc_tp & 3;
    div_ratio = new_div_ratio-1;
    div_phase = new_div_phase & 0x3;
    err = 0;
    adc_present = 0;
    adc_rorder = new_adc_rorder;
    mask3b = 0;

        // Set the read order
    for (i=0; i<7; i++)
    {
        if ((new_adc_rorder & 0x7) < 7)
            adc_present |= (1 << (new_adc_rorder & 0x7));
        else
            mask3b = 7;
        new_adc_rorder = new_adc_rorder >> 3;
        adc_rorder = adc_rorder | (mask3b << (3*i) );
    }
    err += m_pCBus->write32(EB_RD_ORDER, adc_rorder);
    adc_dll_on  = new_adc_dll_on & 1;

    // Reset the ADCs
    if (m_su == SU706)
            err += m_pSU706->resetADCs();

   // use real adc data and not the internally (in LBox FPGA) generated
    //m_pCBus->write32(EB_EXT_CTRL, 0);
	uint8_t tempConfRegister;
	this->getConfigurationRegister(&tempConfRegister);
	this->setConfigurationRegister(0);

    // Set the new frequency
    err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, (1 << 15) | (div_phase << 10) | (div_ratio << 8));

    // DCM reset
    #ifdef debug
    printf("reset DCM\n");
    #endif
    err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, 1);
    #ifdef debug
    printf("Adjust DCM phase to %d...\n", new_dcm_phase);
    #endif

    // Shift the DCM phase
    for (dcm_phase = -255; dcm_phase < new_dcm_phase; dcm_phase++)
    {
        i = 0;
        do
        {
            // read back status
            err += m_pCBus->read32(ADC_LED_DCM_CTRL+6, &L);
            i++;
        } while ((((L >> 4) & 1) == 0) && (i < 10));
        err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, 2+128); // INC
        #ifdef debug
        printf("dcm adjust %4d 0x%02x %2d\n",dcm_phase, L, i);
        #endif
    }

    // set the ADC mask, turn the clocks on
    #ifdef debug
    printf("Set ADC mask to 0x%02x\n", adc_present);
    #endif
    err += m_pCBus->write32(ADC_LED_DCM_CTRL+7, adc_present);

    for (i=0; i<7; i++)
    {
        if (m_su == SU706)
        {
            if ( (adc_present >> i) & 1)
            {
                err += m_pSU706->powerDown(i,0); // power down off
                err += m_pSU706->setDLL(i,adc_dll_on); // set the dll
                err += m_pSU706->setTestPattern(i,adc_tp);      // set the test pattern mode
            }
            else
                err += m_pSU706->powerDown(i,1); // power down on
        }
        else if (m_su == SU735)
        {
			 if ( (adc_present >> i) & 1)
            {
                err += m_pSU735->powerDown(i,0); // power down off
                err += m_pSU735->setTestPattern(i,adc_tp);      // set the test pattern mode
            }
            else
                err += m_pSU735->powerDown(i,1); // power down on
        }
    }

	this->setConfigurationRegister(tempConfRegister);

    return err;
}

int32_t CDL709_7ADC_1TTL::usb_adc_init(uint32_t new_adc_rorder,
                                   uint16_t new_adc_tp,
                                   uint16_t new_div_ratio)
{
    uint16_t new_adc_dll_on;
    int16_t new_dcm_phase;
    uint16_t new_div_phase;

    new_div_ratio = ((new_div_ratio-1) & 3) + 1;

    switch (new_div_ratio)
    {
                case 1:
                {
                        new_adc_dll_on = 1;
                        new_div_phase  = 0;
                        new_dcm_phase  = -139;
                        break;
                }
                case 2:
                {
                        new_adc_dll_on = 1;
                        // working combinations
                        // div_phase    dcm_phase
                        // 0            any
                        // 1            50
                        new_div_phase  = 0;
                        new_dcm_phase  = 139;
                        break;
                }
                case 3:
                {
                        new_adc_dll_on = 1;
                        // working combinations
                        // div_phase    dcm_phase
                        // 0            any
                        // 1            25
                        // 2            -100
                        new_div_phase  =  0;
                        new_dcm_phase  = 74;
                        break;
                }
                case 4:
                {
                        new_adc_dll_on = 1;
                        // working combinations
                        // div_phase    dcm_phase
                        // 0            0
                        // 1            any
                        // 2            any
                        // 3            30
                        new_div_phase  =  1;
                        new_dcm_phase  = -69;
                        break;
                }
    }

    return usb_adc_init(new_adc_rorder, new_adc_dll_on, new_adc_tp, new_dcm_phase, new_div_ratio, new_div_phase);

}

int32_t CDL709_7ADC_1TTL::usb_adc_init(uint32_t new_adc_rorder,
                                   uint16_t new_div_ratio)
{
        int32_t ret;


        // For SU735
        if (m_su == SU735)
        {

                // first set the clock and then init PLL for the frequency
                ret = usb_adc_init(new_adc_rorder, ADC_TP_OFF, new_div_ratio);

                // Init the clock cleaner
                for (uint8_t ch=0; ch<7; ch++)
                        m_pSU735->I2C_Init_CC(ch, new_div_ratio-1);

                return ret;
                //return m_pSU735->check_pattern(new_div_ratio-1);

                //return findBestADCClockPhase(ADC_TP_10_01, new_div_ratio);
        }
        else
        {
        return usb_adc_init(new_adc_rorder, ADC_TP_OFF, new_div_ratio);

        }
}

/*
TIMESTAMP_CLEAR
EVENT_CLEAR
SOFT_TRIGGER
START_TESTPATERN
RESET_STATE_MACHINE
*/
int32_t CDL709_7ADC_1TTL::setCommandRegister(uint8_t value)
{
    int32_t err;

     err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_COMMAND_REGISTER, value);

    return err;
}


// Get the number of events in the FPGA buffer
int32_t CDL709_7ADC_1TTL::getNumberEvents(uint16_t *nEvents)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);

   *nEvents = (reg&0x3FF);

   return err;
}

int32_t CDL709_7ADC_1TTL::getNumberEventsForReading(uint8_t *nEvents)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);

   *nEvents = ((reg>>24)&0xFF);

   return err;
}

int32_t CDL709_7ADC_1TTL::getNumberEventsForWriting(uint8_t *nEvents)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);

   *nEvents = ((reg>>16)&0xFF);

   return err;
}

int32_t CDL709_7ADC_1TTL::getReadStateMachine(uint8_t *state)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);

   *state = ((reg>>13)&0x7);


   return err;


}

int32_t CDL709_7ADC_1TTL::getADCResolutionRegister(uint8_t *resReg, uint8_t *shiftReg)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, &reg);
   
   *resReg = (reg>>8) & 0x7F;
   *shiftReg = (reg>>16)& 0x7F;

   return err;
}
int32_t CDL709_7ADC_1TTL::setADCResolutionRegister(uint8_t resReg, uint8_t shiftReg)
{

   int32_t err;
   uint32_t u32temp;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, &u32temp);

   //TODO: check the unput value
   
   u32temp &= ~0xFFFF00;
   u32temp |= (((uint32_t)resReg)<<8)|(((uint32_t)shiftReg)<<16);

   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, u32temp);


   return err;
}

/*
      #define INVERT_TRIGGER     (0x01<<0)
      #define INVERT_TIME_SYNC   (0x01<<1)
      #define INVERT_BUSY        (0x01<<2)
	  #define DISABLE_TRIGGER    (0x01<<3)
	  #define DISABLE_TIME_SYNC  (0x01<<4)
      #define ENABLE_ADC_PATTERN (0x01<<5)
*/

int32_t CDL709_7ADC_1TTL::getConfigurationRegister(uint8_t *value)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, &reg);

   *value = ((reg)&0x3F);

   return err;
}

/*
      #define INVERT_TRIGGER     (0x01<<0)
      #define INVERT_TIME_SYNC   (0x01<<1)
      #define INVERT_BUSY        (0x01<<2)
	  #define DISABLE_TRIGGER    (0x01<<3)
	  #define DISABLE_TIME_SYNC  (0x01<<4)
      #define ENABLE_ADC_PATTERN (0x01<<5)
*/
int32_t CDL709_7ADC_1TTL::setConfigurationRegister(uint8_t value)
{

   int32_t err;
   uint32_t u32temp;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, &u32temp);

   //TODO: check the unput value
   value &= 0x3F;
   u32temp &= ~0x3F;
   u32temp |= value;

   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, u32temp);


   return err;
}

/*
      #define SAMPLES_16      0
      #define SAMPLES_32      1
      #define SAMPLES_64      2
      #define SAMPLES_128     3
      #define SAMPLES_256     4
      #define SAMPLES_512     5
      #define SAMPLES_1024    6
      #define SAMPLES_2048    7
*/
int32_t CDL709_7ADC_1TTL::setEventSize(uint8_t value)
{

   int32_t err;
   //TODO: check the input value

   if (value > 7)
      return LB_ERROR;


   // Set the new buffer parameters
   m_parBuffer.eventSize  = EventSizeConversion[value]* 2 * m_parBuffer.nChannels +
                             PARTICLE_EVENT_FPGA_HEADER_SIZE; // 16 Samples x 2 bytes
   m_parBuffer.oneEventSize = EventSizeConversion[value];

   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_EVENT_SIZE_REGISTER, value);
   updateReadBuffer();

   return err;
}

void CDL709_7ADC_1TTL::updateReadBuffer(void)
{


   //Clear the FPGA buffer and reset the state machine
   setCommandRegister(EVENT_CLEAR | RESET_STATE_MACHINE);

   // Set the new buffer parameters

   m_parBuffer.eventOffset  = m_parBuffer.eventSize + sizeof(PARTICLE_EVENT);

   m_parBuffer.nTotalEvents = PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE / m_parBuffer.eventOffset;
   m_parBuffer.nEvents = 0;
   m_parBuffer.currentPosWriteEvent = 0;
   m_parBuffer.currentPosReadEvent = 0;
   m_parBuffer.lostEvents = 0;
   m_parBuffer.writeAddress = m_parBuffer.pData ;
   m_parBuffer.readAddress = m_parBuffer.pData ;
   m_parBuffer.endAddress = (m_parBuffer.pData + PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);
}

int32_t CDL709_7ADC_1TTL::getEventSize(uint8_t *value)
{

   int32_t err;
   //TODO: check the input value
   uint32_t u32Temp;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_EVENT_SIZE_REGISTER, &u32Temp);
   *value = (u32Temp&0x07);

   return err;
}

int32_t CDL709_7ADC_1TTL::setEventpresamples(uint16_t value)
{

   int32_t err;
   //TODO: check the input value
   value &= 0x3FF;
   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_PRESAMPLES_REGISTER, value);


   return err;
}


int32_t CDL709_7ADC_1TTL::disableHWTrigger(bool disable)
{
   int32_t err;
   uint8_t value;


   err = getConfigurationRegister(&value);
   if (err)
      return err;
        #ifdef debug
        printf("Read 0x%02x\n",value); // ? VA
        #endif
   if (disable)
      value |= 0x08;
   else
      value &= (~0x08);

   err = setConfigurationRegister(value);
   if (err)
      return err;

   return err;


}
int32_t CDL709_7ADC_1TTL::setEventReadOrder(uint32_t value)
{

   int32_t err;
   uint32_t u32temp;

   //check the input value
   if (value>0xFFFFFF)
      return LB_ERROR;

   u32temp = value;
   m_parBuffer.nChannels = 0;
   m_parBuffer.channelsMask = 0;

   // Disable first all channels
   for(uint8_t i=0;i<7;i++)
   {
	   	if (m_su == SU706)
			m_pSU706->powerDown(i,1);
		else if (m_su == SU735)
			m_pSU735->powerDown(i,1);
   }

   
   for(uint8_t i=0;i<7;i++)
   {
      if ((u32temp & 0x07) != STOP)
      {
         m_parBuffer.nChannels++;
         m_parBuffer.channelsMask |= (0x01<<(u32temp & 0x07));
		 
		 if (m_su == SU706)
			m_pSU706->powerDown((u32temp & 0x07),0);
	     else if (m_su == SU735)
			m_pSU735->powerDown((u32temp & 0x07),0);

         u32temp >>=3;
		 
      }
      else
         break;
   }

   // Stop the adquisition to set the new buffer size
   // save the user confuguration
   uint8_t confRegVal;
   err = getConfigurationRegister(&confRegVal);
   err = disableHWTrigger(true);
   if (err)
      return err;

   // Set the new buffer parameters
   m_parBuffer.eventSize  = m_parBuffer.oneEventSize *2 *  m_parBuffer.nChannels +
                             PARTICLE_EVENT_FPGA_HEADER_SIZE; // 16 Samples x 2 bytes

   updateReadBuffer();
   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_READ_ORDER_REGISTER, value);

   // restore the configuration regiser
   err = setConfigurationRegister(confRegVal);

   return err;
}

int32_t CDL709_7ADC_1TTL::getEventReadOrder(uint32_t *value)
{

   int32_t err;
   //TODO: check the input value
   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_READ_ORDER_REGISTER, value);
   (*value) &= 0x1FFFFF;

   return err;
}


int32_t CDL709_7ADC_1TTL::getMissedEvents(uint32_t *totalCounter, uint32_t *bufferFullCounter)
{

   int32_t err;

   if((totalCounter ==NULL) || (bufferFullCounter == NULL))
      return LB_ERROR;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_MISSED_EVENTS_REGISTER, totalCounter);
   if (err)
      return err;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_MISSED_EVENTS_FULL_REGISTER, bufferFullCounter);

   return err;
}



int32_t CDL709_7ADC_1TTL::getAllFpgaEvents()
{

   uint16_t nEvents,FpgaEvents;
   getNumberEvents(&nEvents);
   if (nEvents>0)
   {
    return  getFpgaEvents(nEvents, &FpgaEvents, false);
     if (FpgaEvents>0)
		 getFpgaEvents(FpgaEvents, &FpgaEvents, false);

	 
   }
  

   return LB_SUCCESS;
}


//nData length of pData in 16-Bits words
int32_t CDL709_7ADC_1TTL::readMemoryEvent( uint32_t *Id,uint32_t *timeStamp, uint16_t nData, uint16_t *pData)
{


   if ((nData*2) != (m_parBuffer.eventSize - PARTICLE_EVENT_FPGA_HEADER_SIZE))
      return LB_ERROR;
   if (m_parBuffer.nEvents == 0)
      return LB_ERROR;



   PARTICLE_EVENT *pEvent;
   pEvent = (PARTICLE_EVENT*) m_parBuffer.readAddress;

   *Id = pEvent->eventCounter;
   *timeStamp = pEvent->timeStamp;
   memcpy(pData,pEvent->pData, m_parBuffer.eventSize-PARTICLE_EVENT_FPGA_HEADER_SIZE);


   if(m_parBuffer.currentPosReadEvent >= (m_parBuffer.nTotalEvents-1))
   {
      m_parBuffer.currentPosReadEvent = 0;
      m_parBuffer.readAddress = m_parBuffer.pData;
   }
   else
   {
      m_parBuffer.currentPosReadEvent++;
      m_parBuffer.readAddress += m_parBuffer.eventOffset;

   }

   m_parBuffer.nEvents--;

   return LB_SUCCESS;

}

int32_t CDL709_7ADC_1TTL::getFpgaEvents(uint16_t Events2Read, uint16_t *FpgaEvents, bool DMA)
{


   int32_t err = 0;




   PARTICLE_EVENT *pEvent;
   *FpgaEvents = 0;


   if (m_parBuffer.pData == NULL)
      return LB_CANNOT_ALLOC_MEMORY;

   //check if the buffer is full
   // TODO: Mirar como ver si se van a perder eventos del hardware
   if ((m_parBuffer.nEvents + Events2Read) >= m_parBuffer.nTotalEvents)
   {
      if  ((m_parBuffer.nEvents ) >= m_parBuffer.nTotalEvents)
         return LB_ERROR;
      else
         Events2Read = (m_parBuffer.nTotalEvents - m_parBuffer.nEvents);
   }

   // If end of buffer go to first memory address
   if (m_parBuffer.currentPosWriteEvent + Events2Read > m_parBuffer.nTotalEvents)
   {
      if (Events2Read>1)
         Events2Read =  m_parBuffer.nTotalEvents - m_parBuffer.currentPosWriteEvent;
      else
         m_parBuffer.writeAddress = m_parBuffer.pData;
   }


   if (DMA)
        err = m_pCBus->read32DMA((m_parBuffer.eventSize * Events2Read),m_pRawData);
   else
        err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_DATA, (m_parBuffer.eventSize * Events2Read),m_pRawData);

   if(err)
      return LB_ERROR;


   uint8_t *pData;
   pData = m_pRawData;

   for(uint16_t i = 0; i < Events2Read ; i++)
   {

      pEvent = (PARTICLE_EVENT*) m_parBuffer.writeAddress;
      pEvent->pData = (uint16_t*)(&pEvent->pData) + sizeof((pEvent->pData));

      pEvent->eventCounter = (pData[0]<<21) | (pData[1]<<13) | (pData[2]<<5) | (pData[3]>>3);
      pEvent->eventSize = EventSizeConversion[(pData[3]&0x07)];
      pEvent->timeStamp = (pData[4]<<24) | (pData[5]<<16) | (pData[6]<<8) | (pData[7]);
      pEvent->nEvents = ((pData[10]<<8)|(pData[11]))&0x3FF;
      pEvent->readOrder = (pData[8]<<13) | (pData[9]<<5) | (pData[10]>>3);
      //TODO: check the format of the data
      memcpy(pEvent->pData, &pData[12], m_parBuffer.eventSize - PARTICLE_EVENT_FPGA_HEADER_SIZE);



      // reorder the adc data
      uint16_t adc0, adc1, j;
      uint8_t *pByte;
      pByte = (uint8_t*)pEvent->pData;

      for(j = 0; j < m_parBuffer.eventSize-PARTICLE_EVENT_FPGA_HEADER_SIZE; j+=4)
      {
            adc1 = pByte[j];
            adc1 = (adc1 << 8) | pByte[j+1];
            adc0 = pByte[j+2];
            adc0 = (adc0 << 8) | pByte[j+3];

            pByte[j  ] =  adc0       & 0xFF;
            pByte[j+1] = (adc0 >> 8) & 0xFF;

            pByte[j+2] =  adc1       & 0xFF;
            pByte[j+3] = (adc1 >> 8) & 0xFF;

      }


      if (m_parBuffer.currentPosWriteEvent == m_parBuffer.nTotalEvents-1)
      {
         m_parBuffer.currentPosWriteEvent = 0;
         // Update the write pointer
         m_parBuffer.writeAddress = (uint8_t*)m_parBuffer.pData;
      }
      else
      {
         m_parBuffer.currentPosWriteEvent++;
         // Update the write pointer
         m_parBuffer.writeAddress += m_parBuffer.eventOffset;
      }

      // Atomic operation????
      m_parBuffer.nEvents++;

      // Next event in the buffer
      pData += m_parBuffer.eventSize;

   }

    //Number of events in the FPGA
    *FpgaEvents = pEvent->nEvents-1;


   return Events2Read;
}




int32_t CDL709_7ADC_1TTL::getFpgaEventsDMA(uint16_t *FpgaEvents)
{

   int32_t err;
   uint16_t Events2Read;
   uint32_t Bytes2Read;

   // get the number of available bytes in the FPGA buffer
   //err = m_pCBus->getAvailableBytes(&Bytes2Read);

   // calculate the event number in the buffer
   Events2Read = 10;//Bytes2Read / m_parBuffer.eventSize;


   // read it
   if (Events2Read > 1)
        err = getFpgaEvents(Events2Read, FpgaEvents, TRUE);
   else
        *FpgaEvents = 0;

   return err;



}

int32_t CDL709_7ADC_1TTL::setDMA(bool ena)
{

        return m_pCBus->setDMA(ena);
}

/* set ADC output test pattern
        new_adc_tp:
          ADC_TP_OFF                    // SU706 and SU735
          ADC_TP_ALL_ZEROS      // Only supported by SU706
          ADC_TP_ALL_ONES               // Only supported by SU706
          ADC_TP_TOGGLE         // SU706 and SU735
          ADC_TP_10_01          // Only supported by SU735

        new_div_ratio:
                1 -> 100 MHz
                2 -> 50 MHz
                3 -> 25 MHz
                4 -> 33 MHz

        return:
          ERROR_ADC_TEST_PATERN_NOT_SUPPORTED */
int32_t CDL709_7ADC_1TTL::findBestADCClockPhase(uint16_t new_adc_tp,
                                                                                uint16_t new_div_ratio)
{
    int16_t i, err;
    uint32_t L, mask3b;
        uint8_t ptxtBuffer[128];

        uint32_t pErrorList[511];
        uint32_t pMinErrorList[511];
        uint32_t minErrorValue = 4294967295;
        uint16_t new_div_phase;
        uint32_t new_adc_rorder;
        uint16_t new_adc_dll_on;

        new_adc_rorder = 0x1AC688; // read all ADC channels

        // New things
        new_div_ratio = ((new_div_ratio-1) & 3) + 1;

        if (new_div_ratio == 1) // 100 MHZ
        {
                new_adc_dll_on = 1;
                new_div_phase  =  0;
        }
        else if (new_div_ratio == 2) // 50 MHZ
        {
                new_adc_dll_on = 1;
                new_div_phase  =  0;
        }
        if (new_div_ratio == 3) // 33 MHZ
        {
                new_adc_dll_on = 1;
                new_div_phase  =  0;
        }
        else if (new_div_ratio == 4) // 25 MHZ
        {
                new_adc_dll_on = 1;
                new_div_phase  =  1;
        }


    if (new_div_ratio>4) new_div_ratio=4;
    if (new_div_ratio<1) new_div_ratio=1;
    adc_tp = new_adc_tp & 3;
    div_ratio = new_div_ratio-1;
    div_phase = new_div_phase & 0x3;
    err = 0;
    adc_present = 0;
    adc_rorder = new_adc_rorder;
    mask3b = 0;
    for (i=0; i<7; i++)
    {
        if ((new_adc_rorder & 0x7) < 7)
            adc_present |= (1 << (new_adc_rorder & 0x7));
        else
            mask3b = 7;
        new_adc_rorder = new_adc_rorder >> 3;
        adc_rorder = adc_rorder | (mask3b << (3*i) );
    }
    err += m_pCBus->write32(EB_RD_ORDER, adc_rorder);
    adc_dll_on  = new_adc_dll_on & 1;

        // ADCs reset
        if (m_su == SU706)
                err += m_pSU706->resetADCs();
        else if (m_su == SU735)
                err += m_pSU735->resetADCs();

        //rubio start
        for (i=0; i<7; i++)
        {

                if (m_su == SU706)
                {
                        err += m_pSU706->powerDown(i,0); // power down off
                        err += m_pSU706->setDLL(i,adc_dll_on); // dll
                        err += m_pSU706->setTestPattern(i,3);      // test pattern mode on
                }
                else if (m_su == SU735)
                {
                        //m_pSU735->SPI_ADC_write(i, 0, 0xFF);
                        Sleep(1);
                        // set ADC output test pattern, 5 for 0101 -> 0101, 7 for 0000 -> 1111...
            //m_pSU735->SPI_ADC_write(i, 4, (5)*8);
                        m_pSU735->setTestPattern(i, ADC_TP_10_01);
                }
        }

        // use real adc data and not the internally (in LBox FPGA) generated
        //m_pCBus->write32(EB_EXT_CTRL, 0);
		uint8_t tempConfRegister;
		this->getConfigurationRegister(&tempConfRegister);
		this->setConfigurationRegister(0);

        err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, (1 << 15) | (div_phase << 10) | (div_ratio << 8));
        //rubio end

    // DCM reset
    err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, 1);

        sprintf((char*)ptxtBuffer,"reset DCM\n");
        #ifdef debug
        printf((char*)ptxtBuffer);
        #endif
        if (m_pFile->is_open())
                (*m_pFile) << (char*)ptxtBuffer;

        sprintf((char*)ptxtBuffer,"Finding the optimal phase using the DCM\n\nPhase    ch1    ch2    ch3    ch4    ch5    ch6    ch7   Total errors\n");
        #ifdef debug
        printf((char*)ptxtBuffer);
        #endif
        if (m_pFile->is_open())
                (*m_pFile) << (char*)ptxtBuffer;


    for (dcm_phase = -255; dcm_phase < 255; dcm_phase++)
    {
        i = 0;
        do
        {
            // read back status
            err += m_pCBus->read32(ADC_LED_DCM_CTRL+6, &L);
            i++;
        } while ((((L >> 4) & 1) == 0) && (i < 10));
        err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, 2+128); // INC

                //rubio start
                // start test pattern check
                uint32_t dat[4], cnt[8];
        m_pCBus->write32(EVBUF_CMD, CMD_START_TP);
        Sleep(100);
        // read the data
                m_pCBus->readBurst(BA_EV_BUFFER, dat, 4, 4, 1);
        for (uint8_t channel=0; channel<4; channel++)
        {
            cnt[channel*2  ]= dat[channel]        & 0xFFFF;
            cnt[channel*2+1]=(dat[channel] >> 16) & 0xFFFF;
        }


                sprintf((char*)ptxtBuffer,"%7d", dcm_phase);
                #ifdef debug
                printf((char*)ptxtBuffer);
                #endif
                if (m_pFile->is_open())
                        (*m_pFile) << (char*)ptxtBuffer;
                pErrorList[dcm_phase+255] = 0;
                for (uint8_t channel=0; channel<7; channel++)
                {
                        sprintf((char*)ptxtBuffer,"%7d",cnt[channel]);
                        #ifdef debug
                        printf((char*)ptxtBuffer);
                        #endif
                        if (m_pFile->is_open())
                                (*m_pFile) << (char*)ptxtBuffer;
                        pErrorList[dcm_phase+255] += cnt[channel];

                }

                if (pErrorList[dcm_phase+255]<minErrorValue)
                        minErrorValue = pErrorList[dcm_phase+255];

                sprintf((char*)ptxtBuffer,"%7d",pErrorList[dcm_phase+255]);
                #ifdef debug
                printf((char*)ptxtBuffer);
                #endif
                if (m_pFile->is_open())
                        (*m_pFile) << (char*)ptxtBuffer;

                sprintf((char*)ptxtBuffer,"\n");
                #ifdef debug
                printf((char*)ptxtBuffer);
                #endif
                if (m_pFile->is_open())
                        (*m_pFile) << (char*)ptxtBuffer;



                // rubio end

    }
    // set the ADC mask, turn the clocks on
    #ifdef debug
    printf("Set ADC mask to 0x%02x\n", adc_present);
    #endif
    err += m_pCBus->write32(ADC_LED_DCM_CTRL+7, adc_present);

    for (i=0; i<7; i++)
        {
                if (m_su == SU706)
                {
                        if ( (adc_present >> i) & 1)
                        {
                                err += m_pSU706->powerDown(i,0); // power down off
                                err += m_pSU706->setDLL(i,adc_dll_on); // dll
                                err += m_pSU706->setTestPattern(i,0);      // test pattern mode off
                        }
                        else
                                err += m_pSU706->powerDown(i,1); // power down off
                }
                else if (m_su == SU735)
                {
                        // switch off the ADC output test pattern
                        //m_pSU735->SPI_ADC_write(i, 4, 0);
                        m_pSU735->setTestPattern(i, ADC_TP_OFF);
                }
        }
    uint16_t k,j;
        uint16_t indexMaxMin; // maximal length with minimal errors
        uint32_t valueMaxMin;

        valueMaxMin = 0;
        indexMaxMin = 0;

        // Create a table with the minimal error length
        for (k = 0; k < 510; k++)
        {
                for (j = k; j < 510; j++)
                {
                        if (pErrorList[j] != minErrorValue)
                                break;
                }
                pMinErrorList[k] = j-k;
                if (pMinErrorList[k]>valueMaxMin)
                {
                        valueMaxMin = j-k;
                        indexMaxMin = k;
                }
        }

        int16_t bestPhase;
        bestPhase = indexMaxMin + (valueMaxMin/2)-255;

        sprintf((char*)ptxtBuffer,"\nThe best phase for %i MHZ was found ph = %d\n\n",100/ new_div_ratio, bestPhase);
        #ifdef debug
        printf((char*)ptxtBuffer);
        #endif
        if (m_pFile->is_open())
                (*m_pFile) << (char*)ptxtBuffer;

        // Set now the best phase
        // DCM reset
    err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, 1);
        for (dcm_phase = -255; dcm_phase < bestPhase; dcm_phase++)
    {
        i = 0;
        do
        {
            // read back status
            err += m_pCBus->read32(ADC_LED_DCM_CTRL+6, &L);
            i++;
        } while ((((L >> 4) & 1) == 0) && (i < 10));
        err += m_pCBus->write32(ADC_LED_DCM_CTRL+6, 2+128); // INC
        }

        if (m_pFile->is_open())
                m_pFile->flush();

	this->setConfigurationRegister(tempConfRegister);
    
	return err;
}

int32_t CDL709_7ADC_1TTL::setRandomizerRegister(uint8_t enableMask)
{

   int32_t err;
   uint32_t u32temp;

   //only supported by SU735
   if (m_su == SU706)
	   return -1;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, &u32temp);

   enableMask &= 0x7F;
   u32temp &= ~0x7F000000;
   u32temp |= (((uint32_t)enableMask)<<24);

   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, u32temp);
   
   for(uint8_t i = 0; i < 7 ; i++)
   {
		if (enableMask &0x01)
			m_pSU735->setRandomizer(i,TRUE);
		else
			m_pSU735->setRandomizer(i,FALSE);

		enableMask>>=1;   
   }


   return err;
}

int32_t CDL709_7ADC_1TTL::getRandomizerRegister(uint8_t *enableMask)
{

   int32_t err;
   uint32_t u32temp;
	
   //only supported by SU735
   if (m_su == SU706)
	   return -1;


   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, &u32temp);
   
   *enableMask = ((u32temp>>24)&0x7F); 
   

   return err;
}