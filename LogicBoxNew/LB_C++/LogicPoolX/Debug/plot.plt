# for swapped odd-even samples only
set xrange [0:*]
#plot "dat.dat" u ($1-1):7 every 2:1:1 with points, \
#     "dat.dat" u ($1+1):7 every 2:1:2 with points
#

# pause -1

plot "dat.dat" u 1:2 with lines, \
     "dat.dat" u 1:3 with lines, \
     "dat.dat" u 1:4 with lines, \
     "dat.dat" u 1:5 with lines, \
     "dat.dat" u 1:6 with lines, \
     "dat.dat" u 1:7 with lines, \
     "dat.dat" u 1:8 with lines
#pause -1         lines
