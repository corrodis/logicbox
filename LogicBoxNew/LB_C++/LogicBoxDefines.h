#ifndef FILE_LogicBoxDefines_H
#define FILE_LogicBoxDefines_H

#pragma pack(show)
#pragma pack(push)
#pragma pack(1)
#pragma pack(show)

typedef struct str_Date{
    uint16_t year;
    uint16_t month;
    uint16_t day;
    uint16_t hour;
    uint16_t min;
    uint16_t sec;
} STRUCT_DATE;

typedef struct str_Subv{
    uint16_t year;
    uint16_t month;
    uint16_t day;
    uint16_t svn;
} STRUCT_SVNID;

typedef struct str_CardInfo{
    uint8_t baseBoard;
    uint16_t designId;
    uint8_t revision;    
} STRUCT_CARDINFO;

#define FX2_SYSTEM      1
#define FTDI_SYSTEM     2
typedef struct struct_logicBoxInf{
    uint32_t systemID; // DL701 = 1, DL706 = 6 .... etc
    uint32_t deviceNumber; // 1,2,3.....
    char systemName[16];
    char serialNumber[16]; // check if the size is enought big
} LOGIC_BOX_INF;


typedef struct struct_logicBoxList{
        int32_t nDevices;
        LOGIC_BOX_INF *pLogicBoxInfo;
} LOGIC_BOX_LIST;

#pragma pack(pop)
#pragma pack(show)


/////////////////////////////////////////
// Error codes
#define LB_ERROR -1
#define LB_SUCCESS      0
#define LB_DLL_ERROR_DLL_NOT_FOUND              1
#define LB_DLL_ERROR_LOADING_FUNCTION           2
#define LB_NO_DEVICE_OPEN                       3
#define LB_CANNOT_OPEN_DEVICE                   4
#define LB_CANNOT_READ_ALL_DATA                 5
#define LB_CANNOT_WRITE_ALL_DATA                6
#define LB_CANNOT_ALLOC_MEMORY                  7



#endif // FILE_LogicBoxDefines_H
