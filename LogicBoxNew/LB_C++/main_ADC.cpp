#include "CLogicBox.h"
#include "CCBUS.h"
#include "CDL709_7ADC_1TTL.h"

#define DMA


int main (int32_t argc, char *argv[])
{

    int hexflag = 1;
   INT ret, dll, adc;
   CDL709_7ADC_1TTL *pMyDL709;
   uint16_t tp;
   CLogicBox  *pMyLogicBox;
   CCBUS  *pMyCBus;
   uint16_t nsamples, clkDivider;
   uint16_t eventSize;
   uint16_t nEvents;
   uint32_t Id,timeStamp;
   uint8_t myByte;

   pMyLogicBox = new CLogicBox;
   //LOGIC_BOX_LIST List;
   LOGIC_BOX_LIST* pList;
   pList = pMyLogicBox->getDevices();




   if (argc > 2)
   {

      int8_t k;

      nsamples = atoi(argv[1]);
      clkDivider = atoi(argv[2]);

      for (k = 0; k < 8;  k++)
      {
         if (EventSizeConversion[k] == nsamples)
            break;
      }

      if (EventSizeConversion[k] != nsamples)
      {
         printf("Error number of samples\n");
         system("pause");
         exit(0);
      }
   }
   else
   {

      nsamples = 16;
      clkDivider = 1;
   }
   // {16,32,64,128,256,512,1024,2048};
   nsamples = 1024;

   printf("# Number of samples pro event: %i\n", nsamples);
   printf("# Clock divider: %i\n" , clkDivider);

   if (pList->nDevices == 0)
   {
      printf("No device found\n");
      system("pause");
      return(0);
   }
   else if (pList->nDevices == 1)
   {
      printf("# Trying to open device %s\n", pList->pLogicBoxInfo[0].serialNumber);
      ret = pMyLogicBox->openDevice(pList->pLogicBoxInfo[0].deviceNumber);
      if (!ret)
         printf("# Device successfully opened\n");
      else
      {
         printf("Error opening device\n");
         system("pause");
         return(0);
      }

   }
   else if (pList->nDevices>1)
   {
      printf("# More than one device found:\n");

      for(int32_t i = 0; i < pList->nDevices;i++)
      {
         printf("# Device %i: %s -> Serial number %s\n", i,
               pList->pLogicBoxInfo[i].systemName,
               pList->pLogicBoxInfo[i].serialNumber);

      }

      int32_t index;
      while(1)
      {

         printf("# please select the device to be opened\n");
         scanf("%u",&index);
         if ((index <= pList->nDevices) && (index >= 0))
            break;
      }

      ret = pMyLogicBox->openDevice(pList->pLogicBoxInfo[index].deviceNumber);

      if (!ret)
         printf("# Device successfully opened\n");
      else
      {
         printf("Error opening device\n");
         system("pause");
         return(0);
      }

   }




   pMyCBus = pMyLogicBox->getCBusHandel();
   
   // create a new object
   STRUCT_CARDINFO cardInfo;
   pMyLogicBox->getCardInfo(&cardInfo);
   pMyDL709 = new CDL709_7ADC_1TTL(pMyCBus, cardInfo.designId, cardInfo.revision);
   tp = 3;

   STRUCT_DATE compDate;
   pMyLogicBox->getCompilationDate(&compDate);
   printf("# Compile date yyyy/mm/dd %4d/%02d/%02d\n",compDate.year,
                                                  compDate.month,
                                                  compDate.day);

   printf("# Compile time hh:mm:ss %02d:%02d:%02d\n",compDate.hour,
                                                 compDate.min,
                                                 compDate.sec);

//   system("pause");

   // Set the FPGA event size
   int8_t k;
   for (k = 0; k < 8;  k++)
   {
      if (EventSizeConversion[k] == nsamples)
         break;
   }
   eventSize = 7*nsamples*2;
   printf("# Set the event size to %i Samples\n", EventSizeConversion[k]);
   pMyDL709->setEventSize(k);
   pMyDL709->usb_adc_init(ADR_RORDER_FULL, 1);

   
   pMyDL709->findBestADCClockPhase(ADC_TP_TOGGLE,1);
   
   uint16_t pData[2048*7*2];
   



   //enable the HW trigger
   printf("# Enabling the trigger\n");
   ret = pMyDL709->disableHWTrigger(false);

   // Trigger one event
   pMyDL709->setCommandRegister(SOFT_TRIGGER);
   printf("# Triggering the system \n");
   	// Trigger one event
	pMyDL709->setCommandRegister(SOFT_TRIGGER);
	printf("# Triggering the system \n");

   // Check
   pMyDL709->getNumberEvents(&nEvents);
   printf("# Number of events: %i\n", nEvents);

   if (nEvents < 1)
   {
      printf("# Error events in buffer != 0\n");
      pMyLogicBox->closeDevice();
      delete pMyDL709;
      return(0);
   }

    pMyDL709->getConfigurationRegister(&myByte);
    printf("Configuration register = 0x%02x\n",myByte);

	// get the number of available bytes in the FPGA buffer
	uint32_t Bytes2Read;
   ret = pMyCBus->getAvailableBytes(&Bytes2Read);


   // read events from FX2
   printf("# reading all events from FPGA memory\n");


#ifdef DMA
	//
	uint16_t FpgaEvents;
	//pMyDL709->getFpgaEventsDMA(&FpgaEvents);
	//pMyDL709->setDMA(false);
#else
	pMyDL709->getAllFpgaEvents();
#endif


  //pMyDL709->setDMA(true);
//while(1)
{
	
	//pMyDL709->getFpgaEventsDMA(&FpgaEvents);
	pMyDL709->getAllFpgaEvents();
   ret = 0;
   //Print out the events<
   
   
   while(ret == 0)
   {
	   ret = pMyDL709->readMemoryEvent(&Id, &timeStamp, eventSize/2, pData);
	   if (!ret)
		  printf("# Event ID: %i, timestamp: %u\n",Id,timeStamp);
	  /* //else
	   //	  printf("# Error reading event\n");*/
   }


}
   /*
   uint32_t offset;
   for(i = 0; i < (nsamples/2); i++)
   {
      offset = i*14;

      printf("%4d ",i*2);
    if (hexflag==1)
      printf("%04x %04x %04x %04x %04x %04x %04x\n",
                pData[offset], pData[2+offset],
                pData[4+offset], pData[6+offset],
                pData[8+offset], pData[10+offset],
                pData[12+offset]);
    else
      printf("%6d %6d %6d %6d %6d %6d %6d\n",
                pData[offset], pData[2+offset],
                pData[4+offset], pData[6+offset],
                pData[8+offset], pData[10+offset],
                pData[12+offset]);
      printf("%4d ",i*2+1);
    if (hexflag==1)
      printf("%04x %04x %04x %04x %04x %04x %04x\n",
                pData[1+offset], pData[3+offset],
                pData[5+offset], pData[7+offset],
                pData[9+offset], pData[11+offset],
                pData[13+offset]);
    else
      printf("%6d %6d %6d %6d %6d %6d %6d\n",
                pData[1+offset], pData[3+offset],
                pData[5+offset], pData[7+offset],
                pData[9+offset], pData[11+offset],
                pData[13+offset]);

   }
	*/

   // close the device
   pMyLogicBox->closeDevice();
   // free my object
   delete pMyDL709;

   system("pause");
   return 0;


   }