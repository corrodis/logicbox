#ifndef FILE_CFTDI_H
#define FILE_CFTDI_H

#include "data_types.h"
#include "LogicBoxDefines.h"
#include "CStdPhyInt.h"

// Include for the FX2 dll
#include "USB_FTDI\ftd2xx.h"
// Include for the dinamic load of the DLL
#include "windows.h"
#include <tchar.h>


// Defines to allow the dinamic load of the DLL of the FX2
typedef  FT_STATUS (__stdcall* LPFT_ListDevices)(PVOID ,PVOID ,DWORD);
typedef  FT_HANDLE(__stdcall* LPFT_W32_CreateFile)(LPCSTR,DWORD,DWORD,LPSECURITY_ATTRIBUTES,DWORD,DWORD,HANDLE);
typedef  BOOL(__stdcall* LPFT_W32_GetCommState)(FT_HANDLE , LPFTDCB );
typedef  BOOL(__stdcall* LPFT_W32_SetCommState)(FT_HANDLE , LPFTDCB );
typedef  BOOL(__stdcall* LPFT_W32_SetupComm)(FT_HANDLE , DWORD, DWORD );
typedef  BOOL(__stdcall* LPFT_W32_WriteFile)(FT_HANDLE , LPVOID, DWORD, LPDWORD, LPOVERLAPPED);
typedef  BOOL(__stdcall* LPFT_W32_ReadFile)(FT_HANDLE , LPVOID, DWORD, LPDWORD, LPOVERLAPPED);
typedef  FT_STATUS(__stdcall* LPFT_CreateDeviceInfoList)(LPDWORD);
typedef  BOOL(__stdcall* LPFT_W32_CloseHandle)(FT_HANDLE);


class CFTDI : public CStdPhyInt  
{
   public:
      CFTDI();
      ~CFTDI();		
      virtual int32_t getNumberDevices(uint32_t *nDev);
      virtual int32_t getSerialNumber(uint32_t nDevice, int8_t *pSerial);
      virtual int32_t openDevice(int32_t index);
      virtual int32_t closeDevice();
      virtual int32_t nBytesAvailable(uint32_t* nBytes);
      virtual int32_t write_bytes(uint32_t nBytes, void * buffer);
      virtual int32_t read_bytes(uint32_t nBytes, void * buffer);
      virtual bool isOpen();
	  static HINSTANCE m_hDLL;

   private:		
      int32_t CFTDI::LoadLib();
      // Function pointers
      static LPFT_ListDevices            DLL_FT_ListDevices      ;
      static LPFT_W32_CreateFile         DLL_FT_W32_CreateFile   ;
      static LPFT_W32_GetCommState       DLL_FT_W32_GetCommState ;
      static LPFT_W32_SetCommState       DLL_FT_W32_SetCommState ;
      static LPFT_W32_SetupComm          DLL_FT_W32_SetupComm    ;
      static LPFT_W32_WriteFile          DLL_FT_W32_WriteFile    ;
      static LPFT_W32_ReadFile           DLL_FT_W32_ReadFile     ;
      static LPFT_CreateDeviceInfoList   DLL_FT_CreateDeviceInfoList;
      static LPFT_W32_CloseHandle        DLL_FT_W32_CloseHandle;
      
      static uint32_t m_isLibraryLoaded;
      static int32_t m_lastError;
      FT_HANDLE m_hDevice; // Handle to LogicBox

};

#endif // FILE_CFTDI_H
