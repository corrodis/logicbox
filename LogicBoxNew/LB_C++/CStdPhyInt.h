#ifndef FILE_CStdPhyInt_H
#define FILE_CStdPhyInt_H

#include "data_types.h"

class CStdPhyInt  
{
    public:
      
      virtual int32_t getNumberDevices(uint32_t *nDev) = 0;
      virtual int32_t getSerialNumber(uint32_t nDevice, int8_t *pSerial) = 0;
		virtual int32_t openDevice(int32_t index)= 0;
		virtual int32_t closeDevice()= 0;
		virtual int32_t nBytesAvailable(uint32_t* nBytes)= 0;
		virtual int32_t write_bytes(uint32_t nBytes, void * buffer)= 0;
		virtual int32_t read_bytes(uint32_t nBytes, void * buffer)= 0;      
      virtual bool isOpen() =  false;

};

#endif // FILE_CStdPhyInt_H