#include "CDL709_2TTL_TDC.h"

CDL709_2TTL_TDC::CDL709_2TTL_TDC(CCBUS *pCBus)
{

   m_enable = false;
   m_pCBus = pCBus;
   tdc_present = 0x1FF;
   inp_termination = 0;
   inp_disabled = 0;
   io_inverted = 0;

   // try to allocate the memory for the particle event
   m_parBuffer.nChannels = 7;
   m_parBuffer.channelsMask = 0x7F;
   m_parBuffer.eventSize  = sizeof(PARTICLE_EVENT_TDC);
   m_parBuffer.eventOffset  = sizeof(PARTICLE_EVENT_TDC);

   m_parBuffer.nTotalEvents = TDC_PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE / m_parBuffer.eventOffset;

   m_parBuffer.nEvents = 0;
   m_parBuffer.currentPosWriteEvent = 0;
   m_parBuffer.currentPosReadEvent = 0;
   m_parBuffer.pData = (uint8_t*) malloc(TDC_PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);
   m_pRawData = (uint8_t*) malloc(m_parBuffer.eventSize*m_parBuffer.nTotalEvents);

   m_parBuffer.writeAddress = m_parBuffer.pData ;
   m_parBuffer.readAddress = m_parBuffer.pData ;
   m_parBuffer.endAddress = (m_parBuffer.pData + TDC_PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);


}
CDL709_2TTL_TDC::~CDL709_2TTL_TDC(){}


int32_t CDL709_2TTL_TDC::usb_write_long(uint32_t Address, uint32_t Data)
{
   return m_pCBus->write32(Address,Data);
}

int32_t CDL709_2TTL_TDC::usb_read_long(uint32_t Address, uint32_t* Data)
{

   return m_pCBus->read32(Address,Data);
}



int32_t CDL709_2TTL_TDC::getTerminationMask(uint16_t *termination, uint16_t *standard)
{
   
   int32_t err;
   uint32_t reg;
   err = m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_INP_TERMINATE_REGISTER, &reg);
   
   *termination = reg&0x1FF;
   *standard = (reg>>16)&0x1FF;

   return err;
}

int32_t CDL709_2TTL_TDC::setTerminationMask(uint16_t termination, uint16_t standard)
{
   
   int32_t err;   
   uint32_t temp;
   temp = ((uint32_t)(standard<<16)) | termination;

   err = m_pCBus->write32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_INP_TERMINATE_REGISTER, temp);      

   return err;
}

int32_t CDL709_2TTL_TDC::getIOMask(uint16_t *disableMask, uint16_t *invertMask)
{
   
   int32_t err;
   uint32_t reg;
   err = m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_INP_DIS_INV_REGISTER, &reg);
   
   *disableMask = reg&0x1FF;
   *invertMask = (reg>>16)&0x3FF;

   return err;
}

int32_t CDL709_2TTL_TDC::setIOMask(uint16_t disableMask, uint16_t invertMask)
{
   
   int32_t err;
   uint32_t reg;

   reg = (invertMask<<16) | disableMask;
   err = m_pCBus->write32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_INP_DIS_INV_REGISTER, reg);
   
   return err;
}

int32_t CDL709_2TTL_TDC::enableEventbuffer(bool ena)
{
   
   m_enable = ena;

   if (ena)
      return m_pCBus->write32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_SM_COMMAND_REGISTER, 0x01);
   else
      return m_pCBus->write32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_SM_COMMAND_REGISTER, 0);

   
}

int32_t CDL709_2TTL_TDC::setDMA(bool ena)
{

	return m_pCBus->setDMA(ena);
}

int32_t CDL709_2TTL_TDC::resetEventbuffer(void)
{
   uint32_t reg;
   
   reg = 0;
   if (m_enable)
      reg |= 0x01;
   reg |= TDC_EVENT_CLEAR | TDC_RESET_STATE_MACHINE;

   return m_pCBus->write32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_SM_COMMAND_REGISTER, reg);
}
int32_t CDL709_2TTL_TDC::triggerSystem(uint16_t channelMask)
{
  

   return m_pCBus->write32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_SOFT_TRIGGER_REGISTER, channelMask);      
   
}


int32_t CDL709_2TTL_TDC::getNumberEvents(uint16_t *nEvents)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_STATUS_REGISTER, &reg);

   *nEvents = (reg&0xFFFF);

   return err;
}

int32_t CDL709_2TTL_TDC::getFPGAEngineStatus(uint16_t *nEvents, uint8_t *readEngine, uint8_t *readMux,
                                             bool *bufferFull, bool *bufferEmpty)
{
   
   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_STATUS_REGISTER, &reg);
   
   if (!err)
   {
      *nEvents    = (reg&0x3FF);
      *readEngine = ( (reg>>16) & 0x7);
      *readMux    = ( (reg>>19) & 0x7);
      *bufferFull    = ( (reg>>22) & 0x1);
      *bufferEmpty    = ( (reg>>23) & 0x1);
   }

   return err;




}


int32_t CDL709_2TTL_TDC::getNumberEventsStored(uint32_t *nEventsStored)
{
   return m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_TOTAL_EV_STORED, nEventsStored);
}

int32_t CDL709_2TTL_TDC::getNumberEventsRead(uint32_t *nEventsRead)
{
   return m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_TOTAL_EV_READ, nEventsRead);
}



int32_t CDL709_2TTL_TDC::getMissedEvents(uint32_t *totalCounter, uint32_t *bufferFullCounter)
{

   int32_t err;

   if((totalCounter ==NULL) || (bufferFullCounter == NULL))
      return LB_ERROR;

   err = m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_MISSED_EVENTS_REGISTER, totalCounter);
   if (err)
      return err;

   err = m_pCBus->read32(BASE_ADDR_TDC_EVENT_CONTROL+TDC_ADDR_MISSED_EVENTS_FULL_REGISTER, bufferFullCounter);

   return err;
}



int32_t CDL709_2TTL_TDC::getAllFpgaEvents()
{

	
   uint16_t nEvents,FpgaEvents;
   getNumberEvents(&nEvents);
   if (nEvents>0)
   {
         getFpgaEvents(nEvents, &FpgaEvents, FALSE);
      if (FpgaEvents>0)
         getFpgaEvents(FpgaEvents, &FpgaEvents, FALSE);
   }

   return LB_SUCCESS;
}


//nData length of pData in 16-Bits words
int32_t CDL709_2TTL_TDC::readMemoryEvent( uint32_t *Id,uint32_t *timeStamp, uint16_t nData, uint16_t *pData)
{


   if ((nData*2) != (m_parBuffer.eventSize - TDC_PARTICLE_EVENT_FPGA_HEADER_SIZE))
      return LB_ERROR;
   if (m_parBuffer.nEvents == 0)
      return LB_ERROR;



   PARTICLE_EVENT_TDC *pEvent;
   pEvent = (PARTICLE_EVENT_TDC*) m_parBuffer.readAddress;

   *Id = pEvent->eventID;
   *timeStamp = pEvent->timeStamp;
   memcpy(pData,&(pEvent->dataCh0), 7*2);


   if(m_parBuffer.currentPosReadEvent >= (m_parBuffer.nTotalEvents-1))
   {
      m_parBuffer.currentPosReadEvent = 0;
      m_parBuffer.readAddress = m_parBuffer.pData;
   }
   else
   {
      m_parBuffer.currentPosReadEvent++;
      m_parBuffer.readAddress += m_parBuffer.eventOffset;

   }

   m_parBuffer.nEvents--;

   return LB_SUCCESS;

}

int32_t CDL709_2TTL_TDC::getFpgaEvents(uint16_t Events2Read, uint16_t *FpgaEvents, bool DMA)
{


   int32_t err;



   PARTICLE_EVENT_TDC *pEvent;
   *FpgaEvents = 0;


   if (m_parBuffer.pData == NULL)
      return LB_CANNOT_ALLOC_MEMORY;

   //check if the PC buffer is full, if not read the FPGA events until the buffer is full
   if ((m_parBuffer.nEvents + Events2Read) >= m_parBuffer.nTotalEvents)
   {
      if  ((m_parBuffer.nEvents ) >= m_parBuffer.nTotalEvents)
         return LB_ERROR;
      else
         Events2Read = (m_parBuffer.nTotalEvents - m_parBuffer.nEvents);
   }

   // If end of buffer read only until the end of the buffer and go to first memory address
   if (m_parBuffer.currentPosWriteEvent + Events2Read > m_parBuffer.nTotalEvents)
   {
      if (Events2Read>1)
         Events2Read =  m_parBuffer.nTotalEvents - m_parBuffer.currentPosWriteEvent;
      else
         m_parBuffer.writeAddress = m_parBuffer.pData;
   }


   if (DMA)
	err = m_pCBus->read32DMA((m_parBuffer.eventSize * Events2Read),m_pRawData);
   else
	err = m_pCBus->read32(TDC_BASE_ADDR_TDC_EVENT_DATA, (m_parBuffer.eventSize * Events2Read),m_pRawData);
   
   if(err)
      return LB_ERROR;

   // TODO: reorder the data
   
   uint8_t *pData;
   pData = m_pRawData;

   for(uint16_t i = 0; i < Events2Read ; i++)
   {

      pEvent = (PARTICLE_EVENT_TDC*) m_parBuffer.writeAddress;
      pEvent->eventID = (pData[0]<<24) | (pData[1]<<16) | (pData[2]<<8) | (pData[3]);
      //pEvent->eventSize = EventSizeConversion[(pData[3]&0x07)];
      pEvent->timeStamp = (pData[4]<<24) | (pData[5]<<16) | (pData[6]<<8) | (pData[7]);
      //pEvent->nEvents = ((pData[8]<<8)|(pData[9]))&0xFFFF;
      pEvent->nEvents = ((pData[10]<<8)|(pData[11]))&0xFFFF;
      
      pEvent->dataCh0 = ((pData[8]<<8)|(pData[9]))&0xFFFF;
      pEvent->dataCh2 = ((pData[12]<<8)|(pData[13]))&0xFFFF;
      pEvent->dataCh1 = ((pData[14]<<8)|(pData[15]))&0xFFFF;
      pEvent->dataCh4 = ((pData[16]<<8)|(pData[17]))&0xFFFF;
      pEvent->dataCh3 = ((pData[18]<<8)|(pData[19]))&0xFFFF;
      pEvent->dataCh6 = ((pData[20]<<8)|(pData[21]))&0xFFFF;
      pEvent->dataCh5 = ((pData[22]<<8)|(pData[23]))&0xFFFF;

      

      if (m_parBuffer.currentPosWriteEvent == m_parBuffer.nTotalEvents-1)
      {
         m_parBuffer.currentPosWriteEvent = 0;
         // Update the write pointer
         m_parBuffer.writeAddress = (uint8_t*)m_parBuffer.pData;
      }
      else
      {
         m_parBuffer.currentPosWriteEvent++;
         // Update the write pointer
         m_parBuffer.writeAddress += m_parBuffer.eventOffset;
      }

      // Atomic operation????
      m_parBuffer.nEvents++;

      // Next event in the buffer
      pData += m_parBuffer.eventSize;

   }

    //Number of events in the FPGA
    *FpgaEvents = pEvent->nEvents-1;




   return err;
}


int32_t CDL709_2TTL_TDC::getFpgaEventsDMA(uint16_t *FpgaEvents)
{

   int32_t err; 
   uint16_t Events2Read;   
   uint32_t Bytes2Read;
   
   // get the number of available bytes in the FPGA buffer
   err = m_pCBus->getAvailableBytes(&Bytes2Read);
   
   // calculate the event number in the buffer
   Events2Read = Bytes2Read / m_parBuffer.eventSize;

   // read it
   if (Events2Read > 0)
	err = getFpgaEvents(Events2Read, FpgaEvents, TRUE);

   return err;

   

}