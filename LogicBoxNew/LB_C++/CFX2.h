#ifndef FILE_CFX2_H
#define FILE_CFX2_H

#include "data_types.h"
#include "LogicBoxDefines.h"
#include "CStdPhyInt.h"

// Include for the FX2 dll
#include "USB_FX2//USBBoxlib.h"
// Include for the dinamic load of the DLL
#include "windows.h"
#include <tchar.h>





// Defines to allow the dinamic load of the DLL of the FX2
//int32_t USBBoxCountDevices( uint32_t * numberOfDevices );
typedef int32_t (__cdecl* LPUSBBoxCountDevices)(uint32_t*);
//const int8_t * USBBoxGetDeviceSerialNumber( uint32_t index );
typedef int8_t* (__cdecl* LPUSBBoxGetDeviceSerialNumber)(uint32_t);
//int32_t USBBoxOpenDevice( uint32_t index, USBBoxDeviceRef * result );
typedef int32_t (__cdecl* LPUSBBoxOpenDevice)(uint32_t,USBBoxDeviceRef*);
//int32_t USBBoxCloseDevice( USBBoxDeviceRef device );
typedef int32_t (__cdecl* LPUSBBoxCloseDevice)(USBBoxDeviceRef);
//int32_t USBBoxSetPortDOutputEnable( USBBoxDeviceRef device, uint8_t OERegisterByte );
typedef int32_t (__cdecl* LPUSBBoxSetPortDOutputEnable)(USBBoxDeviceRef, uint8_t);
//int32_t USBBoxSetPortDOutputs( USBBoxDeviceRef device, uint8_t IO );
typedef int32_t (__cdecl* LPUSBBoxSetPortDOutputs)(USBBoxDeviceRef, uint8_t);
//int32_t USBBoxGetPortDOutputs( USBBoxDeviceRef device, uint8_t * io_register );
typedef int32_t (__cdecl* LPUSBBoxGetPortDOutputs)(USBBoxDeviceRef, uint8_t*);
//DLLAPI int32_t USBBoxSetPortDOutputsExt( USBBoxDeviceRef device, uint8_t Number, uint8_t* IO );
typedef int32_t (__cdecl* LPUSBBoxSetPortDOutputsExt)(USBBoxDeviceRef, uint8_t, uint8_t*);
typedef int32_t (__cdecl* LPUSBBoxSetConfFpgaMode )(USBBoxDeviceRef, uint8_t);
//int32_t USBBoxLoadFirmware( USBBoxDeviceRef device, uint8_t *pFileName)
typedef int32_t ( __cdecl* LPUSBBoxLoadFirmware)(USBBoxDeviceRef, uint8_t*);
//DLLAPI int32_t USBBoxReadEprom( USBBoxDeviceRef device, uint8_t epromAddr, uint16_t addr, uint8_t len, void * buffer);
typedef int32_t ( __cdecl* LPUSBBoxReadEprom)( USBBoxDeviceRef , uint8_t, uint16_t , uint8_t , void *);
//DLLAPI int32_t USBBoxWriteDevice( USBBoxDeviceRef device, uint32_t bytes, void * buffer );
typedef int32_t ( __cdecl* LPUSBBoxWriteDevice)( USBBoxDeviceRef , uint32_t, void *);
//DLLAPI int32_t USBBoxReadDevice( USBBoxDeviceRef device, uint32_t bytes, void * buffer );
typedef int32_t ( __cdecl* LPUSBBoxReadDevice)( USBBoxDeviceRef , uint32_t, void *);
//DLLAPI int32_t USBBoxNumberOfBytesAvailable( USBBoxDeviceRef device, uint32_t * bytesAvailable );
typedef int32_t ( __cdecl* LPUSBBoxNumberOfBytesAvailable)( USBBoxDeviceRef , uint32_t*);


class CFX2 : public CStdPhyInt  
{
   public:
      CFX2();
      ~CFX2();		
      virtual int32_t getNumberDevices(uint32_t *nDev);
      virtual int32_t getSerialNumber(uint32_t nDevice, int8_t *pSerial);
      virtual int32_t openDevice(int32_t index);
      virtual int32_t closeDevice();
      virtual int32_t nBytesAvailable(uint32_t* nBytes);
      virtual int32_t write_bytes(uint32_t nBytes, void * buffer);
      virtual int32_t read_bytes(uint32_t nBytes, void * buffer);
      virtual bool isOpen();
	  static HINSTANCE m_hDLL;

   private:		
      int32_t CFX2::LoadLib();
      static LPUSBBoxCountDevices DLL_USBBoxCountDevices;                            // Function pointer
      static LPUSBBoxGetDeviceSerialNumber DLL_USBBoxGetDeviceSerialNumber;          // Function pointer
      static LPUSBBoxOpenDevice DLL_USBBoxOpenDevice;                                // Function pointer
      static LPUSBBoxCloseDevice DLL_USBBoxCloseDevice;                              // Function pointer
      static LPUSBBoxSetPortDOutputEnable DLL_USBBoxSetPortDOutputEnable;            // Function pointer
      static LPUSBBoxSetPortDOutputs DLL_USBBoxSetPortDOutputs;                      // Function pointer
      static LPUSBBoxGetPortDOutputs DLL_USBBoxGetPortDOutputs;                      // Function pointer
      static LPUSBBoxSetPortDOutputsExt DLL_USBBoxSetPortDOutputsExt;                // Function pointer
      static LPUSBBoxSetConfFpgaMode DLL_USBBoxSetConfFpgaMode;
      static LPUSBBoxLoadFirmware DLL_USBBoxLoadFirmware;
      static LPUSBBoxReadEprom DLL_USBBoxReadEprom;
      static LPUSBBoxWriteDevice DLL_USBBoxWriteDevice;
      static LPUSBBoxReadDevice DLL_USBBoxReadDevice;
      static LPUSBBoxNumberOfBytesAvailable DLL_USBBoxNumberOfBytesAvailable;

      static uint32_t m_isLibraryLoaded;
      static int32_t m_lastError;
      
      USBBoxDeviceRef m_hDevice; // Handle to LogicBox

};

#endif // FILE_CFX2_H