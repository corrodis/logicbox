#include "CSU735.h"

CSU735::CSU735(CCBUS* pCBus)
{
  m_pFile = new ofstream("out_adc_su735.txt");

  m_pCBus = pCBus;
}


CSU735::~CSU735()
{
        if (m_pFile->is_open())
                m_pFile->close();

        if (m_pFile != NULL)
                delete m_pFile;
}




int32_t CSU735::initClockCleaner(uint8_t div_ratio)
{
        int32_t ret;

        for(uint8_t i; i<7 ; i++)
                if (!ret)
                        I2C_Init_CC(i, div_ratio);
                else
                        return ret;

        return 0;
}


// set ADC output test pattern, 5 for 0101 -> 0101, 7 for 0000 -> 1111...

int32_t CSU735::setTestPattern(uint8_t nADC, uint8_t tp)
{

        uint8_t temp;

        if ((tp != ADC_TP_OFF) && (tp != ADC_TP_TOGGLE) && (tp != ADC_TP_10_01))
                return ERROR_ADC_TEST_PATERN_NOT_SUPPORTED;

        if (tp == ADC_TP_OFF)
                temp = 0;
        else if (tp == ADC_TP_10_01)
                temp = 5;
        else if (tp == ADC_TP_TOGGLE)
                temp = 7;

    return SPI_ADC_write(nADC, 4, temp*8);
}

int32_t CSU735::resetADCs(void)
{
        int32_t ret = 0;
        for (uint8_t ch=0; ch<7 ; ch++)
        {
                if (!ret)
                        ret = SPI_ADC_write(ch, 0, 0xFF);
                Sleep(1);
        }

        return ret;
}


int32_t CSU735::print_usage(void)
{
    printf("\n");
    printf("dl_i2c <parameters>\n");
    printf("The <parameters> are:\n");
    printf("--i2c <channel_mask> : init the Si5338 chips on all ADC channels with 1 in the mask\n");
    printf("--spi <channel_mask> <data> : send <data> over SPI to all ADC channels with 1 in the mask\n");
    printf("comments to angelov@physi.uni-heidelberg.de\n");
    return 0;
}

int32_t CSU735::SPI_ADC_print_reg(char prefix, uint8_t iaddr, uint8_t dat)
{
    float klvds;
    iaddr &= 7;
    klvds = 1;
    if (iaddr>4) iaddr=4;
    printf("%c",prefix);
    switch (iaddr)
    {
        case 0: {
                    if (dat >> 7) printf("Reset\n");
                    break;
                }
        case 1: {
                    switch(dat & 0x3)
                    {
                        case 0: { printf("Normal operation\n"); break;}
                        case 1: { printf("Nap mode\n"); break;}
                        case 2: { printf("Not used!!!\n"); break;}
                        case 3: { printf("Sleep mode\n"); break;}
                    }
                    break;
                }
        case 2: {
                    if ((dat >> 3) & 1) printf("Inverted clockout\n");
                    else                printf("Non-inverted clockout\n");

                    printf("%c",prefix);
                    if (dat & 1) printf("Clock duty cycle stabilizer ON\n");
                    else         printf("Clock duty cycle stabilizer OFF\n");

                    printf("%c",prefix);
                    printf("Clockout phase delay = %d�\n",((dat >> 1) & 3)*45);
                    break;
                }
        case 3: {
                    if ((dat >> 3) & 1) {printf("Internal LVDS termination ON\n"); klvds=1.6;}
                    else                 printf("Internal LVDS termination OFF\n");

                    printf("%c",prefix);
                    if ((dat >> 2) & 1) printf("Digital Outputs DISABLED\n");
                    else                printf("Digital Outputs ENABLED\n");

                    printf("%c",prefix);
                    switch(dat & 0x3)
                    {
                        case 0: { printf("Full-Rate CMOS Output Mode\n"); break;}
                        case 1: { printf("DDR LVDS Output Mode\n"); break;}
                        case 2: { printf("DDR CMOS Output Mode\n"); break;}
                        case 3: { printf("Not used Output Mode!!!\n"); break;}
                    }

                    printf("%c",prefix);
                    switch((dat >> 4) & 7)
                    {
                        case 0: {klvds *= 3.5; break;}
                        case 1: {klvds *= 4.0; break;}
                        case 2: {klvds *= 4.5; break;}
                        case 3: {klvds *= 0  ; break;}
                        case 4: {klvds *= 3.0; break;}
                        case 5: {klvds *= 2.5; break;}
                        case 6: {klvds *= 2.1; break;}
                        case 7: {klvds *= 1.75; break;}
                    }
                    printf("LVDS Output Current %0.2f mA\n",klvds);

                    break;
                }
        case 4:
                {
                    if ((dat >> 1) & 1) printf("Data Output Randomizer ON\n");
                    else                printf("Data Output Randomizer OFF\n");

                    printf("%c",prefix);
                    if ((dat >> 2) & 1) printf("Alternate Bit Polarity ON\n");
                    else                printf("Alternate Bit Polarity OFF\n");

                    printf("%c",prefix);
                    if ( dat       & 1) printf("Two's Complement Data Format\n");
                    else                printf("Offset Binary Data Format\n");

                    printf("%c",prefix);
                    switch((dat >> 3) & 7)
                    {
                        case 0: {printf("Digital Output Test Pattern OFF\n"); break;}
                        case 1: {printf("All Digital Outputs = 0\n"); break;}
                        case 3: {printf("All Digital Outputs = 1\n"); break;}
                        case 5: {printf("Checkerboard Output Pattern 0x1555 <-> 0x2AAA\n"); break;}
                        case 7: {printf("Alternating Output Pattern 0x0000 <-> 0x3FFF\n"); break;}
                        default: {printf("Digital Output Test Pattern UNKNOWN!\n"); break;}
                    }

                    break;


                }
    }

    return 0;
}


uint32_t CSU735::base_addr_i2c_cbus(uint8_t channel)
{
    channel &= 7;
    if (channel == 7) channel=6;
    return BASE_ADDR_I2C + channel;
}

uint32_t CSU735::base_addr_spi_cbus(uint8_t channel)
{
    channel &= 7;
    if (channel == 7) channel=6;
    return BASE_ADDR_SPI + channel;
}

int32_t CSU735::I2C_status(uint32_t base_addr)
{
    uint32_t cbus_dat;

        m_pCBus->read32(base_addr, &cbus_dat);
    if (i2c_debug)
    {
        if ((cbus_dat >> 25) & 1) printf("I2C Timeout!\n");
        if ((cbus_dat >> 24) & 1) printf("I2C ready!\n");
                            else  printf("I2C not ready!\n");
        if (((cbus_dat >> 16) & 0xFF) != 0) printf("State machines not in idle!\n");
    }
    return cbus_dat;
}

int32_t CSU735::I2C_ByteWrite(uint32_t base_addr, uint16_t reg_addr, uint16_t dat)
{
    uint32_t i2c_cmd, i2c_dat, i2c_addr, i2c_long, cbus_dat, err;

    i2c_cmd = 7;
    i2c_dat = ((dat & 0xFF) << 8) | (reg_addr & 0xFF);
    i2c_long = 1;
    i2c_addr = SLAVE_ADDR;
    cbus_dat = (i2c_cmd << 24) | (i2c_long << 27) | (i2c_addr << 16) | i2c_dat;

    m_pCBus->write32(base_addr, cbus_dat);

    err = 0;
    while ( ( ((I2C_status(base_addr) >> 24) & 1) == 0) && (err < 100))
    {
        err++;
        if (i2c_debug) printf("i2c_write_wait %2d\n",err);
    }
    return err;
}

uint8_t CSU735::I2C_ByteRead(uint32_t base_addr, uint16_t reg_addr)
{
    uint32_t i2c_cmd, i2c_dat, i2c_addr, i2c_long, cbus_dat, err;

    i2c_cmd = 7;
    i2c_dat = reg_addr & 0xFF;
    i2c_long = 0;
    i2c_addr = SLAVE_ADDR;
    cbus_dat = (i2c_cmd << 24) | (i2c_long << 27) | (i2c_addr << 16) | i2c_dat;

    err = m_pCBus->write32(base_addr, cbus_dat);
    err = 0;
    while ( ( ((I2C_status(base_addr) >> 24) & 1) == 0) && (err < 100))
    {
        err++;
        if (i2c_debug) printf("i2c_write_addr_wait %2d\n",err);
    }

    i2c_cmd = 0;
    i2c_dat = reg_addr & 0xFF;
    i2c_long = 0;
    i2c_addr = SLAVE_ADDR;
    cbus_dat = (i2c_cmd << 24) | (i2c_long << 27) | (i2c_addr << 16) | i2c_dat;

    m_pCBus->write32(base_addr, cbus_dat);

    cbus_dat = I2C_status(base_addr);
    while ( ( ((cbus_dat >> 24) & 1) == 0) && (err < 100))
    {
        err++;
        cbus_dat = I2C_status(base_addr);
        if (i2c_debug) printf("i2c_read_wait %2d\n",err);
    }
    return (cbus_dat & 0xFF);
}

// Get the register data, depending on the frequency.
REG_DATA CSU735::GetRegisterCC(uint16_t reg_nr, uint8_t div_ratio)
{
    REG_DATA *stable;
    int i;

    div_ratio &= 3; // 0 for 100 MHz, 1 for 50 Mhz, 2 for 33 MHz, 3 for 25 MHz

    if ((reg_nr > 255) || (div_ratio == 0)) 
		return Reg_Store[reg_nr]; // second register page or 100 MHz
    else
    {
        switch(div_ratio)
        {
        case 1: 
			stable = Reg_Store_50;
			break;
        case 2: 
			stable = Reg_Store_33;
			break;
        case 3: stable = Reg_Store_25;
			break;
        }
        for (i=0; i<NUM_REGS_SPEC; i++)
            if (stable[i].Reg_Addr == Reg_Store[reg_nr].Reg_Addr)
                return stable[i];
        return Reg_Store[reg_nr];
    }
}

int32_t CSU735::I2C_Init_CC(uint8_t channel, uint8_t div_ratio)

{



    uint16_t counter;
    uint32_t baddr;
    REG_DATA curr;
    uint8_t curr_chip_val, clear_curr_val, clear_new_val, combined, reg;

    baddr = base_addr_i2c_cbus(channel);

    I2C_ByteWrite(baddr, 230, 0x10);                   //OEB_ALL = 1
    I2C_ByteWrite(baddr, 241, 0xE5);                   //DIS_LOL = 1

    //for all the register values in the Reg_Store array
    //get each value and mask and apply it to the Si5338
    for(counter=0; counter<NUM_REGS_MAX; counter++)
    {

//        curr = Reg_Store[counter];
        curr = GetRegisterCC(counter, div_ratio);

        if(curr.Reg_Mask != 0x00)
        {
            if(curr.Reg_Mask == 0xFF)
            {
                // do a write transaction only
                // since the mask is all ones
                I2C_ByteWrite(baddr, curr.Reg_Addr, curr.Reg_Val);
            }
            else
            {
                //do a read-modify-write
                curr_chip_val = I2C_ByteRead(baddr, curr.Reg_Addr);
                clear_curr_val = curr_chip_val & ~curr.Reg_Mask;
                clear_new_val = curr.Reg_Val & curr.Reg_Mask;
                combined = clear_new_val | clear_curr_val;
                I2C_ByteWrite(baddr, curr.Reg_Addr, combined);
                if (i2c_debug)
                {
                    printf("addr %02x, read %02x, mask %02x, new %02x, comb %02x\n",
                    curr.Reg_Addr,
                    curr_chip_val, curr.Reg_Mask, curr.Reg_Val, combined);
                }
            }
        }
    }

    // check LOS alarm for the xtal input
    // on IN1 and IN2 (and IN3 if necessary) -
    // change this mask if using inputs on IN4, IN5, IN6
    reg = I2C_ByteRead(baddr, 218) & LOS_MASK;
    while(reg != 0)
    {
        reg = I2C_ByteRead(baddr, 218) & LOS_MASK;
    }

    I2C_ByteWrite(baddr, 49, I2C_ByteRead(baddr, 49) & 0x7F); //FCAL_OVRD_EN = 0
    I2C_ByteWrite(baddr, 246, 2);                      //soft reset
    I2C_ByteWrite(baddr, 241, 0x65);                   //DIS_LOL = 0

    // wait for Si5338 to be ready after calibration (ie, soft reset)
    Sleep(100);

    //make sure the device locked by checking PLL_LOL and SYS_CAL
    reg = I2C_ByteRead(baddr, 218) & LOCK_MASK;
    while(reg != 0)
    {
        reg = I2C_ByteRead(baddr, 218) & LOCK_MASK;
    }

    //copy FCAL values
    I2C_ByteWrite(baddr, 45, I2C_ByteRead(baddr, 235));
    I2C_ByteWrite(baddr, 46, I2C_ByteRead(baddr, 236));
    // clear bits 0 and 1 from 47 and
    // combine with bits 0 and 1 from 237
    reg = (I2C_ByteRead(baddr, 47) & 0xFC) | (I2C_ByteRead(baddr, 237) & 3);
    I2C_ByteWrite(baddr, 47, reg);

    I2C_ByteWrite(baddr, 49, I2C_ByteRead(baddr, 49) | 0x80); // FCAL_OVRD_EN = 1
    I2C_ByteWrite(baddr, 230, 0x00);                          // OEB_ALL = 0

    return 0;

}

int32_t CSU735::SPI_write(uint8_t channel, uint16_t dat)
{
    uint32_t cbus_dat, baddr;

    cbus_dat = dat;

    baddr = base_addr_spi_cbus(channel);

    return m_pCBus->write32(baddr, cbus_dat);

}

uint16_t CSU735::SPI_write_read(uint8_t channel, uint16_t datin)
{
    uint32_t cbus_dat, baddr;

    cbus_dat = datin;

    baddr = base_addr_spi_cbus(channel);

    m_pCBus->write32(baddr, cbus_dat);
    m_pCBus->read32(baddr+8, &cbus_dat);

    return (cbus_dat & 0xFFFF);
}

int32_t CSU735::SPI_ADC_write(uint8_t channel, uint8_t iaddr, uint8_t dat)
{
    uint16_t datin;
    iaddr &= 7;
    if (iaddr>4) iaddr=4;
    datin = (iaddr << 8) | dat;
    if (spi_debug)
    {
        printf("*** Writing 0x%02 to SPI register A%d of ADC %d\n",dat, iaddr, channel);
        SPI_ADC_print_reg(9, iaddr, dat);
    }
    return SPI_write(channel, datin);
}

int32_t CSU735::SPI_ADC_read(uint8_t channel, uint8_t iaddr)
{
    uint16_t datin;
    iaddr &= 7;
    if (iaddr>4) iaddr=4;
    datin = (iaddr << 8) | 0x8000;
    datin = SPI_write_read(channel, datin) & 0xFF;
    if (spi_debug)
    {
        printf("*** Reading 0x%02x from SPI register A%d of ADC %d\n",datin, iaddr, channel);
        SPI_ADC_print_reg(9, iaddr, datin);
    }
    return datin;
}

// 0..3 are valid for 100, 50, 33, 25 MHz
// #define DIV_100_MHZ 0
// #define DIV_50_MHZ  1
// #define DIV_33_MHZ  2
// #define DIV_25_MHZ  3
int32_t CSU735::check_pattern(uint8_t div_ratio)
{
    uint8_t adc_p = 0, dcm_p, channel, tp, nMeass;
    uint32_t dat[4], cnt[8];
        uint8_t ptxtBuffer[128];
        uint32_t pMeass[7][32];
        struct SSettings{
                uint8_t tp;
                uint8_t adc_p;
                uint8_t dcm_p;
        };
        struct SSettings settings[32];


    div_ratio &= 3; // 0..3 are valid for 100, 50, 33, 25 MHz

    // use real adc data and not the internally (in LBox FPGA) generated
    m_pCBus->write32(EB_EXT_CTRL, 0);

        for (channel=0; channel<7; channel++)
        {
                SPI_ADC_write(channel, 0, 0xFF);
                Sleep(1);
        }

        sprintf((char*)ptxtBuffer,"\n\nSearching the right phase for %i MHz\n", 100/(div_ratio+1));
        printf((char*)ptxtBuffer);
        if (m_pFile->is_open())
          (*m_pFile) << (char*)ptxtBuffer;

        sprintf((char*)ptxtBuffer,"tp adc_p dcm_p   adc0   adc1   adc2   adc3   adc4   adc5   adc6\n");
    printf((char*)ptxtBuffer);
        if (m_pFile->is_open())
          (*m_pFile) << (char*)ptxtBuffer;

        nMeass = 0;
    for (adc_p=0; adc_p < 8; adc_p++)
     for (dcm_p=0; dcm_p <= div_ratio; dcm_p++)
      for (tp=0; tp < 2; tp++)
      {
                settings[nMeass].adc_p = adc_p;
                settings[nMeass].dcm_p = dcm_p;
                settings[nMeass].tp = tp;

                // set frequency and phase
        m_pCBus->write32(DCM_CONF, CLK_DIVR1 + (div_ratio << 8) + (dcm_p << 10));
        Sleep(10);
        for (channel=0; channel<7; channel++)
        {
            // set ADC output clock phase
            SPI_ADC_write(channel, 2, 1+adc_p*2);
            // set ADC output test pattern, 5 for 0101 -> 0101, 7 for 0000 -> 1111...
            SPI_ADC_write(channel, 4, (5+2*tp)*8);
        }
        Sleep(10);
        // start test pattern check
        m_pCBus->write32(EVBUF_CMD, CMD_START_TP);
        Sleep(100);
        // read the data
                sprintf((char*)ptxtBuffer,"%d    %d     %d  ",tp, adc_p, dcm_p);
        printf((char*)ptxtBuffer);
                if (m_pFile->is_open())
                        (*m_pFile) << (char*)ptxtBuffer;

                //LPi_USB_BURST_RD(BA_EV_BUFFER, dat, 4, 4, 1);
                m_pCBus->readBurst(BA_EV_BUFFER, dat, 4, 4, 1);
        for (channel=0; channel<4; channel++)
        {
            cnt[channel*2  ]= dat[channel]        & 0xFFFF;
            cnt[channel*2+1]=(dat[channel] >> 16) & 0xFFFF;
        }
        for (channel=0; channel<7; channel++)
                {
                        sprintf((char*)ptxtBuffer,"%7d",cnt[channel]);
            //printf("%7d",cnt[channel]);
                        printf((char*)ptxtBuffer);
                        if (m_pFile->is_open())
                                (*m_pFile) << (char*)ptxtBuffer;

                        pMeass[channel][nMeass] = cnt[channel];

                }
                nMeass++;

                sprintf((char*)ptxtBuffer,"\n");
                printf((char*)ptxtBuffer);
                if (m_pFile->is_open())
                        (*m_pFile) << (char*)ptxtBuffer;
     }

        sprintf((char*)ptxtBuffer,"\n");
        printf((char*)ptxtBuffer);
        if (m_pFile->is_open())
                (*m_pFile) << (char*)ptxtBuffer;

        // reduce the table, add the error count for the same phase with the two test pattern
        //tp = 0 -> 0101 -> 1010
        //tp = 1 -> 0000 -> 1111
        for (channel=0; channel<7; channel++)
                for (uint8_t j=0; j<nMeass; j+=2)
                        pMeass[channel][j/2] = pMeass[channel][j] + pMeass[channel][j+1];


        for (uint8_t j=0; j<nMeass; j+=2)
        {
                settings[j/2].adc_p = settings[j].adc_p;
                settings[j/2].dcm_p = settings[j].dcm_p;
                settings[j/2].tp = 0;
        }

        nMeass = nMeass/2;

        // Find the best settings for the adc phase
        // for each channel
        uint8_t bestIndex[7];
        uint8_t pResult[7][32];
        uint8_t nNull, index;
        // Create a list with the number of consecutive 0 errors
        for (uint8_t i=0; i<7; i++)
        {
                for (uint8_t j=0; j<nMeass; j++)
                {
                        pResult[i][j] = 0;
                        nNull = 0;
                        if (pMeass[i][j] == 0)          {

                                for (uint8_t z=0; z<nMeass; z++)
                                {
                                        if ((z+j) < nMeass)
                                                index = (z+j);
                                        else
                                                index = (z+j-nMeass);
                                        if(pMeass[i][index] == 0)
                                                nNull++;
                                        else
                                                break;
                                }
                                pResult[i][j] = nNull;
                        }


                }

                //Find the maximum of nulls
                uint8_t maxIndex;
                maxIndex = 0;
                for (uint8_t j=1; j<nMeass; j++)
                        if (pResult[i][j] > pResult[i][maxIndex])
                                maxIndex = j;

                bestIndex[i] = maxIndex + pResult[i][maxIndex] / 2;
                if (bestIndex[i] > nMeass)
                        bestIndex[i] = bestIndex[i] -nMeass;

        }


        sprintf((char*)ptxtBuffer,"Best settings\nCh tp adc_p dcm_p\n");
        printf((char*)ptxtBuffer);
        if (m_pFile->is_open())
                (*m_pFile) << (char*)ptxtBuffer;
        for (channel=0; channel<7; channel++)
        {
                sprintf((char*)ptxtBuffer,"%i  %i    %i     %i\n",channel,
                        settings[bestIndex[channel]].tp,
                        settings[bestIndex[channel]].adc_p,
                        settings[bestIndex[channel]].dcm_p);
                printf((char*)ptxtBuffer);
                if (m_pFile->is_open())
                        (*m_pFile) << (char*)ptxtBuffer;

        }

        // Set the phase the best phase for each channel
        m_pCBus->write32(DCM_CONF, CLK_DIVR1 + (div_ratio << 8) + (settings[bestIndex[0]].dcm_p << 10));// set frequency and phase
        Sleep(10);
        for (channel=0; channel<7; channel++)
        {
                // set ADC output clock phase
                SPI_ADC_write(channel, 2, 1+settings[bestIndex[channel]].adc_p*2);
                // switch off the ADC output test pattern
                SPI_ADC_write(channel, 4, 0);
                if (settings[bestIndex[0]].dcm_p != settings[bestIndex[channel]].dcm_p)
                {
                        sprintf((char*)ptxtBuffer,"Warning: the best DCM phase is no the same for all channels\n");
                        printf((char*)ptxtBuffer);
                        if (m_pFile->is_open())
                                (*m_pFile) << (char*)ptxtBuffer;

                }

        }


        return 0;
}
// power down mode on/off
int32_t CSU735::powerDown(uint8_t nADC, uint8_t enable)
{
	/*
	if (on)
		on = 0x03;
	else
		on = 0;
    
	return SPI_write(channel, (0x0100|on));
	*/
	uint8_t u8Temp;

	u8Temp = SPI_ADC_read(nADC, LTC226X_POWER_DOWN_REGISTER);
	
	if (enable)
		u8Temp |= 0x03;
	else
		u8Temp &= ~0x03;

	return SPI_ADC_write(nADC, LTC226X_POWER_DOWN_REGISTER, u8Temp);
}


int32_t CSU735::setRandomizer(uint8_t nADC, uint8_t enable)
{
    uint8_t u8Temp;
	
	//Activate it in ADC
	u8Temp = SPI_ADC_read(nADC, LTC226X_DATA_FORMAT_REGISTER);

	if (enable)
		u8Temp |= 0x02;
	else
		u8Temp &= ~0x02;
    
	return SPI_ADC_write(nADC, LTC226X_DATA_FORMAT_REGISTER, u8Temp);

}
