/*
	USBBoxLib DLL Host Library - Windows XP
	---------------------------------------
	  
	USBBoxLib.h

	This is the main include file of the C Library DLL that
	interfaces to the 'LogicBox' or 'Cascade Neutron Detector'.

	---------------------------------------------------------------------------
	
	USBBoxLib Library for 'The Cascade Neutron Detector' and 'LogicBox'
	
	Department of Physics and Astronomy
	Dr. Martin Klein
	University Heidelberg, Germany
	
	Programmed by Fabian Renn 2007 f.renn@urz.uni-heidelberg.de
	
	---------------------------------------------------------------------------
	---------------------------------------------------------------------------
*/
#ifndef __USBBOX_USBBOXLIB_HEADER__
#define __USBBOX_USBBOXLIB_HEADER__

#ifndef DLLAPI
#define DLLAPI __declspec(dllimport)
#endif

#ifndef UINT
#include <Windows.h>
#endif

// USB Errors
enum {
	kUSBBoxNoError = 0,
	kUSBBoxSystemError = -1,
	kUSBBoxDeviceRemovedError = -2,
	kUSBBoxDeviceIsAlreadyOpen = -3,
	kUSBBoxDeviceNotOpen = -4,
	kUSBBoxDeviceTimedOut = -5,
	kUSBBoxDeviceWriteIOError = -6,
	kUSBBoxDeviceBufferOverrun = -7,
	kUSBBoxDeviceDoesNotSupportFeature = -8,
	kUSBBoxDeviceSetupStringTooLongForEPROM = -9
};

typedef struct {
	unsigned char data;
} USBBoxDeviceOpaque, * USBBoxDeviceRef;

/*
	INT USBBoxCountDevices( UINT * numberOfDevices )

	Counts all USBBox devices connected to the system,
	with a vendorID/productID combination of 0xDEAD/0xBEEF.

    If the function succeeds zero is returned otherwise
	an error code is returned.
*/
DLLAPI INT USBBoxCountDevices( UINT * numberOfDevices );

/*
	const CHAR * USBBoxGetDeviceSerialNumber( UINT index )

	Returns the device's unique serial number string.

	If the function fails or index is not a valid index
	then this functions returns NULL.
*/
DLLAPI const CHAR * USBBoxGetDeviceSerialNumber( UINT index );

/*
	
	INT USBBoxOpenDevice( UINT index, USBBoxDeviceRef * result )

	Opens the device with the index 'index'.

	On Failure this function will return the error code.
*/
DLLAPI INT USBBoxOpenDevice( UINT index, USBBoxDeviceRef * result );

/*
	INT USBBoxOpenDeviceWithSerial( const CHAR * serialNumber, USBBoxDeviceRef * result )

	Opens the device with a serial number.
*/
DLLAPI INT USBBoxOpenDeviceWithSerial( const CHAR * serialNumber, USBBoxDeviceRef * result );

/*
	INT USBBoxCloseDevice( USBBoxDeviceRef device )

	Close the device.
*/
DLLAPI INT USBBoxCloseDevice( USBBoxDeviceRef device );

/*
	INT USBBoxWriteDevice( USBBoxDeviceRef device, UINT bytes, void * buffer )

	This function writes bytes to the USBBox. Please note that this high level
	function has a default time out of 200 ms.
*/
DLLAPI INT USBBoxWriteDevice( USBBoxDeviceRef device, UINT bytes, void * buffer );

/*
	INT USBBoxNumberOfBytesAvailable( USBBoxDeviceRef device, UINT * bytesAvailable )

	This function returns the number of bytes available.
*/
DLLAPI INT USBBoxNumberOfBytesAvailable( USBBoxDeviceRef device, UINT * bytesAvailable );

/*
	INT USBBoxReadDevice( USBBoxDeviceRef device, UINT bytes, void * buffer )

	This function reads from the USBBox. This function will time out after 200 ms
	if it hasn't read all the data requested by bytes.
*/
DLLAPI INT USBBoxReadDevice( USBBoxDeviceRef device, UINT bytes, void * buffer );

/*
	INT USBBoxSetTimeOut( USBBoxDeviceRef device, UINT timeOutInMilliSeconds )

	Sets the time out value. A timeOut of zero means forever.
*/
DLLAPI INT USBBoxSetTimeOut( USBBoxDeviceRef device, UINT timeOutInMilliSeconds );

/*
	INT USBBoxSetStartupCommand( USBBoxDeviceRef device, UCHAR bytes, void * buffer )

	This command will only run on LogicBoxes that have a firmware that has
	startup command support enabled!!

	Following restrictions apply:
		command sequence can be no larger than 255 bytes!
*/
DLLAPI INT USBBoxSetStartupCommand( USBBoxDeviceRef device, USHORT bytes, void * buffer );

/*
	INT USBBoxSetPortDOutputEnable( USBBoxDeviceRef device, UCHAR OERegisterByte )

	This command will write to the output enable register.
*/
DLLAPI INT USBBoxSetPortDOutputEnable( USBBoxDeviceRef device, UCHAR OERegisterByte );

/* 
	INT USBBoxSetPortDOutputs( USBBoxDeviceRef device, UCHAR io_register );
	
	This command will set the IO register.
*/
DLLAPI INT USBBoxSetPortDOutputs( USBBoxDeviceRef device, UCHAR IO );

/*
	INT USBBoxGetPortDOutputs( USBBoxDeviceRef device, UCHAR * io_register )

	This command reads the IO register.
*/
DLLAPI INT USBBoxGetPortDOutputs( USBBoxDeviceRef device, UCHAR * io_register );

#endif /* __USBBOX_USBBOXLIB_HEADER__ */

