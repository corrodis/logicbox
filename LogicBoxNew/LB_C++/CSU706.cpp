#include "CSU706.h"

CSU706::CSU706(CCBUS* pCBus)
{
  m_pFile = new ofstream("out_adc_SU706.txt");
 
  m_pCBus = pCBus;
}


CSU706::~CSU706()
{
	if (m_pFile->is_open())
		m_pFile->close();

	if (m_pFile != NULL)
		delete m_pFile;
}

// ADC DLL on/off, must be on for > 60 MHz and off for < 60 MHz
int32_t CSU706::setDLL(uint16_t nADC, uint16_t on)
{
    on = on & 1;
    return usb_wadc_spi(nADC, (3 << 14) | (1 << 12) | (on << 1));
}

// test pattern 0..3, 0 is normal, 1 is all 0, 2 is all 1, 3 is toggle
// tp:
// ADC_TP_OFF			
// ADC_TP_ALL_ZEROS	
// ADC_TP_ALL_ONES		
// ADC_TP_TOGGLE		

int32_t CSU706::setTestPattern(uint16_t nADC, uint16_t tp)
{

	if ((tp != ADC_TP_OFF) && (tp != ADC_TP_ALL_ZEROS) && (tp != ADC_TP_ALL_ONES) && (tp != ADC_TP_TOGGLE))
		return ERROR_ADC_TEST_PATERN_NOT_SUPPORTED;

    tp = tp & 3;
    return usb_wadc_spi(nADC, (3 << 14) | (2 << 12) | (tp << 9));
}

// power down mode on/off
int32_t CSU706::powerDown(uint16_t nADC, uint16_t on)
{
    on = on & 1;
    return usb_wadc_spi(nADC, (3 << 14) | (3 << 12) | (on << 11));
}

// reset ALL ADCs! Generate a 2 us reset pulse
int32_t CSU706::resetADCs(void)
{
    return usb_wadc_spi(8, 0);
}


// write to the ADC SPI interface block, offset addres is the ADC #
// ADC # > 7 is reset
int32_t CSU706::usb_wadc_spi(uint16_t nADC, uint32_t confreg)
{
   #ifdef debug
   uint32_t L;
   printf("send 0x%04x to addr 0x%08x",confreg, ADC_SPI+nADC);
        m_pCBus->write32(ADC_SPI+nADC, confreg);
        m_pCBus->read32(ADC_SPI+nADC, &L);
   printf("   read back 0x%04x\n",L);
   return 0;
   #else
   return m_pCBus->write32(ADC_SPI+nADC, confreg);

   #endif
}