// $Id: main_DAQ.cpp 38 2013-04-04 14:13:19Z angelov $:

#include "CLogicBox.h"
#include "CCBUS.h"
#include "CDL709_7ADC_1TTL.h"


int main (int32_t argc, char *argv[])
{


   int hexflag = 1;
   INT ret, i, j, dll, adc;
   CDL709_7ADC_1TTL *pMyDL709;
   uint32_t L, led1, led2;
   uint16_t tp;
   CLogicBox  *pMyLogicBox;
   CCBUS  *pMyCBus;
   uint16_t nsamples, clkDivider;
   uint16_t eventSize;
   uint16_t nEvents;
   uint32_t Id, timeStamp, rorder_w, rorder_r;
   uint8_t myByte;

   pMyLogicBox = new CLogicBox;
   //LOGIC_BOX_LIST List;
   LOGIC_BOX_LIST* pList;
   pList = pMyLogicBox->getDevices();

   if (argc > 2)
   {

      int8_t k;

      nsamples = atoi(argv[1]);
      clkDivider = atoi(argv[2]);

      for (k = 0; k < 8;  k++)
      {
         if (EventSizeConversion[k] == nsamples)
            break;
      }

      if (EventSizeConversion[k] != nsamples)
      {
                  printf("Error: number of samples must be power of 2, between 16 and 2048!\n");
         system("pause");
         exit(0);
      }
   }
   else
   {

      nsamples = 16;
      clkDivider = 1;
   }

   printf("# Number of samples per event: %i\n", nsamples);
   printf("# Clock divider: %i\n" , clkDivider);

   if (pList->nDevices == 0)
   {
      printf("No device found\n");
      system("pause");
      return(0);
   }
   else if (pList->nDevices == 1)
   {
      printf("# Trying to open device %s\n", pList->pLogicBoxInfo[0].serialNumber);
      ret = pMyLogicBox->openDevice(pList->pLogicBoxInfo[0].deviceNumber);
      if (!ret)
         printf("# Device successfully opened\n");
      else
      {
         printf("Error opening device\n");
         system("pause");
         return(0);
      }

   }
   else if (pList->nDevices>1)
   {
      printf("# More than one device found:\n");

      for(int32_t i = 0; i < pList->nDevices;i++)
      {
         printf("# Device %i: %s -> Serial number %s\n", i,
               pList->pLogicBoxInfo[i].systemName,
               pList->pLogicBoxInfo[i].serialNumber);

      }

      int32_t index;
      while(1)
      {

         printf("# please select the device to be opened\n");
         scanf("%u",&index);
         if ((index <= pList->nDevices) && (index >= 0))
            break;
      }

      ret = pMyLogicBox->openDevice(pList->pLogicBoxInfo[index].deviceNumber);

      if (!ret)
         printf("# Device successfully opened\n");
      else
      {
         printf("Error opening device\n");
         system("pause");
         return(0);
      }

   }




   pMyCBus = pMyLogicBox->getCBusHandel();
   // create a new object
   pMyDL709 = new CDL709_7ADC_1TTL(pMyCBus);
   tp = 3;

   // read the Id Date & Time of compilation
   STRUCT_DATE compDate;

   pMyLogicBox->getCompilationDate(&compDate);
   printf("# Compile date yyyy/mm/dd %4d/%02d/%02d\n",compDate.year,
                                                  compDate.month,
                                                  compDate.day);

   printf("# Compile time hh:mm:ss %02d:%02d:%02d\n",compDate.hour,
                                                 compDate.min,
                                                 compDate.sec);

   // read the svn id : Date and Version Nr. of Subversion Id
   STRUCT_SVNID svnId;
   pMyLogicBox->getSvnID(&svnId);
   printf("# Subversion Ver. %d date yyyy/mm/dd %4d/%02d/%02d\n",
                                                      svnId.svn,
                                                      svnId.year,
                                                  svnId.month,
                                                  svnId.day);

   // Test the readout order
   rorder_w = 3 | (2 << 3) | (1 << 6) | (6 << 9) | (7 << 12) | (5 << 15) | (6 << 18);
   pMyDL709->setEventReadOrder(rorder_w);
   pMyDL709->getEventReadOrder(&rorder_r);
   printf("# Readout order: header");
   for (i=0; i<7; i++)
           if ((rorder_r >> (3*i) & 7) == 7) break;
           else printf(" => ch[%d]", (rorder_r >> (3*i)) & 0x7);
   printf("\n# ADC Mask = ");
   for (i=0; i<7; i++)
                printf(" %d", (rorder_r >> (i+24)) & 1);
   printf("\n# Number of active channels = %d\n", (rorder_r >> 21) & 7);

//   system("pause");

   pMyLogicBox->closeDevice();
   // free my object
   delete pMyDL709;

   return 0;


}
