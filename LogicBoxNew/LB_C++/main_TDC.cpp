// $Id: main.cpp 144 2012-08-16 13:15:07Z  $
#include "CLogicBox.h"
#include "CCBUS.h"
#include "CDL709_2TTL_TDC.h"

CDL709_2TTL_TDC *pMyDL709;
CLogicBox  *pMyLogicBox;

void tdc_test();
void tdc_dma_test();

int32_t getEventCounters(void)
{

   uint32_t nTotalEvents;
   uint32_t nReadEvents;
   int32_t ret;
   

   ret = pMyDL709->getNumberEventsStored(&nTotalEvents);
   if (!ret)
      ret = pMyDL709->getNumberEventsRead(&nReadEvents);
   if (!ret)
   {
      printf("\n");
      
      printf("Total captured events: %i\n", nTotalEvents);
      printf("Read events: %i\n", nReadEvents);
      printf("\n");
      
      
   }
   else
   {
      
      printf("Error getting the events counters, error code: %i\n", ret);
   }
   
   return ret; 
}

int32_t getNEvents(void)
{
   uint16_t nEvents;
   int32_t ret;
   // Check
   ret = pMyDL709->getNumberEvents(&nEvents);
   if(!ret)
   {
      printf("Number of events: %i\n", nEvents);
      return nEvents;
   }
   else
   {
      return ret;
      printf("Error getting the number of events, error code: %i\n", ret);
   }

}

int32_t getFPGAStatus(void)
{
   uint16_t nEvents;
   int32_t ret;
   uint8_t readEngine, readMux;
   bool bufferFull, bufferEmpty;

   // Check
   ret = pMyDL709->getFPGAEngineStatus(&nEvents, &readEngine, &readMux, &bufferFull, &bufferEmpty );
   if(!ret)
   {
      printf("\n");
      
      printf("Number of events: %i\n", nEvents);
      printf("Read engine state: %s\n", readEngineState2Text[readEngine]);
      printf("Read mux state: %s\n", readMuxState2Text[readMux]);
      if (bufferFull)
         printf("Buffer full\n");
      else
         printf("Buffer not full\n");
      if (bufferEmpty)
         printf("Buffer empty\n");
      else
         printf("Buffer not empty\n");
      
      printf("\n");
      
   }
   else
   {
      
      printf("Error getting the number of events, error code: %i\n", ret);
   }

   return ret;

}

void getIOMask(void)
{
   uint16_t invertMask, disableMask;
   int32_t ret;

   ret = pMyDL709->getIOMask(&disableMask, &invertMask);
   if(!ret)
   {
      printf("\n");
      
      printf("Enable Mask: 0x%03X\n", disableMask);
      printf("Invert Mask: 0x%03X\n", invertMask);
            
      printf("\n");
   }
   else
      printf("Error getting the IO masks, error code: %i\n", ret);
}


int main (int32_t argc, char *argv[])
{
	
   INT ret;

   
   CCBUS  *pMyCBus;


   pMyLogicBox = new CLogicBox;
   LOGIC_BOX_LIST* pList;
   pList = pMyLogicBox->getDevices();



   if (pList->nDevices == 0)
   {
      printf("No device found\n");
      system("pause");
      return(0);
   }
   else if (pList->nDevices == 1)
   {
      printf("Trying to open device %s\n", pList->pLogicBoxInfo[0].serialNumber);
      ret = pMyLogicBox->openDevice(pList->pLogicBoxInfo[0].deviceNumber);
      if (!ret)
         printf("Device successfully opened\n");
      else
      {
         printf("Error opening device\n");
         system("pause");
         return(0);
      }

   }
   else if (pList->nDevices>1)
   {
      printf("More than one device found:\n");

      for(int32_t i = 0; i < pList->nDevices;i++)
      {
         printf("Device %i: %s -> Serial number %s\n", i,
               pList->pLogicBoxInfo[i].systemName,
               pList->pLogicBoxInfo[i].serialNumber);

      }

      int32_t index;
      while(1)
      {

         printf("please select the device to be opened\n");
         scanf("%u",&index);
         if ((index <= pList->nDevices) && (index >= 0))
            break;
      }

      ret = pMyLogicBox->openDevice(pList->pLogicBoxInfo[index].deviceNumber);

      if (!ret)
         printf("Device successfully opened\n");
      else
      {
         printf("Error opening device\n");
         system("pause");
         return(0);
      }

   }




   pMyCBus = pMyLogicBox->getCBusHandel();
   // create a new object
   pMyDL709 = new CDL709_2TTL_TDC(pMyCBus);
   

   STRUCT_DATE compDate;
   pMyLogicBox->getCompilationDate(&compDate);
   printf("Compile date yyyy/mm/dd %4d/%02d/%02d\n",compDate.year,
                                                  compDate.month,
                                                  compDate.day);

   printf("Compile time hh:mm:ss %02d:%02d:%02d\n",compDate.hour,
                                                 compDate.min,
                                                 compDate.sec);

   
   getFPGAStatus();
   

   getIOMask();
   uint16_t disableMask = 0x000;
   uint16_t invertMask = 0x000;
   ret = pMyDL709->setIOMask(disableMask, invertMask);
   getIOMask();
 

   //tdc_test();
   tdc_dma_test();

   

   // close the device
   pMyLogicBox->closeDevice();
   
   // free my object
   delete pMyDL709;

   return 0;
}


void tdc_dma_test()
{
   INT ret;
   uint16_t pData[8192];
   
   
   uint16_t eventSize;
   uint32_t Id,timeStamp;
   uint16_t disableMask = 0x000;
   uint16_t invertMask = 0x000;


   printf("Reseting the FPGA buffer and enabling the TDC\n");
   pMyDL709->enableEventbuffer(true);
   pMyDL709->resetEventbuffer();

   getEventCounters();

   getFPGAStatus();
  
 
   
   // disable the hardware io to use the software trigger
   disableMask = 0x1FF;
   invertMask = 0x000;
   ret = pMyDL709->setIOMask(disableMask, invertMask);


   
   getFPGAStatus();

   // Check   
   if (getNEvents() < 0)
   {
      system("pause");
      printf("Error events in buffer != 0\n");
      pMyLogicBox->closeDevice();
      delete pMyDL709;
      return;
   }

  
   

   getFPGAStatus();
   getEventCounters();

	// read events from FPGA
   printf("reading all events from FPGA memory\n");
   
  // pMyDL709->getAllFpgaEvents();

	ret = pMyDL709->setDMA(TRUE);

	   printf("Sync the time stamp\n");
   pMyDL709->triggerSystem(0x100);
 
   printf("Forcing software trigger \n");
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0x02);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x04);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x08);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x10);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x20);
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0x40);
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0x80);
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0xFE);
   pMyDL709->triggerSystem(0x01);
	
   uint16_t FpgaEvents;
	ret = pMyDL709->getFpgaEventsDMA(&FpgaEvents);

	ret = pMyDL709->setDMA(FALSE);
   
	//getFPGAStatus();


   //Print out the events
   ret = 0;
   while (ret == 0)
   {
      eventSize = 7; // seven channels = 7 x 16-Bits words
      ret = pMyDL709->readMemoryEvent(&Id, &timeStamp, eventSize, pData);
      if (!ret)
         printf("Event ID: %i, timestamp: %u\n",Id,timeStamp);
      else
         printf("Error reading event\n");
      
      if (!ret)
      {
         printf(" CH0: %i\n",pData[0]);   
         printf(" CH1: %i\n",pData[1]);   
         printf(" CH2: %i\n",pData[2]);   
         printf(" CH3: %i\n",pData[3]);   
         printf(" CH4: %i\n",pData[4]);   
         printf(" CH5: %i\n",pData[5]);   
         printf(" CH6: %i\n",pData[6]);   
         
      }
   }

   ret = pMyDL709->setDMA(FALSE);
   system("pause");


}

void tdc_test()
{
   INT ret;
   uint16_t pData[8192];
   
   
   uint16_t eventSize;
   uint32_t Id,timeStamp;
   uint16_t disableMask = 0x000;
   uint16_t invertMask = 0x000;


   printf("Reseting the FPGA buffer and enabling the TDC\n");
   pMyDL709->enableEventbuffer(true);
   pMyDL709->resetEventbuffer();

   getEventCounters();

   getFPGAStatus();

 
   
   // disable the hardware io to use the software trigger
   disableMask = 0x1FF;
   invertMask = 0x000;
   ret = pMyDL709->setIOMask(disableMask, invertMask);

   printf("Sync the time stamp\n");
   pMyDL709->triggerSystem(0x100);
 
   printf("Forcing software trigger \n");
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0x02);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x04);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x08);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x10);
   pMyDL709->triggerSystem(0x01);
   
   pMyDL709->triggerSystem(0x20);
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0x40);
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0x80);
   pMyDL709->triggerSystem(0x01);

   pMyDL709->triggerSystem(0xFE);
   pMyDL709->triggerSystem(0x01);
   
   getFPGAStatus();

   // Check   
   if (getNEvents() < 0)
   {
      system("pause");
      printf("Error events in buffer != 0\n");
      pMyLogicBox->closeDevice();
      delete pMyDL709;
      return;
   }

  
   

   getFPGAStatus();
   getEventCounters();

	// read events from FPGA
   printf("reading all events from FPGA memory\n");
	
	pMyDL709->getAllFpgaEvents();
	pMyDL709->getAllFpgaEvents();
	pMyDL709->getAllFpgaEvents();
	pMyDL709->getAllFpgaEvents();

   getFPGAStatus();


   //Print out the events
   ret = 0;
   while (ret == 0)
   {
      eventSize = 7; // seven channels = 7 x 16-Bits words
      ret = pMyDL709->readMemoryEvent(&Id, &timeStamp, eventSize, pData);
      if (!ret)
         printf("Event ID: %i, timestamp: %u\n",Id,timeStamp);
      else
         printf("Error reading event\n");
      
      if (!ret)
      {
         printf(" CH0: %i\n",pData[0]);   
         printf(" CH1: %i\n",pData[1]);   
         printf(" CH2: %i\n",pData[2]);   
         printf(" CH3: %i\n",pData[3]);   
         printf(" CH4: %i\n",pData[4]);   
         printf(" CH5: %i\n",pData[5]);   
         printf(" CH6: %i\n",pData[6]);   
         
      }
   }

   system("pause");


}