// $Id: io.h 154 2012-08-23 15:57:06Z  $:
//
// ADCs : LED and Clock control
//
// Addresses
#define BA_LED_DCM   0x00000200
#define LED1N       (BA_LED_DCM + 0)
#define LED2N       (BA_LED_DCM + 1)
#define DCM_CONF    (BA_LED_DCM + 6)
//
// Data
//
// to increment the DCM phase just
// write DCM_CNTUP to DCM_CONF
#define DCM_CNTUP   ((1 <<  7) | 1)
//
// to decrement the DCM phase just
// write DCM_CNTDN to DCM_CONF
#define DCM_CNTDN    (1 <<  7)
//
// to set the div ratio and phase just
// write CLK_DIVRx | CLK_DIVPx to DCM_CONF
#define CLK_DIVR1    (1 << 15)
#define CLK_DIVR2   ((1 << 15) | (1 <<  8))
#define CLK_DIVR3   ((1 << 15) | (2 <<  8))
#define CLK_DIVR4   ((1 << 15) | (3 <<  8))
#define CLK_DIVP0    (1 << 15)
#define CLK_DIVP1   ((1 << 15) | (1 << 10))
#define CLK_DIVP2   ((1 << 15) | (2 << 10))
#define CLK_DIVP3   ((1 << 15) | (3 << 10))


//
// ADCs : CLKOUT counters
//
// Addresses
#define BA_CLKO_CNT  0x00000100
// Offset       Bits 7..0   15..8    23..16   31..24
//   0             ch 0      ch 1      ch 2     ch 3
//   1             ch 4      ch 5      ch 6     -

//
// ADCs : SPI
//
// Addresses
#define BA_SPI       0x00000000
#define SPI_ADCn     BA_SPI
#define SPI_ADC_RST (BA_SPI + 8)
//
// Data
//
// DLL in the ADC
#define DLL_OFF    ((3 << 14) | (1 << 12) | (1 << 1))
#define DLL_ON     ((3 << 14) | (1 << 12) | (0 << 1))
// in order to turn on/off the DLL just
// write DLL_Oxx to SPI_ADCn + <adc channel>
//
// Test pattern mode in ADC
#define TP_OFF     ((3 << 14) | (2 << 12) | (0 << 9))
#define TP_ON      ((3 << 14) | (2 << 12) | (3 << 9))
//
// in order to turn on/off the test pattern mode just
// write TP_Oxx to SPI_ADCn + <adc channel>
//
// Power down mode in ADC
#define PWR_ON     ((3 << 14) | (3 << 12) | (0 << 11))
#define PWR_OFF    ((3 << 14) | (3 << 12) | (1 << 11))
//
// in order to turn on/off the ADC just
// write PWR_Oxx to SPI_ADCn + <adc channel>
// It is recommended FIRST to reset the ADCs and then to change the necessary settings!

//
// ADCs : Test pattern checker
//
// Addresses
#define BA_EV_BUFFER 0x00010000
#define BA_TP_CHECK  BA_EV_BUFFER
// read the test pattern error counters of
//      - channel 2*n   at Bits 15.. 0 of address BA_TP_CHECK + n
//      - channel 2*n+1 at Bits 31..16 of address BA_TP_CHECK + n

#define EVBUF_CMD    (BA_EV_BUFFER+8+3)
// start test pattern check
#define CMD_START_TP (1 << 3)
// software trigger to store one event
#define CMD_SOFT_TRG (1 << 2)
// clear the event buffer
#define CMD_EV_CLR   (1 << 1)
// clear the read state machine
#define CMD_RSM_CLR  (1 << 4)
// clear the time stamp counter
#define CMD_TS_CLR   (1 << 0)
// in order to start the test pattern check just
// write CMD_START_TP to EVBUF_CMD
//
// in order to trigger one event by software just
// write CMD_SOFT_TRG to EVBUF_CMD
//
// in order to prepare the event buffer for a new run
// write CMD_EV_CLR | CMD_TS_CLR to EVBUF_CMD
//
// in case of lost sync between readout software and the hardware
// write CMD_RSM_CLR to EVBUF_CMD

#define EVBUF_STATUS (BA_EV_BUFFER+8+3)
// Status Register
//      mem_status read-only   32
//      bits  9.. 0 - number of events in event buffer
//      bits 15..13 - the state of the read state machine, for debugging:
//            0 r_idle | r_reset -- after reset and if event buffer empty
//            1 r_wait,          -- event buffer not empty, waiting for read request
//            2 r_head1,         -- send header 1
//            3 r_head2,         -- 2
//            4 r_head3,         -- 3
//            5 r_ram,           -- send ram data
//            6 r_inc_sampl,     -- inc the sample (read address)
//            7 r_check_end,     -- check if end of the event


#define EB_EXT_CTRL (BA_EV_BUFFER+8+4)
#define EB_EXT_TS_NINV  0
#define EB_EXT_TS_INV   2
#define EB_EXT_TS_OFF   (1 << 4)
//
#define EB_EXT_TRG_NINV  0
#define EB_EXT_TRG_INV   1
#define EB_EXT_TRG_OFF  (1 << 3)
//
#define EB_EXT_BUSY_NINV 0
#define EB_EXT_BUSY_INV  4
// to configure the external i/o control signals just
// write EB_EXT_TS_xxx | EB_EXT_TRG_xxx | EB_EXT_BUSY_xxx to EB_EXT_CTRL
//
#define EB_EV_SIZE  (BA_EV_BUFFER+8+5)
// in order to config the event size to be 2**N, where N=4..11 just
// write (N-4) to EB_EV_SIZE
#define EB_EV_PRES  (BA_EV_BUFFER+8+6)
// in order to config the number of presamples (0..1023) just
// write <presamples> to EB_EV_PRES
// if the event size < presamples, the upper bits of presamples are ignored

#define EB_RD_ORDER (BA_EV_BUFFER+8+7)
// write here the adc ch# to be read, as
// ch0 | (ch1 << 3) | (ch2 << 6) | (ch3 << 9) | (ch4 << 12) | (ch5 << 15) | (ch6 << 18),
// where ch0..6 are 0..6 for ADC channels 0..6 or 7 to mark the end of the packet.
// if e.g. ch3=7, ch4,5,6 are ignored and should be 7
// with ch0=2, ch1=0, ch2=5, ch3=7, the readout order is 2 => 0 => 5 => end

#define EB_RD_FIFO (BA_EV_BUFFER+16)
// read from there the complete events
//
// header 1     Bits 31.. 3 - event counter (starting from 0)
//              Bits  2.. 0 - the evnt_size coded as above
// header 2     Bits 31.. 0 - time stamp (with 10 ns resolution)
//
// header 3     bits  9.. 0 - number of events in the buffer, including the current one
//              bits 31..11 - ADC channel readout order (21 bits)
// data 0       Bits 15.. 0 - sample[0] of ADC[ch0]
//              Bits 31..16 - sample[1] of ADC[ch0]
// data 1       Bits 15.. 0 - sample[0] of ADC[ch1]
//              Bits 31..16 - sample[1] of ADC[ch1]
//...<all channels, samples 0..1>
//...<all channels, samples 2..3>
