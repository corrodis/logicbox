#ifndef FILE_CCBUS_H
#define FILE_CCBUS_H

#include "data_types.h"

class CCBUS  
{
    public:
      virtual int32_t write32(uint32_t Address, uint32_t Data) = 0;
	  virtual int32_t read32(uint32_t Address, uint32_t *Data)= 0;
	  virtual int32_t read32(uint32_t Address, uint32_t nData, uint8_t *pData) = 0;
	  virtual int32_t setDMA(bool ena)=0;
	  virtual int32_t read32DMA(uint32_t nData, uint8_t *pData) = 0;
      virtual int32_t getAvailableBytes(uint32_t *nAvaBytes)= 0;
	  virtual int32_t setAddress(uint32_t Address)= 0;
	  virtual int32_t readBurst(uint32_t addr, uint32_t *rdata, uint32_t nwords, uint32_t word_size, uint32_t aainc)=0;
	  virtual int32_t writeBurst(uint32_t addr, uint32_t *wdata, uint32_t nwords, uint32_t word_size, uint32_t aainc)=0;
};




#endif