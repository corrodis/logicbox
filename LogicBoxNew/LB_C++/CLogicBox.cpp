#include "CLogicBox.h"
#include <iostream>
#define debug 0
//TODO: free the allocated memory!!!

CLogicBox::CLogicBox()
{
   // Init all the variables
   m_subverID.day = 0;
   m_subverID.month = 0;
   m_subverID.year = 0;
   m_subverID.svn = 0;

   m_compilationDate.day = 0;
   m_compilationDate.hour = 0;
   m_compilationDate.min = 0;
   m_compilationDate.month = 0;
   m_compilationDate.sec = 0;
   m_compilationDate.year = 0;

   m_isLogicBoxOpened = false;
   m_List.pLogicBoxInfo = NULL;
   m_List.nDevices = -1;
        // Load the dynamic library of all cards

}

CLogicBox::~CLogicBox()
{


}

CCBUS* CLogicBox::getCBusHandel()
{

   return this;
}

int32_t CLogicBox::getSvnID(STRUCT_SVNID *svnID)
{
        if (m_isLogicBoxOpened)
        {
                svnID->day    = m_subverID.day;
                svnID->month  = m_subverID.month;
                svnID->svn    = m_subverID.svn;
                svnID->year   = m_subverID.year;
                return LB_SUCCESS;
        }
        else
                return LB_ERROR;


}

int32_t CLogicBox::getCardInfo(STRUCT_CARDINFO* cardInfo)
{
	if (m_isLogicBoxOpened)
	{
	    cardInfo->baseBoard = m_cardInfo.baseBoard;
		cardInfo->designId  = m_cardInfo.designId;
		cardInfo->revision  = m_cardInfo.revision;		
		return LB_SUCCESS;
	}
	else
		return LB_ERROR;

}

int32_t CLogicBox::getCompilationDate(STRUCT_DATE *compilationDate)
{
        if (m_isLogicBoxOpened)
        {
                compilationDate->day    = m_compilationDate.day;
                compilationDate->hour   = m_compilationDate.hour;
                compilationDate->min    = m_compilationDate.min;
                compilationDate->month  = m_compilationDate.month;
                compilationDate->sec    = m_compilationDate.sec;
                compilationDate->year   = m_compilationDate.year;

                return LB_SUCCESS;
        }
        else
                return LB_ERROR;


}

int32_t CLogicBox::openDevice(uint16_t index)
{
   int32_t ret;

   if (m_List.nDevices == -1)
      getDevices();

   if (m_List.nDevices < (int32_t) index)
      return LB_ERROR;
   if (m_List.pLogicBoxInfo[index].systemID == FX2_SYSTEM)
      m_pInterface = &m_FX2;
  // else if (m_List.pLogicBoxInfo[index].systemID == FTDI_SYSTEM)
  //    m_pInterface = &m_FTDI;

   // Check if already open
   if (m_pInterface->isOpen())
      m_pInterface->closeDevice();


   ret = m_pInterface->openDevice(m_List.pLogicBoxInfo[index].deviceNumber);

   if(!ret)
   {

      m_isLogicBoxOpened = true;
      UCHAR bbuf[4];
      uint32_t L;
      bbuf[0]='#';

      ret = m_pInterface->write_bytes(1,bbuf);
      if (!ret)
         ret = m_pInterface->read_bytes(4,bbuf);

      L = bbuf[0];
      L = (L << 8) | bbuf[1];
      L = (L << 8) | bbuf[2];
      L = (L << 8) | bbuf[3];
      if (!ret)
      {
         m_compilationDate.year  = (L >> 26) + 2000;
         m_compilationDate.month = (L >> 22) & 0xF;
         m_compilationDate.day   = (L >> 17) & 0x1F;
         m_compilationDate.hour  = (L >> 12) & 0x1F;
         m_compilationDate.min   = (L >>  6) & 0x3F;
         m_compilationDate.sec   =  L        & 0x3F;
      }


      if (SUBV_AVAIL)
      {
          bbuf[0]='V';

          ret = m_pInterface->write_bytes(1,bbuf);
          if (!ret)
             ret = m_pInterface->read_bytes(4,bbuf);

          L = bbuf[0];
          L = (L << 8) | bbuf[1];
          L = (L << 8) | bbuf[2];
          L = (L << 8) | bbuf[3];
          if (!ret)
          {
             m_subverID.year  = (L >> 26) + 2000;
             m_subverID.month = (L >> 22) & 0xF;
             m_subverID.day   = (L >> 17) & 0x1F;
             m_subverID.svn   =  L        & 0x1FFFF;
          }
      }

	  // get the Design Id
      bbuf[0]='P';

      ret = m_pInterface->write_bytes(1,bbuf);
      if (!ret)
         ret = m_pInterface->read_bytes(4,bbuf);

      if (!ret)
      {
		  m_cardInfo.baseBoard = bbuf[0];
		  m_cardInfo.designId = (uint16_t) (bbuf[1]<<8) + bbuf[2];
		  m_cardInfo.revision = bbuf[3];

	  }

   }

   return ret;


}
int32_t CLogicBox::closeDevice()
{

   if ((m_pInterface != NULL) && (m_pInterface->isOpen()))
   {
      m_pInterface->closeDevice();
      return LB_SUCCESS;
   }
   else
      return LB_NO_DEVICE_OPEN;
}

LOGIC_BOX_LIST * CLogicBox::getDevices()
{

        uint32_t nFTDI,nFX2, i;
   LOGIC_BOX_LIST *pList;
	
   nFTDI = 0;
   nFX2 = 0;
   pList = &m_List;

   // Allocate memory for FTDI and FX2
   if (m_FX2.m_hDLL != NULL){
		std::cout << "Getting devices" << std::endl;
	  std::cout << m_FX2.getNumberDevices(&nFX2) << std::endl;
   }
   //else
	//   return 0;
   std::cout << nFX2 << std::endl;
   //if (m_FTDI.m_hDLL != NULL)
	//	m_FTDI.getNumberDevices(&nFTDI);
   pList->nDevices = nFX2+nFTDI;
   if (m_List.nDevices > 0)
   {

      if(pList->pLogicBoxInfo != NULL)
      {
         free(pList->pLogicBoxInfo);
         pList->pLogicBoxInfo = NULL;
      }
      pList->pLogicBoxInfo = (LOGIC_BOX_INF*) malloc(sizeof(LOGIC_BOX_INF)*(pList->nDevices));
   }


   // For all FX2 cards try to find out all the information
   for (i = 0; i < nFX2 ; i++)
   {
      pList->pLogicBoxInfo[i].deviceNumber = i;
      m_FX2.getSerialNumber(i,(int8_t*)(pList->pLogicBoxInfo[i].serialNumber));
      pList->pLogicBoxInfo[i].systemID = FX2_SYSTEM;
      memcpy(&pList->pLogicBoxInfo[i].systemName, "DL706\0",6);

   }

   // For all FTDI cards try to find out all the information
  /* for (i = 0; i < nFTDI ; i++)
   {
      pList->pLogicBoxInfo[i+nFX2].deviceNumber = i;
      m_FTDI.getSerialNumber(i,(int8_t*)(pList->pLogicBoxInfo[i+nFX2].serialNumber));
      pList->pLogicBoxInfo[i].systemID = FTDI_SYSTEM;
      memcpy(&pList->pLogicBoxInfo[i+nFX2].systemName, "DL701\0",6);

   }*/



   return pList;
}


int32_t CLogicBox::getCBusRef(CCBUS* pCBus)
{


   return LB_SUCCESS;
}

int32_t CLogicBox::write32(uint32_t Address, uint32_t Data)
{
   uint8_t bbuf[10];
   int32_t ret;

   bbuf[0]='A';
   bbuf[4]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[3]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[2]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[1]=Address & 0xFF;
   bbuf[5]='L';
   bbuf[9]=Data & 0xFF;
   Data = Data >> 8;
   bbuf[8]=Data & 0xFF;
   Data = Data >> 8;
   bbuf[7]=Data & 0xFF;
   Data = Data >> 8;
   bbuf[6]=Data & 0xFF;


   //err=usb_write_bytes(10,bbuf);
   ret = m_pInterface->write_bytes(10,bbuf);
   return ret;

}

int32_t CLogicBox::read32(uint32_t Address, uint32_t *Data)
{

   uint8_t bbuf[6];
   uint32_t L;
   int32_t ret;

   bbuf[0]='A';
   bbuf[4]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[3]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[2]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[1]=Address & 0xFF;
   bbuf[5]='l';

   //err=usb_write_bytes(6,bbuf);
   ret = m_pInterface->write_bytes(6,bbuf);
   /*
   #ifdef debug
   printf("error code writing the address %d\n",ret);
   //LB_usb_nBytesAvailable(&L);
   m_pInterface->nBytesAvailable(&L);
   printf("Bytes av. %d\n",L);
   #endif
*/
   //err=usb_read_bytes(4,bbuf);
   ret = m_pInterface->read_bytes(4,bbuf);
  /*
   #ifdef debug
   printf("error code reading 4 bytes %d\n",ret);
   #endif
*/
   L = bbuf[0];
   L = (L << 8) | bbuf[1];
   L = (L << 8) | bbuf[2];
   L = (L << 8) | bbuf[3];
   *Data = L;

   return ret;

}
int32_t CLogicBox::setAddress(uint32_t Address)
{


   return LB_SUCCESS;
}

// Address: CBus Address
// nData: number of bytes to be read!!! bytes!!
// pData: pointer to the buffer
int32_t CLogicBox::read32(uint32_t Address, uint32_t nData, uint8_t *pData)
{

   uint8_t bbuf[9];
   int32_t ret;

   uint16_t nWords;
   nWords = nData/4;

   bbuf[0]='A';
   bbuf[4]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[3]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[2]=Address & 0xFF;
   Address = Address >> 8;
   bbuf[1]=Address & 0xFF;
   bbuf[5]='F';
   bbuf[7]=nWords & 0xFF; // nwords
   bbuf[6]=(nWords>>8) & 0xFF; //nWords
   bbuf[8]='l';

   // send the read command
   ret = m_pInterface->write_bytes(9,bbuf);

   // read the info in 32-Bit words
   ret = m_pInterface->read_bytes(4*nWords,pData);

   return ret;

}

int32_t CLogicBox::getAvailableBytes(uint32_t *nAvaBytes)
{

   return m_pInterface->nBytesAvailable(nAvaBytes);

}


int32_t CLogicBox::setDMA(bool ena)
{
   uint8_t bbuf[1];
   
   if (ena)
	bbuf[0] = 'Q';
   else
	bbuf[0] = 'q';

   return m_pInterface->write_bytes(1,bbuf);
   
}

// nData: number of bytes to be read!!! bytes!!
// pData: pointer to the buffer
int32_t CLogicBox::read32DMA(uint32_t nData, uint8_t *pData)
{
   int32_t ret;
   
   // try to read the info in 32-Bit words
   ret = m_pInterface->read_bytes(nData,pData);
   
   return ret;
   
}


int32_t CLogicBox::readBurst(uint32_t addr, uint32_t *rdata, uint32_t nwords, uint32_t word_size, uint32_t aainc)
{

    uint8_t pBuffer[0x10000];
    uint32_t avaBytes, L, bytes2read;
    int32_t ret, pos, i;

    if (debug)
    {
        printf("Burst read: Address 0x%08x Number of words %d  Word size %d   AddrAinc %d\n",addr, nwords, word_size, aainc);
        printf("Buffer size 0x%04x, size of packet requested 0x%x\n",sizeof(pBuffer), nwords*word_size);
    }

    if (nwords*word_size > sizeof(pBuffer))
    {
        printf("Buffer size 0x%04x is smaller than the packet requested 0x%x\n",sizeof(pBuffer), nwords*word_size);
        return -1;
    }

    if (nwords > 0)
    {
        pos = 0;

        pBuffer[pos++] = 'A';
        pBuffer[pos++] = (addr >> 24) & 0xFF; // MSByte
        pBuffer[pos++] = (addr >> 16) & 0xFF;
        pBuffer[pos++] = (addr >>  8) & 0xFF;
        pBuffer[pos++] =  addr        & 0xFF; // LSByte

        if (aainc)
            pBuffer[pos++] = 'N';
        else
            pBuffer[pos++] = 'F';

        pBuffer[pos++] = (nwords >>  8) & 0xFF;
        pBuffer[pos++] =  nwords        & 0xFF; // LSByte

        if (word_size==4) pBuffer[pos++] = 'l';
        else
        if (word_size==3) pBuffer[pos++] = 't';
        else
        if (word_size==2) pBuffer[pos++] = 'w';
        else
        if (word_size==1) pBuffer[pos++] = 'b';

        if (debug)
        {
            for (i=0; i<pos; i++) printf("%c",pBuffer[i]);
            printf("\n");
        }

        //ret = LPi_usb_write_bytes(pos, pBuffer);
		m_pInterface->write_bytes(pos, pBuffer);
        bytes2read = nwords*word_size;
        if (debug)
        {
            //LPi_usb_nBytesAvailable(&avaBytes);
			m_pInterface->nBytesAvailable(&avaBytes);
            printf("Bytes availables %i, expected %d\n\n", avaBytes, bytes2read);
        }
    }
    else
        {
            //LPi_usb_nBytesAvailable(&avaBytes);
			m_pInterface->nBytesAvailable(&avaBytes);
            bytes2read = avaBytes;
            nwords = bytes2read/word_size;
            if (debug)
                printf("Bytes availables %i\n\n", avaBytes);

            *rdata = nwords;
            rdata++;
        }


    //ret = LPi_usb_read_bytes(bytes2read, pBuffer);
	ret = m_pInterface->read_bytes(bytes2read, pBuffer);

    pos = 0;
    for (i=0; i<nwords; i++)
    {
        L = 0;
        if (word_size > 3) L = pBuffer[pos++];
        if (word_size > 2) L = (L << 8) | pBuffer[pos++];
        if (word_size > 1) L = (L << 8) | pBuffer[pos++];
                           L = (L << 8) | pBuffer[pos++];
        if (debug) printf("%3d   0x%08x\n",i,L);
        *rdata = L;
        rdata++;
    }

    return ret;



}


int32_t CLogicBox::writeBurst(uint32_t addr, uint32_t *wdata, uint32_t nwords, uint32_t word_size, uint32_t aainc)
{

  UCHAR pBuffer[4*0x10000+8];
    INT ret, pos, i;

    pos = 0;
    if (debug)
        printf("Burst write: Address 0x%08x Number of words %d  Word size %d   AddrAinc %d\n",addr, nwords, word_size, aainc);

    pBuffer[pos++] = 'A';
    pBuffer[pos++] = (addr >> 24) & 0xFF; // MSByte
    pBuffer[pos++] = (addr >> 16) & 0xFF;
    pBuffer[pos++] = (addr >>  8) & 0xFF;
    pBuffer[pos++] =  addr        & 0xFF; // LSByte

    if (aainc)
        pBuffer[pos++] = 'N';
    else
        pBuffer[pos++] = 'F';

    pBuffer[pos++] = (nwords >>  8) & 0xFF;
    pBuffer[pos++] =  nwords        & 0xFF; // LSByte

    if (word_size==4) pBuffer[pos++] = 'L';
    else
    if (word_size==3) pBuffer[pos++] = 'T';
    else
    if (word_size==2) pBuffer[pos++] = 'W';
    else
    if (word_size==1) pBuffer[pos++] = 'B';

    for (i=0; i<nwords; i++)
    {
        if (word_size > 3) pBuffer[pos++] = (*wdata >> 24) & 0xFF; // MSByte
        if (word_size > 2) pBuffer[pos++] = (*wdata >> 16) & 0xFF;
        if (word_size > 1) pBuffer[pos++] = (*wdata >>  8) & 0xFF;
                           pBuffer[pos++] =  *wdata        & 0xFF; // LSByte
        wdata++;
    }

    if (debug)
    {
        for (i=0; i<pos; i++) printf("%c",pBuffer[i]);
        printf("\n");
    }

    //ret = LPi_usb_write_bytes(pos, pBuffer);
	ret = m_pInterface->write_bytes(pos, pBuffer);
    if (debug)
        printf("Return code %d\n",ret);
    return ret;

}