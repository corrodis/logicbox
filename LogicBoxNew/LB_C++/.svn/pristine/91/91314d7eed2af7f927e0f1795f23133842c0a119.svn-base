#include "CDL709_7ADC_1TTL.h"

CDL709_7ADC_1TTL::CDL709_7ADC_1TTL(CCBUS *pCBus)
{
   m_pCBus = pCBus;

   // try to allocate the memory for the particle event
   m_parBuffer.nChannels = 7;
   m_parBuffer.channelsMask = 0x7F;
   m_parBuffer.eventSize  = PARTICLE_EVENT_FPGA_DEFAULT_SIZE * m_parBuffer.nChannels +
                             PARTICLE_EVENT_FPGA_HEADER_SIZE; // 16 Samples x 2 bytes
   m_parBuffer.eventOffset  = m_parBuffer.eventSize + sizeof(PARTICLE_EVENT);
   
   m_parBuffer.nTotalEvents = PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE / m_parBuffer.eventOffset;
   
   m_parBuffer.nEvents = 0;
   m_parBuffer.currentPosEvent = 0;
   m_parBuffer.lostEvents = 0;   
   m_parBuffer.pData = (uint8_t*) malloc(PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);
   m_pRawData = (uint8_t*) malloc(m_parBuffer.eventSize*m_parBuffer.nTotalEvents);

   m_parBuffer.writeAddress = m_parBuffer.pData ;
   m_parBuffer.readAddress = m_parBuffer.pData ;
   m_parBuffer.endAddress = (m_parBuffer.pData + PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);
   
   
}
CDL709_7ADC_1TTL::~CDL709_7ADC_1TTL(){}


int32_t CDL709_7ADC_1TTL::usb_write_long(uint32_t Address, uint32_t Data)
{
   return m_pCBus->write32(Address,Data);
}

int32_t CDL709_7ADC_1TTL::usb_read_long(uint32_t Address, uint32_t* Data)
{

   return m_pCBus->read32(Address,Data);
}

// write to the ADC SPI interface block, offset addres is the ADC #
// ADC # > 7 is reset
int32_t CDL709_7ADC_1TTL::usb_wadc_spi(uint16_t adcnr, uint32_t confreg)
{
   #ifdef debug
   uint32_t L;
   printf("send 0x%04x to addr 0x%08x",confreg, ADC_SPI+adcnr);
        LB_m_pCBus->write32(ADC_SPI+adcnr, confreg);
        LB_m_pCBus->read32(ADC_SPI+adcnr, &L);
   printf("   read back 0x%04x\n",L);
   return 0;
   #else
   return m_pCBus->write32(ADC_SPI+adcnr, confreg);

   #endif
}

// ADC DLL on/off, must be on for > 60 MHz and off for < 60 MHz
int32_t CDL709_7ADC_1TTL::usb_adc_dll(uint16_t adcnr, uint16_t on)
{
    on = on & 1;
    return usb_wadc_spi(adcnr, (3 << 14) | (1 << 12) | (on << 1));
}

// test pattern 0..3, 0 is normal, 1 is all 0, 2 is all 1, 3 is toggle
int32_t CDL709_7ADC_1TTL::usb_adc_tp(uint16_t adcnr, uint16_t tp)
{
    tp = tp & 3;
    return usb_wadc_spi(adcnr, (3 << 14) | (2 << 12) | (tp << 9));
}

// power down mode on/off
int32_t CDL709_7ADC_1TTL::usb_adc_pwrdn(uint16_t adcnr, uint16_t on)
{
    on = on & 1;
    return usb_wadc_spi(adcnr, (3 << 14) | (3 << 12) | (on << 11));
}

// reset ALL ADCs! Generate a 2 us reset pulse
int32_t CDL709_7ADC_1TTL::usb_adcs_reset(void)
{
    return usb_wadc_spi(8, 0);
}

// measure the CLKOUT from the ADCs in the FPGA synchron with the system clock
// the transition from 0 to 127 is the best region to sample the ADC data
int32_t CDL709_7ADC_1TTL::usb_adc_DCM_scan(uint16_t dll_on)
{
    uint32_t L, led1, led2, adc;
    int32_t i, err;

    err = 0;
    led1 = 0xAA;
    led2 = 0x55;

    for (i=0; i<7; i++)
        if ((adc_present >> i) & 1)
            err += usb_adc_dll(i,dll_on); // dll
    // DCM reset
    #ifdef debug
    printf("reset DCM\n");
    #endif
    err += m_pCBus->write32(ADC_LED+6, 1+128);
    Sleep(100);

    for (i=-255; i<256; i++)
    {

        // clear & start test pattern
        err += m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_COMMAND_REGISTER, START_TESTPATERN);
        Sleep(1);

        // read the CLKOUT counters
        err += m_pCBus->read32(ADC_CLK, &L);
        printf("%3d %3d %3d %3d %3d",i, L & 0xFF, (L >> 8) & 0xFF, (L >> 16) & 0xFF, (L >> 24) & 0xFF);

        err += m_pCBus->read32(ADC_CLK+1, &L);
        printf(" %3d %3d %3d %3d",L & 0xFF, (L >> 8) & 0xFF, (L >> 16) & 0xFF, (L >> 24) & 0xFF);

        err += m_pCBus->read32(ADC_LED+6, &L);
        printf(" 0x%02x",L & 0xFF);

        // read the error pattern counters
        for (adc=0; adc<7; adc++)
        {
            m_pCBus->read32(ADC_MEM+adc, &L);
            printf(" %10d",L);
        }
        printf("\n");

        // UP
        err += m_pCBus->write32(ADC_LED+6,  2+128);
        // toglle LEDs
        err += m_pCBus->write32(ADC_LED,   led1);
        err += m_pCBus->write32(ADC_LED+1, led2);
        // reset the sync
        err += m_pCBus->write32(ADC_LED+6, (1 << 15) | (div_phase << 10) | (div_ratio << 8));
        err += m_pCBus->write32(ADC_LED+6, (1 << 15) | (div_phase << 10) | (div_ratio << 8));
        led1 = ~led1;
        led2 = ~led2;
//        Sleep(10);
    }
    return err;
}


// turn on and configure the adc_present
int32_t CDL709_7ADC_1TTL::usb_adc_init(uint32_t new_adc_rorder,
                                   uint16_t new_adc_dll_on,
                                   uint16_t new_adc_tp,
                                   int16_t new_dcm_phase,
                                   uint16_t new_div_ratio,
                                   uint16_t new_div_phase)
{
    int16_t i, err;
    uint32_t L, mask3b;

    if (new_div_ratio>4) new_div_ratio=4;
    if (new_div_ratio<1) new_div_ratio=1;
    adc_tp = new_adc_tp & 3;
    div_ratio = new_div_ratio-1;
    div_phase = new_div_phase & 0x3;
    err = 0;
    adc_present = 0;
    adc_rorder = new_adc_rorder;
    mask3b = 0;
    for (i=0; i<7; i++)
    {
        if ((new_adc_rorder & 0x7) < 7)
            adc_present |= (1 << (new_adc_rorder & 0x7));
        else
            mask3b = 7;
        new_adc_rorder = new_adc_rorder >> 3;
        adc_rorder = adc_rorder | (mask3b << (3*i) );
    }
    err += m_pCBus->write32(EB_RD_ORDER, adc_rorder);
    adc_dll_on  = new_adc_dll_on & 1;
    // ADCs reset
    err += usb_adcs_reset();
    // DCM reset
    #ifdef debug
    printf("reset DCM\n");
    #endif
    err += m_pCBus->write32(ADC_LED+6, 1);
    #ifdef debug
    printf("Adjust DCM phase to %d...\n", new_dcm_phase);
    #endif
    for (dcm_phase = -255; dcm_phase < new_dcm_phase; dcm_phase++)
    {
        err += m_pCBus->write32(ADC_LED+6, 2+128); // INC
        i = 0;
        do
        {
            // read back status
            err += m_pCBus->read32(ADC_LED+6, &L);
            i++;
        } while ((((L >> 4) & 1) == 0) && (i < 10));
        err += m_pCBus->write32(ADC_LED+6, 2+128); // INC
        #ifdef debug
        printf("dcm adjust %4d 0x%02x %2d\n",dcm_phase, L, i);
        #endif
    }
    // set the ADC mask, turn the clocks on
    #ifdef debug
    printf("Set ADC mask to 0x%02x\n", adc_present);
    #endif
    err += m_pCBus->write32(ADC_LED+7, adc_present);

    for (i=0; i<7; i++)
    if ( (adc_present >> i) & 1)
    {
        err += usb_adc_pwrdn(i,0); // power down off
        err += usb_adc_dll(i,adc_dll_on); // dll
        err += usb_adc_tp(i,adc_tp);      // test pattern mode off
    }
    else
        err += usb_adc_pwrdn(i,1); // power down off

    err += m_pCBus->write32(ADC_LED+6, (1 << 15) | (div_phase << 10) | (div_ratio << 8));

    return err;
}

int32_t CDL709_7ADC_1TTL::usb_adc_init(uint32_t new_adc_rorder,
                                   uint16_t new_adc_tp,
                                   uint16_t new_div_ratio)
{
    uint16_t new_adc_dll_on;
    int16_t new_dcm_phase;
    uint16_t new_div_phase;

    new_div_ratio = ((new_div_ratio-1) & 3) + 1;

    switch (new_div_ratio)
    {
    case 1:
    {
        new_adc_dll_on = 1;
        new_div_phase  = 0;
        new_dcm_phase  = 70;
        break;
    }
    case 2:
    {
        new_adc_dll_on = 0;
        // working combinations
        // div_phase    dcm_phase
        // 0            any
        // 1            50
        new_div_phase  = 0;
        new_dcm_phase  = 50;
        break;
    }
    case 3:
    {
        new_adc_dll_on = 0;
        // working combinations
        // div_phase    dcm_phase
        // 0            any
        // 1            25
        // 2            -100
        new_div_phase  =  0;
        new_dcm_phase  = 25;
        break;
    }
    case 4:
    {
        new_adc_dll_on = 0;
        // working combinations
        // div_phase    dcm_phase
        // 0            0
        // 1            any
        // 2            any
        // 3            30
        new_div_phase  =  1;
        new_dcm_phase  = 10;
        break;
    }
    }

    return usb_adc_init(new_adc_rorder, new_adc_dll_on, new_adc_tp, new_dcm_phase, new_div_ratio, new_div_phase);
}

int32_t CDL709_7ADC_1TTL::usb_adc_init(uint32_t new_adc_rorder,
                                   uint16_t new_div_ratio)
{
    return usb_adc_init(new_adc_rorder, 0, new_div_ratio);
}

/*
TIMESTAMP_CLEAR    
EVENT_CLEAR        
SOFT_TRIGGER       
START_TESTPATERN   
RESET_STATE_MACHINE
*/
int32_t CDL709_7ADC_1TTL::setCommandRegister(uint8_t value)
{
    int32_t err;

     err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_COMMAND_REGISTER, value);

    return err;
}


         
int32_t CDL709_7ADC_1TTL::getNumberEvents(uint16_t *nEvents)
{

   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);
   
   *nEvents = (reg&0x3FF);

   return err;
}

int32_t CDL709_7ADC_1TTL::getNumberEventsForReading(uint8_t *nEvents)
{
    
   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);
   
   *nEvents = ((reg>>24)&0xFF);

   return err;
}

int32_t CDL709_7ADC_1TTL::getNumberEventsForWriting(uint8_t *nEvents)
{
    
   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);
   
   *nEvents = ((reg>>16)&0xFF);

   return err;
}

int32_t CDL709_7ADC_1TTL::getReadStateMachine(uint8_t *state)
{
   
   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_STATUS_REGISTER, &reg);
   
   *state = ((reg>>13)&0x7);
   
 
   return err;
   
   
}
int32_t CDL709_7ADC_1TTL::getConfigurationRegister(uint8_t *value)
{
    
   int32_t err;
   uint32_t reg;

   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, &reg);
   
   *value = ((reg)&0x1F);

   return err;
}

/*
      #define INVERT_TRIGGER     (0x01<<0)
      #define INVERT_TIME_SYNC   (0x01<<1)
      #define INVERT_BUSY        (0x01<<2)
*/
int32_t CDL709_7ADC_1TTL::setConfigurationRegister(uint8_t value)
{
   
   int32_t err;
   //TODO: check the unput value 
   value &= 0x1F;
   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_CONFIGURATION_REGISTER, value);
   

   return err;
}

/*
      #define SAMPLES_16      0         
      #define SAMPLES_32      1
      #define SAMPLES_64      2
      #define SAMPLES_128     3
      #define SAMPLES_256     4      
      #define SAMPLES_512     5
      #define SAMPLES_1024    6
      #define SAMPLES_2048    7
*/
int32_t CDL709_7ADC_1TTL::setEventSize(uint8_t value)
{
    
   int32_t err;
   //TODO: check the input value

   if (value > 7)
      return LB_ERROR;

   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_EVENT_SIZE_REGISTER, value);
   updateReadBuffer();

   return err;
}

void CDL709_7ADC_1TTL::updateReadBuffer(void)
{


   //Clear the FPGA buffer and reset the state machine
   setCommandRegister(EVENT_CLEAR | RESET_STATE_MACHINE);

   // Set the new buffer parameters
   m_parBuffer.eventSize  = PARTICLE_EVENT_FPGA_DEFAULT_SIZE * m_parBuffer.nChannels +
                             PARTICLE_EVENT_FPGA_HEADER_SIZE; // 16 Samples x 2 bytes
   m_parBuffer.eventOffset  = m_parBuffer.eventSize + sizeof(PARTICLE_EVENT);
   
   m_parBuffer.nTotalEvents = PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE / m_parBuffer.eventOffset;
   m_parBuffer.nEvents = 0;
   m_parBuffer.currentPosEvent = 0;
   m_parBuffer.lostEvents = 0;   
   m_parBuffer.writeAddress = m_parBuffer.pData ;
   m_parBuffer.readAddress = m_parBuffer.pData ;
   m_parBuffer.endAddress = (m_parBuffer.pData + PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE);
}

int32_t CDL709_7ADC_1TTL::getEventSize(uint8_t *value)
{
    
   int32_t err;
   //TODO: check the input value
   uint32_t u32Temp;
   
   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_EVENT_SIZE_REGISTER, &u32Temp);
   *value = (u32Temp&0x07);

   return err;
}

int32_t CDL709_7ADC_1TTL::setEventpresamples(uint16_t value)
{
    
   int32_t err;
   //TODO: check the input value
   value &= 0x3FF;
   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_PRESAMPLES_REGISTER, value);
   

   return err;
}


int32_t CDL709_7ADC_1TTL::disableHWTrigger(bool disable)
{
   int32_t err;
   uint8_t value;

   
   err = getConfigurationRegister(&value);
   if (err)
      return err;

   if (disable)
      value |= 0x08;
   else
      value &= (~0x08);
   
   err = setConfigurationRegister(value);
   if (err)
      return err;
   
   return err;
   
   
}
int32_t CDL709_7ADC_1TTL::setEventReadOrder(uint32_t value)
{
    
   int32_t err;
   uint32_t u32temp;
   
   //check the input value
   if (value>0xFFFFFF)
      return LB_ERROR;

   u32temp = value;
   m_parBuffer.nChannels = 0;
   m_parBuffer.channelsMask = 0;
   for(uint8_t i=0;i<7;i++)
   {
      if ((u32temp&0x07)!=STOP)
      {
         m_parBuffer.nChannels++;         
         m_parBuffer.channelsMask |= (0x01<<(u32temp&0x07));
         u32temp >>=3;
      }
      else
         break;      
   }
   
   err = disableHWTrigger(true);
   if (err)
      return err;

   updateReadBuffer();
   err = m_pCBus->write32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_READ_ORDER_REGISTER, value);
   

   return err;
}

int32_t CDL709_7ADC_1TTL::getEventReadOrder(uint32_t *value)
{
    
   int32_t err;
   //TODO: check the input value
   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_CONTROL+ADDR_READ_ORDER_REGISTER, value);
   (*value) &= 0x1FFFFF;
   
   return err;
}



int32_t CDL709_7ADC_1TTL::getAllFpgaEvents()
{
   
   uint16_t nEvents,FpgaEvents;
   getNumberEvents(&nEvents);
   if (nEvents>0)
      getFpgaEvents(nEvents, &FpgaEvents);
   if (FpgaEvents>0)
      getFpgaEvents(FpgaEvents, &FpgaEvents);
   

   return LB_SUCCESS;   
}

int32_t CDL709_7ADC_1TTL::readMemoryEvent( uint32_t *Id,uint32_t *timeStamp, uint16_t nData, uint8_t *pData)
{
   
   int32_t err;   

   if (nData != (m_parBuffer.eventSize - PARTICLE_EVENT_FPGA_HEADER_SIZE))
      return LB_ERROR;
   if (m_parBuffer.nEvents == 0)
      return LB_ERROR;

   PARTICLE_EVENT *pEvent;
   pEvent = (PARTICLE_EVENT*) m_parBuffer.readAddress;

   *Id = pEvent->eventCounter;
   *timeStamp = pEvent->timeStamp;
   memcpy(pData,pEvent->pData, m_parBuffer.eventSize-PARTICLE_EVENT_FPGA_HEADER_SIZE);
   m_parBuffer.readAddress += m_parBuffer.eventOffset;
   if (m_parBuffer.readAddress>=m_parBuffer.endAddress)
      m_parBuffer.readAddress = m_parBuffer.pData;
   
   m_parBuffer.nEvents--;

   return LB_SUCCESS;

}

int32_t CDL709_7ADC_1TTL::getFpgaEvents(uint16_t Events2Read, uint16_t *FpgaEvents)
{
    

   int32_t err;   
   

   
   
   PARTICLE_EVENT *pEvent;
   
   if (m_parBuffer.pData == NULL)
      return LB_CANNOT_ALLOC_MEMORY;
   
   //check if the buffer is full
   // TODO: Mirar como ver si se van a perder eventos del hardware
   if ((m_parBuffer.nEvents + Events2Read) >= m_parBuffer.nTotalEvents) 
   {
      if  ((m_parBuffer.nEvents ) >= m_parBuffer.nTotalEvents) 
         return LB_ERROR;
      else
         Events2Read = (m_parBuffer.nTotalEvents - m_parBuffer.nEvents);
   }

   // If end of buffer go to first memory address
   if (m_parBuffer.currentPosEvent + Events2Read > m_parBuffer.nTotalEvents)
   {
      if (Events2Read>1)
         Events2Read =  m_parBuffer.nTotalEvents - m_parBuffer.currentPosEvent;
      else
         m_parBuffer.writeAddress = m_parBuffer.pData;
   }


   
   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_DATA, (m_parBuffer.eventSize * Events2Read),m_pRawData);
   if(err)
      return LB_ERROR;


   uint8_t *pData;
   pData = m_pRawData;

   for(uint16_t i = 0; i < Events2Read ; i++)
   {

      pEvent = (PARTICLE_EVENT*) m_parBuffer.writeAddress;
      pEvent->pData = (uint16_t*)(&pEvent->pData) + sizeof((pEvent->pData));
      
      pEvent->eventCounter = (pData[0]<<21) | (pData[1]<<13) | (pData[2]<<5) | (pData[3]>>3);
      pEvent->eventSize = EventSizeConversion[(pData[3]&0x07)];
      pEvent->timeStamp = (pData[4]<<24) | (pData[5]<<16) | (pData[6]<<8) | (pData[7]);
      pEvent->nEvents = ((pData[10]<<8)|(pData[11]))&0x3FF;
      pEvent->readOrder = (pData[8]<<13) | (pData[9]<<5) | (pData[10]>>3);
      //TODO: check the format of the data
      memcpy(pEvent->pData, &pData[12], m_parBuffer.eventSize - PARTICLE_EVENT_FPGA_HEADER_SIZE);
     
      

      // reorder the adc data
      uint8_t *pU8Hi, *pU8Lo;
      uint8_t u8Temp;
      pU8Hi = (uint8_t*)pEvent->pData;
      pU8Lo = pU8Hi + 1;
      
      for(uint16_t j= 0; j < ((m_parBuffer.eventSize-PARTICLE_EVENT_FPGA_HEADER_SIZE)/2); j++)
      {
            u8Temp = *pU8Hi;
            *pU8Hi = *pU8Lo; 
            *pU8Lo = u8Temp; 
            pU8Hi+=2;
            pU8Lo+=2;         
      }
      
     
      if (m_parBuffer.currentPosEvent == m_parBuffer.nTotalEvents-1)
      {
         m_parBuffer.currentPosEvent = 0;
         // Update the write pointer
         m_parBuffer.writeAddress = (uint8_t*)m_parBuffer.pData;
      }
      else
      {
         m_parBuffer.currentPosEvent++;
         // Update the write pointer
         m_parBuffer.writeAddress += m_parBuffer.eventOffset;
      }
      
      // Atomic operation????
      m_parBuffer.nEvents++;

      // Next event in the buffer
      pData += m_parBuffer.eventSize;
      
   }

    //Number of events in the FPGA
    *FpgaEvents = pEvent->nEvents-1;


   /*
   err = m_pCBus->read32(BASE_ADDR_ADC_EVENT_DATA, nData, pData);

   PARTICLE_EVENT Event;

   Event.eventCounter = (pData[0]<<21) | (pData[1]<<13) | (pData[2]<<5) | (pData[3]>>3);
   Event.eventSize = (pData[3]&0x07);
   Event.timeStamp = (pData[4]<<24) | (pData[5]<<16) | (pData[6]<<8) | (pData[7]);
   Event.nEvents = ((pData[10]<<8)|(pData[11]))&0x3FF;
   Event.readOrder = (pData[8]<<13) | (pData[9]<<5) | (pData[10]>>3);
   Event.pData = (uint16_t*) &pData[12];
   
   
   // reorder the adc data
   uint8_t *pU8Hi, *pU8Lo;
   uint8_t u8Temp;
   pU8Hi = &pData[12];
   pU8Lo = &pData[13];
   
   //uint16_t pDataA[1024];
   //uint16_t pDataB[1024];
   //memcpy(pDataA,Event.pData,(nData-12));
   
   for(uint16_t i= 0; i < ((nData-12)/2); i++)
   {
         u8Temp = *pU8Hi;
         *pU8Hi = *pU8Lo; 
         *pU8Lo = u8Temp; 
         pU8Hi+=2;
         pU8Lo+=2;         
   }

   //memcpy(pDataB,Event.pData,(nData-12));

  */ 
   
   return err;
}


