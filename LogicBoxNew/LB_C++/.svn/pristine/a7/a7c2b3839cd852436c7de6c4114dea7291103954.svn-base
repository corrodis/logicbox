// $Id: cbus.h 144 2012-08-16 13:15:07Z  $

#ifndef CDL709_7ADC_1TTL_H
#define CDL709_7ADC_1TTL_H



#include "data_types.h"
#include "CCBUS.h"
#include "LogicBoxDefines.h"
#include <windows.h>
#include <stdio.h>


// move later to a specific to the project .h file
#define ADC_SPI 0x00000000
#define ADC_CLK 0x00000100
#define ADC_LED 0x00000200
#define ADC_MEM 0x00010000

#define BA_EV_BUFFER 0x00010000
#define EB_RD_ORDER (BA_EV_BUFFER+8+7)

const char StateMachine2Text[8][24]={  "Reset", "Idle", "Header 1", "Header 2", 
                                       "Header 3", "sending ram data",
                                       "Inc. the sample","Checking end of Event"};
const uint16_t EventSizeConversion[8] = {16,32,64,128,256,512,1024,2048};

//------------------ Register Map ------------------//

#define BASE_ADDR_ADC_EVENT_CONTROL 0x10008 
   
   #define ADDR_MISSED_EVENTS_REGISTER  0x01 // read only
   #define ADDR_MISSED_EVENTS_FULL_REGISTER  0x02 // read only

   #define ADDR_COMMAND_REGISTER  0x03 // write only
      #define TIMESTAMP_CLEAR     (0x01<<0)
      #define EVENT_CLEAR         (0x01<<1)
      #define SOFT_TRIGGER        (0x01<<2)
      #define START_TESTPATERN    (0x01<<3)
      #define RESET_STATE_MACHINE (0x01<<4)
            

   #define ADDR_STATUS_REGISTER  0x03 // read only
   #define ADDR_CONFIGURATION_REGISTER  0x04 // read/write
      #define INVERT_TRIGGER     (0x01<<0)
      #define INVERT_TIME_SYNC   (0x01<<1)
      #define INVERT_BUSY        (0x01<<2)
   #define ADDR_EVENT_SIZE_REGISTER  0x05 // read/write????
      #define SAMPLES_16      0         
      #define SAMPLES_32      1
      #define SAMPLES_64      2
      #define SAMPLES_128     3
      #define SAMPLES_256     4      
      #define SAMPLES_512     5
      #define SAMPLES_1024    6
      #define SAMPLES_2048    7
   #define ADDR_PRESAMPLES_REGISTER  0x06 // read/write????
   #define ADDR_READ_ORDER_REGISTER  0x07 // read/write????
      #define CH0    0
      #define CH1    1
      #define CH2    2
      #define CH3    3      
      #define CH4    4
      #define CH5    5
      #define CH6    6
      #define STOP   7
      #define _1ST(CH)   CH
      #define _2ND(CH)   (CH<<3)
      #define _3RD(CH)   (CH<<6)
      #define _4TH(CH)   (CH<<9)
      #define _5TH(CH)   (CH<<12)
      #define _6TH(CH)   (CH<<15)
      #define _7TH(CH)   (CH<<18)
      #define _8TH(CH)   (CH<<21)

#define BASE_ADDR_ADC_EVENT_DATA 0x10010 





#pragma pack(push)
#pragma pack(1)



typedef struct struct_event_buffer
{
   uint8_t nChannels; // Number of active channels 1..7
   uint8_t channelsMask; // If '1' channel is activated bit 6 = channel 6, bit 0 = channel 0
   uint16_t eventSize; // Event size in Bytes x nChannels with header
   uint16_t eventOffset; // Offset to get the next PARTICLE_EVENT
   uint16_t nEvents; // Number of events in this buffer
   uint16_t currentPosWriteEvent; // indicate the position in the buffer of the last Written event 
   uint16_t currentPosReadEvent; // indicate the position in the buffer of the last Written event 
   uint16_t nTotalEvents; // Number of events that could be save in this buffer
   uint32_t lostEvents; // number of lost events (Buffer full)
   uint8_t *writeAddress; // Address to wite the next event   
   uint8_t *readAddress;  // Address to read the next event	
   uint8_t *endAddress;  // Address to read the next event	

   uint8_t *pData; // Pointer to the buffer where all the particle events structures are saved
} PARTICLE_EVENT_BUFFER;

typedef struct struct_particle_event
{
	uint32_t eventCounter;  // FPGA particle event counter
   uint16_t eventSize;      // FPGA particle event size
   uint32_t timeStamp;     // FPGA particle event time stamp
   uint16_t nEvents;       // Number of particke events in the FPGA buffer, including the current one
   uint32_t readOrder;     // channel read order
   uint16_t *pData;        // Pointer to the ADC samples
} PARTICLE_EVENT;
#pragma pack(pop)


#define PARTICLE_EVENT_HEADER_SIZE        sizeof(PARTICLE_EVENT)
// 16 Samples/event x 2 bytes/Sample x 512 events = 3670016 bytes

#define PARTICLE_EVENT_FPGA_MAX_CHANNELS (7)
#define PARTICLE_EVENT_FPGA_HEADER_SIZE (12)
#define PARTICLE_EVENT_FPGA_DEFAULT_SIZE ((16*2)) // No header!!!
#define PARTICLE_EVENT_FPGA_MAX_NUMBER   512
#define PARTICLE_EVENT_FPGA_MAX_BUFFER_SIZE   ((PARTICLE_EVENT_FPGA_HEADER_SIZE +(PARTICLE_EVENT_FPGA_DEFAULT_SIZE*PARTICLE_EVENT_FPGA_MAX_CHANNELS) + PARTICLE_EVENT_HEADER_SIZE) * PARTICLE_EVENT_FPGA_MAX_NUMBER)

class CDL709_7ADC_1TTL
{
    public:
        CDL709_7ADC_1TTL(CCBUS *pCBus);
        ~CDL709_7ADC_1TTL();

        // mask with the ADCs present or turned on
        int16_t adc_present;
        uint16_t adc_rorder;
        // the DCM phase in FPGA
        int16_t dcm_phase;
        // ADC DLL status after init
        uint16_t adc_dll_on;
        uint16_t adc_tp;
        uint16_t div_phase;
        uint16_t div_ratio;

        int32_t usb_write_long(uint32_t Address, uint32_t Data);
        int32_t usb_read_long(uint32_t Address, uint32_t* Data);

        // write to the ADC SPI interface block, offset addres is the ADC #
        // ADC # > 7 is reset
        int32_t usb_wadc_spi(uint16_t adcnr, uint32_t confreg);
        // ADC DLL on/off, must be on for > 60 MHz and off for < 60 MHz
        int32_t usb_adc_dll(uint16_t adcnr, uint16_t on);
        // test pattern 0..3, 0 is normal, 1 is all 0, 2 is all 1, 3 is toggle
        int32_t usb_adc_tp(uint16_t adcnr, uint16_t tp);
        // power down mode on/off
        int32_t usb_adc_pwrdn(uint16_t adcnr, uint16_t on);
        // reset ALL ADCs! Generate a 2 us reset pulse
        int32_t usb_adcs_reset(void);

        // turn on and configure the adc_present, full control
        int32_t usb_adc_init(uint32_t new_adc_rorder,
                            uint16_t new_adc_dll_on,
                            uint16_t new_adc_tp,
                            int16_t new_dcm_phase,
                            uint16_t new_div_ratio,
                            uint16_t new_div_phase);

        // for testpattern use, automatic set the proper values
        int32_t usb_adc_init(uint32_t new_adc_rorder,
                            uint16_t new_adc_tp,
                            uint16_t new_div_ratio);

        //TODO: implemet
        // new_div_ratio 1..4 (100..25MHz)
        // for normal use, automatic set the proper values
        int32_t usb_adc_init(uint32_t new_adc_rorder,
                            uint16_t new_div_ratio);

         //TODO: implemet (output in buffer)
        // measure the CLKOUT from the ADCs in the FPGA synchron with the system clock
        // the transition from 0 to 127 is the best region to sample the ADC data
        int32_t usb_adc_DCM_scan(uint16_t dll_on);

        int32_t setCommandRegister(uint8_t value);
        int32_t getNumberEvents(uint16_t *nEvents);
        int32_t getNumberEventsForReading(uint8_t *nEvents);
        int32_t getNumberEventsForWriting(uint8_t *nEvents);
        int32_t getConfigurationRegister(uint8_t *value);
        int32_t setConfigurationRegister(uint8_t value);
        int32_t setEventSize(uint8_t value);
        int32_t getEventSize(uint8_t *value);
        int32_t setEventpresamples(uint16_t value);
        int32_t setEventReadOrder(uint32_t value);
        int32_t getEventReadOrder(uint32_t *value);
        int32_t getReadStateMachine(uint8_t *state);
        int32_t disableHWTrigger(bool disable);
        int32_t getMissedEvents(uint32_t *totalCounter, uint32_t *bufferFullCounter);

        // ------ read event functions ------ //
        // run this function in a thread
        int32_t getAllFpgaEvents();     
        //Run this function in a new thread
        int32_t readMemoryEvent(uint32_t *Id,uint32_t *timeStamp, uint16_t nData, uint16_t *pData);
    
   private:
      void updateReadBuffer(void);
      int32_t getFpgaEvents(uint16_t Events2Read, uint16_t *FpgaEvents);
      CCBUS *m_pCBus;
      PARTICLE_EVENT_BUFFER m_parBuffer;
      uint8_t *m_pRawData;

};

#endif //CDL709_7ADC_1TTL_H
