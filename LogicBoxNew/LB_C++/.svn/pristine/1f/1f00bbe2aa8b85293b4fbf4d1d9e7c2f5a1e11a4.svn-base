#include "data_types.h"
#include "LogicBoxDefines.h"

#ifdef __cplusplus
extern "C" {
#endif
 
   
__declspec(dllexport) int32_t getDevices(LOGIC_BOX_INF *pInfo);
__declspec(dllexport) int32_t openDeviceNumber(uint16_t index, uint8_t designId, int32_t* Handle);
__declspec(dllexport) int32_t openDeviceSerialNumber(char sn[16], uint8_t designId, int32_t* Handle);
__declspec(dllexport) int32_t closeDevice(int32_t Handle);
__declspec(dllexport) int32_t getCompilationDate(int32_t Handle, STRUCT_DATE *compilationDate);


// ------------ DAQ functions ------------//
__declspec(dllexport) int32_t daq_setCommandRegister(int32_t Handle, uint8_t value);
__declspec(dllexport) int32_t daq_getNumberEvents(int32_t Handle, uint16_t *nEvents);
__declspec(dllexport) int32_t daq_getNumberEventsForReading(int32_t Handle, uint8_t *nEvents);
__declspec(dllexport) int32_t daq_getNumberEventsForWriting(int32_t Handle, uint8_t *nEvents);
__declspec(dllexport) int32_t daq_getConfigurationRegister(int32_t Handle, uint8_t *value);
__declspec(dllexport) int32_t daq_setConfigurationRegister(int32_t Handle, uint8_t value);
__declspec(dllexport) int32_t daq_setEventSize(int32_t Handle, uint8_t value);
__declspec(dllexport) int32_t daq_getEventSize(int32_t Handle, uint8_t *value);
__declspec(dllexport) int32_t daq_setEventpresamples(int32_t Handle, uint16_t value);
__declspec(dllexport) int32_t daq_setEventReadOrder(int32_t Handle, uint32_t value);
__declspec(dllexport) int32_t daq_getEventReadOrder(int32_t Handle, uint32_t *value);
__declspec(dllexport) int32_t daq_getReadStateMachine(int32_t Handle, uint8_t *state);
__declspec(dllexport) int32_t daq_disableHWTrigger(int32_t Handle, bool disable);
__declspec(dllexport) int32_t daq_getMissedEvents(int32_t Handle, uint32_t *totalCounter, uint32_t *bufferFullCounter);
__declspec(dllexport) int32_t daq_getADCResolutionRegister(int32_t Handle, uint8_t *resReg, uint8_t *shiftReg);
__declspec(dllexport) int32_t daq_setADCResolutionRegister(int32_t Handle, uint8_t resReg, uint8_t shiftReg);
__declspec(dllexport) int32_t daq_setRandomizerRegister(int32_t Handle,uint8_t enableMask);
__declspec(dllexport) int32_t daq_getRandomizerRegister(int32_t Handle,uint8_t* enableMask);

// read event functions
__declspec(dllexport) int32_t daq_getAllFpgaEvents(int32_t Handle);  // run this function in a thread   
__declspec(dllexport) int32_t daq_readMemoryEvent(int32_t Handle, uint32_t *Id,
                                                  uint32_t *timeStamp, uint16_t nData, uint16_t *pData);//Run this function in a new thread 
__declspec(dllexport) int32_t daq_initADCs(int32_t Handle, uint32_t sampleOrder, uint16_t divRatio);

__declspec(dllexport) int32_t daq_setDMA(int32_t Handle, bool ena);
__declspec(dllexport) int32_t daq_getFpgaEventsDMA(int32_t Handle, uint16_t *FpgaEvents);

/* set ADC output test pattern
	new_adc_tp:
	  ADC_TP_OFF			// SU706 and SU735
	  ADC_TP_ALL_ZEROS	// Only supported by SU706
	  ADC_TP_ALL_ONES		// Only supported by SU706
	  ADC_TP_TOGGLE		// SU706 and SU735
	  ADC_TP_10_01    	// Only supported by SU735
	
	new_div_ratio:
		1 -> 100 MHz
		2 -> 50 MHz
		3 -> 25 MHz
		4 -> 33 MHz
	
	return:
	  ERROR_ADC_TEST_PATERN_NOT_SUPPORTED */
__declspec(dllexport) int32_t daq_findBestADCClockPhase(int32_t Handle, uint16_t new_adc_tp, uint16_t new_div_ratio);

// ------------ TDC functions ------------//
__declspec(dllexport) int32_t tdc_getAllFpgaEvents(int32_t Handle);
__declspec(dllexport) int32_t tdc_readMemoryEvent(int32_t Handle, uint32_t *Id,uint32_t *timeStamp, 
                                                  uint16_t nData, uint16_t *pData);
__declspec(dllexport) int32_t tdc_setIOMask(int32_t Handle, uint16_t disableMask, uint16_t invertMask);
__declspec(dllexport) int32_t tdc_getIOMask(int32_t Handle, uint16_t *disableMask, uint16_t *invertMask);
__declspec(dllexport) int32_t tdc_resetEventbuffer(int32_t Handle);

__declspec(dllexport) int32_t tdc_getFPGAEngineStatus(int32_t Handle, uint16_t *nEvents, 
                                                      uint8_t *readEngine, uint8_t *readMux,
                                                      uint8_t *bufferFull, uint8_t *bufferEmpty);

// bit 0 : stop, bit 1..7 : start0..6, bit 8 : timesync
__declspec(dllexport) int32_t tdc_setTerminationMask(int32_t Handle, uint16_t termination, uint16_t standard);
__declspec(dllexport) int32_t tdc_getTerminationMask(int32_t Handle, uint16_t *termination, uint16_t *standard);

__declspec(dllexport) int32_t tdc_getNumberEventsStored(int32_t Handle, uint32_t *nEventsStored);
__declspec(dllexport) int32_t tdc_getNumberEventsRead(int32_t Handle, uint32_t *nEventsRead);
__declspec(dllexport) int32_t tdc_getNumberEventsFPGABuffer(int32_t Handle, uint16_t *nEventsFPGABuffer);

__declspec(dllexport) int32_t tdc_softwareTrigger(int32_t Handle, uint16_t triggerMask);
__declspec(dllexport) int32_t tdc_setDMA(int32_t Handle, bool ena);
__declspec(dllexport) int32_t tdc_getFpgaEventsDMA(int32_t Handle, uint16_t *FpgaEvents);



// DLL ERROR CODES
#define ERROR_OFFSET    1000
#define ERROR_NO_ENOUGH_MEMORY            (ERROR_OFFSET+1)
#define ERROR_LOGIC_BOX_MEMORY_FULL       (ERROR_OFFSET+2)
#define ERROR_INVALID_LOGIC_BOX_HANDLE    (ERROR_OFFSET+3)
#define ERROR_NO_DEVICE_FOUND             (ERROR_OFFSET+4)
#define ERROR_NO_DEVICE_FOUND_WITH_SN     (ERROR_OFFSET+5)
#define ERROR_UNKNOW_FPGA_DESIGN          (ERROR_OFFSET+6)

// Design identification
#define MAX_DESIGNS 2
#define DESIGN_DAQ   1
#define DESIGN_TDC   2


#ifdef __cplusplus
}
#endif