// $Id: cbus.h 144 2012-08-16 13:15:07Z  $

#ifndef CDL709_7ADC_1TTL_H
#define CDL709_7ADC_1TTL_H



#include "data_types.h"
#include "CCBUS.h"
#include <windows.h>
#include <stdio.h>


// move later to a specific to the project .h file
#define ADC_SPI 0x00000000
#define ADC_CLK 0x00000100
#define ADC_LED 0x00000200
#define ADC_MEM 0x00010000

class CDL709_7ADC_1TTL
{
    public:
        CDL709_7ADC_1TTL(CCBUS *pCBus);
        ~CDL709_7ADC_1TTL();

        // mask with the ADCs present or turned on
        int16_t adc_present;
        // the DCM phase in FPGA
        int16_t dcm_phase;
        // ADC DLL status after init
        uint16_t adc_dll_on;
        uint16_t adc_tp;
        uint16_t div_phase;
        uint16_t div_ratio;

        int32_t usb_write_long(uint32_t Address, uint32_t Data);
        int32_t usb_read_long(uint32_t Address, uint32_t* Data);

        // write to the ADC SPI interface block, offset addres is the ADC #
        // ADC # > 7 is reset
        int32_t usb_wadc_spi(uint16_t adcnr, uint32_t confreg);
        // ADC DLL on/off, must be on for > 60 MHz and off for < 60 MHz
        int32_t usb_adc_dll(uint16_t adcnr, uint16_t on);
        // test pattern 0..3, 0 is normal, 1 is all 0, 2 is all 1, 3 is toggle
        int32_t usb_adc_tp(uint16_t adcnr, uint16_t tp);
        // power down mode on/off
        int32_t usb_adc_pwrdn(uint16_t adcnr, uint16_t on);
        // reset ALL ADCs! Generate a 2 us reset pulse
        int32_t usb_adcs_reset(void);

        // turn on and configure the adc_present, full control
        int32_t usb_adc_init(uint16_t new_adc_present,
                            uint16_t new_adc_dll_on,
                            uint16_t new_adc_tp,
                            int16_t new_dcm_phase,
                            uint16_t new_div_ratio,
                            uint16_t new_div_phase);

        // for testpattern use, automatic set the proper values
        int32_t usb_adc_init(uint16_t new_adc_present,
                            uint16_t new_adc_tp,
                            uint16_t new_div_ratio);

        // for normal use, automatic set the proper values
        int32_t usb_adc_init(uint16_t new_adc_present,
                            uint16_t new_div_ratio);
        // measure the CLKOUT from the ADCs in the FPGA synchron with the system clock
        // the transition from 0 to 127 is the best region to sample the ADC data
        int32_t usb_adc_DCM_scan(uint16_t dll_on);
    
   private:
      CCBUS *m_pCBus;

};

#endif //CDL709_7ADC_1TTL_H