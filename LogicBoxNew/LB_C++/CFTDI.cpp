#include "CFTDI.h"


uint32_t CFTDI::m_isLibraryLoaded = 0;
int32_t CFTDI::m_lastError = 0;
HINSTANCE CFTDI::m_hDLL = NULL;

// Function pointers
LPFT_ListDevices            CFTDI::DLL_FT_ListDevices      ;
LPFT_W32_CreateFile         CFTDI::DLL_FT_W32_CreateFile   ;
LPFT_W32_GetCommState       CFTDI::DLL_FT_W32_GetCommState ;
LPFT_W32_SetCommState       CFTDI::DLL_FT_W32_SetCommState ;
LPFT_W32_SetupComm          CFTDI::DLL_FT_W32_SetupComm    ;
LPFT_W32_WriteFile          CFTDI::DLL_FT_W32_WriteFile    ;
LPFT_W32_ReadFile           CFTDI::DLL_FT_W32_ReadFile     ;
LPFT_CreateDeviceInfoList   CFTDI::DLL_FT_CreateDeviceInfoList;
LPFT_W32_CloseHandle        CFTDI::DLL_FT_W32_CloseHandle;                            


CFTDI::CFTDI()
{

	int32_t ret;
	m_hDevice = NULL;
	
	// Load the lirbary if necesary
    if (m_isLibraryLoaded == 0)
    {
		ret = LoadLib();
        if (!ret)
                m_isLibraryLoaded++;
        else
        {
                m_lastError = ret;
                m_isLibraryLoaded=-1;
            }
    }
    else
            m_isLibraryLoaded++;

}

CFTDI::~CFTDI()
{
	closeDevice();
    if (m_isLibraryLoaded>0)
    {
        m_isLibraryLoaded--;
        if (m_isLibraryLoaded == 0)
            if (m_hDLL != NULL)
				   FreeLibrary(m_hDLL);    
    }
	
}


int32_t CFTDI::getNumberDevices(uint32_t *nDev)
{

	 
    FT_STATUS ftStatus;    
    DWORD numDevs;
    ftStatus = DLL_FT_CreateDeviceInfoList(&numDevs);
    *nDev = numDevs;

    return ftStatus;
	
}
int32_t CFTDI::getSerialNumber(uint32_t nDevice, int8_t *pSerial)
{
   FT_STATUS	ftStatus;
   
   // get the name of the first device   
   ftStatus = DLL_FT_ListDevices((PVOID)nDevice,pSerial,FT_LIST_BY_INDEX|FT_OPEN_BY_SERIAL_NUMBER); 

   return LB_SUCCESS;

}

int32_t CFTDI::openDevice(int32_t index)
{
   int32_t ret = 0;
   FT_STATUS	ftStatus;
   char Buf[4096];
   
   // get the name of the first device   
   ftStatus = DLL_FT_ListDevices(0,Buf,FT_LIST_BY_INDEX|FT_OPEN_BY_SERIAL_NUMBER);  


   // Open the first LogicBox	
   m_hDevice = DLL_FT_W32_CreateFile(Buf,GENERIC_READ|GENERIC_WRITE,0,0,
            OPEN_EXISTING,
            FILE_ATTRIBUTE_NORMAL | FT_OPEN_BY_SERIAL_NUMBER,
            0);
   
   if (m_hDevice == NULL)
      ret = LB_CANNOT_OPEN_DEVICE;


    if (!ret)
    {
        FTDCB ftDCB;
        if (DLL_FT_W32_GetCommState(m_hDevice,&ftDCB))
        {
          // FT_W32_GetCommState ok, device state is in ftDCB
          ftDCB.BaudRate = 921600;
          if (!DLL_FT_W32_SetCommState(m_hDevice,&ftDCB))
            return -1; // FT_W32_SetCommState failed
        }
        else
          return -1; // FT_W32_GetCommState failed

        ret = DLL_FT_W32_SetupComm(m_hDevice, 4096, 4096);
        if (!ret)
            return -1;
        ret = 0;
    }



	return ret;

   //   ret = LB_CANNOT_OPEN_DEVICE;

}

bool CFTDI::isOpen()
{
   if (m_hDevice == NULL)
      return false;
   else
      return true;
}


int32_t CFTDI::closeDevice()
{
	int32_t ret;    

   if (m_hDevice != NULL)
   {
      ret = DLL_FT_W32_CloseHandle(m_hDevice);  
      if (!ret)
         m_hDevice = NULL;
   }
   else
      return LB_NO_DEVICE_OPEN;

	return ret;

}

int32_t CFTDI::nBytesAvailable(uint32_t* nBytes)
{
/*
    if (m_hDevice != NULL)
		return DLL_USBBoxNumberOfBytesAvailable(m_hDevice, nBytes);
	else
		return LB_NO_DEVICE_OPEN;
*/

   return -1;
}

int32_t CFTDI::write_bytes(uint32_t nBytes, void * buffer )
{

   OVERLAPPED osWrite = { 0 };
   DWORD dwWritten;
   int32_t ret;
   if (m_hDevice != NULL)
   {
		
      ret = DLL_FT_W32_WriteFile(m_hDevice, buffer, nBytes, &dwWritten, &osWrite);
      if (nBytes == dwWritten)
        return LB_SUCCESS;
      else
         return LB_ERROR;
   }
   else
		return LB_NO_DEVICE_OPEN;
	
}

int32_t CFTDI::read_bytes(uint32_t nBytes, void * buffer )
{
   
   int32_t ret;
   DWORD dwRead;
   OVERLAPPED osRead = { 0 };

   if (m_hDevice != NULL)
   {
      ret = DLL_FT_W32_ReadFile(m_hDevice, buffer, nBytes, &dwRead, &osRead);
      if (nBytes == dwRead)
         return LB_SUCCESS;
      else
         return LB_ERROR;
   }
   else
      return LB_NO_DEVICE_OPEN;

}


int32_t CFTDI::LoadLib()
{
	
   int32_t  ReturnVal = 0;

   m_hDLL = LoadLibrary(_T("FTD2XX.dll"));

   if (m_hDLL == NULL)
      return LB_DLL_ERROR_DLL_NOT_FOUND;

   DLL_FT_ListDevices			   = (LPFT_ListDevices)GetProcAddress(m_hDLL, "FT_ListDevices");
   DLL_FT_W32_CreateFile         = (LPFT_W32_CreateFile)GetProcAddress(m_hDLL, "FT_W32_CreateFile");
   DLL_FT_W32_GetCommState       =   (LPFT_W32_GetCommState ) GetProcAddress(m_hDLL, "FT_W32_GetCommState");
   DLL_FT_W32_SetCommState       =   (LPFT_W32_SetCommState ) GetProcAddress(m_hDLL, "FT_W32_SetCommState");
   DLL_FT_W32_SetupComm          =   (LPFT_W32_SetupComm    ) GetProcAddress(m_hDLL, "FT_W32_SetupComm");
   DLL_FT_W32_WriteFile          =   (LPFT_W32_WriteFile    ) GetProcAddress(m_hDLL, "FT_W32_WriteFile");
   DLL_FT_W32_ReadFile           =   (LPFT_W32_ReadFile     ) GetProcAddress(m_hDLL, "FT_W32_ReadFile");
   DLL_FT_CreateDeviceInfoList   =   (LPFT_CreateDeviceInfoList) GetProcAddress(m_hDLL, "FT_CreateDeviceInfoList");
   DLL_FT_W32_CloseHandle        = (LPFT_W32_CloseHandle) GetProcAddress(m_hDLL, "FT_W32_CloseHandle");

   if (
      (!DLL_FT_ListDevices			      ) ||
      (!DLL_FT_W32_CreateFile	         ) ||
      (!DLL_FT_W32_GetCommState        ) ||
      (!DLL_FT_W32_SetCommState        ) ||
      (!DLL_FT_W32_SetupComm           ) ||
      (!DLL_FT_W32_WriteFile           ) ||
      (!DLL_FT_CreateDeviceInfoList    ) ||
      (!DLL_FT_W32_CloseHandle         ) ||
      (!DLL_FT_W32_ReadFile            )
      )
   {
      // handle the error
      FreeLibrary(m_hDLL);
      return LB_DLL_ERROR_LOADING_FUNCTION;

   }
    return LB_SUCCESS;
	
	
}
