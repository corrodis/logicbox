#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include<ctime>

#ifndef _MSC_VER
    typedef char _TCHAR;
#else
	#if (_MSC_VER >= 1700) 
		#include <tchar.h>
	#else
        typedef char _TCHAR;
	#endif
#endif

#include <TTree.h>
#include <TFile.h>
#include <TParameter.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <THStack.h>

typedef signed char        int8_t;
typedef short              int16_t;
typedef int                int32_t;
//typedef long long          int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
//typedef unsigned long long uint64_t;


typedef struct fileHeaderStruct {
	uint32_t RunNumber;
	uint32_t SamplingFrequency;
	uint32_t Presamples;
	uint32_t reserved[61];
} FileHeader;

typedef struct eventHeaderStruct {
	uint32_t Number;
	uint32_t Type;
	uint32_t Time;
	uint32_t reserved;
	uint32_t FPGAEvent;
	uint32_t FPGATime;
	uint32_t Channels;
	uint32_t Samples;
} EventHeader;

int makehist(const int runnum = -1, int nbins = 50, int take = -1, int minx = -1, int maxx = -1, const char includeChannels[] = "0123456")
{
    if (runnum < 0) {
        printf("Makehist plots histograms for all events in run #runnum.\n\n" \
               "Syntax:\n\nmakehist(int runnum[, int nbins = 50[, int samplesForPedestal = -1[, int x_axis_min = -1[, int x_axis_max = -1[, str channels]]]]])\n\nSetting an argument to -1 means you don't care, calculate it automatically.\n\n'Channels' allows control over which " \
               "channels to display in the histogram.\n\nFor example, passing \"014\" (in quotes) displays channels 0, 1, and 4 only. The default is to display all channels.\n\n" \
               "For run 255: makehist(255)\n\n");
        return 1;
    }
    if (nbins < 0) nbins = 50;

    FILE* datainput;
    //char datafiledir[256]; // No trailing slash
    const char datafiledir[15] = "E:\\PSI2013";

    // Convert runnum to filename
    char filename[256];
    char ROOTFileName[256];
    char histogramFileName[256];
    char histogramROOTFileName[256];
    uint32_t timeval = time(nullptr);
    sprintf(filename, "%s/psi13_%06d.bin", datafiledir, runnum);
    sprintf(ROOTFileName,"%s/psi13_%06d.root", datafiledir, runnum);

    sprintf(histogramFileName,"%s/makehist_%06i_%i.png", datafiledir, runnum, timeval);
    sprintf(histogramROOTFileName,"%s/makehist_%06i_%i.root", datafiledir, runnum, timeval);

    // Check if ROOT file already exists, if it does skip the data storage.
    if( access( ROOTFileName, F_OK ) == -1 ) {
        // file doesn't exist
	
        datainput = fopen(filename,"rb");
        if (datainput==NULL) {fputs ("Couldn't open data file.",stderr); exit (1);}

        FileHeader fh;
        fread((void*)&fh,sizeof(FileHeader),1,datainput);
        printf("size = %d, RunNumber = %i, SamplingFrequency = %i, Presamples = %i", sizeof(FileHeader), fh.RunNumber , fh.SamplingFrequency, fh.Presamples);

        EventHeader eh;
        uint16_t * eb;
        unsigned int elements;

        TFile *ROOTFile = new TFile(ROOTFileName, "RECREATE");
        TTree *T = new TTree("Tree","Run");

        TParameter<int> RunNumber("RunNumber", fh.RunNumber);
        T->GetUserInfo()->Add(&RunNumber);
        TParameter<int> Presamples("Presamples", fh.Presamples);
        T->GetUserInfo()->Add(&Presamples);
        TParameter<int> SamplingFrequency("SamplingFrequency", fh.SamplingFrequency);
        T->GetUserInfo()->Add(&SamplingFrequency);

        //Get information from first event and reset position in file
        uint32_t Samples, Channels;
        fread((void*)&eh,sizeof(EventHeader),1,datainput);
        Samples = eh.Samples;
        Channels = eh.Channels;
        printf("\nChannels %d, Samples %d",Channels,Samples);
        rewind(datainput);
        fread((void*)&fh,sizeof(FileHeader),1,datainput);

        T->Branch("EventNumber",&eh.Number,"EventNumber/i");
        T->Branch("EventTime",&eh.Time,"Time/i");
        T->Branch("FPGANumber",&eh.FPGAEvent,"FPGANumber/i");
        T->Branch("FPGATime",&eh.FPGATime,"FPGATime/i");
        T->Branch("Type",&eh.Type,"Type/i");
        T->Branch("Channels",&eh.Channels,"Channels/i");
        T->Branch("Samples",&eh.Samples,"Samples/i");

        char channelname[256],channelname_t[256];
        eb = new uint16_t[Samples * Channels];
        for (unsigned int i=0;i<Channels;i++) {
            sprintf(channelname, "Channel%d",i);
            sprintf(channelname_t, "Channel%d[Samples]/s",i);
            T->Branch(channelname, eb + i * Samples, channelname_t);
        }

        while(!feof(datainput)) {
            elements = fread((void*)&eh,sizeof(EventHeader),1,datainput);
            if (elements > 0) {
                fread((void*)eb, Samples * Channels * sizeof(uint16_t), 1, datainput);
                T->Fill(); // Segmentation violation here!
            }
        }

        fclose(datainput);
        ROOTFile->Write();
        ROOTFile->Close();

        printf("\n\nRaw data stored in ROOT file '%s'\n", ROOTFileName);
    }else{
        printf("\nROOT file already exists...Skipping.\n\n");
    }

    // Now make histograms
    // This is for choosing the channels to display.
    int l = 0;
    int displayChannelsArr[7] = {-1};
    for (int k=0;k<7;k++) {
        for (int i=0; i<strlen(includeChannels); i++) {
            if(includeChannels[i] - '0' == k) {
                //printf("\nDisplaying %i.\n", k);
                displayChannelsArr[l++] = k;
            }
        }
    }

    TFile* input;
    input = new TFile(ROOTFileName, "READ");
    TTree* t = (TTree*)input->Get("Tree");
    if (t==0) return -6;

    printf("%i entries\n",t->GetEntries());

    EventHeader eh;
    t->SetBranchAddress("EventNumber", &eh.Number);
    t->SetBranchAddress("Type", &eh.Type);
    t->SetBranchAddress("EventTime", &eh.Time);
    t->SetBranchAddress("FPGANumber", &eh.FPGAEvent);
    t->SetBranchAddress("FPGATime", &eh.FPGATime);
    t->SetBranchAddress("Channels", &eh.Channels);
    t->SetBranchAddress("Samples", &eh.Samples);

    uint16_t channel[7][2048];
    t->SetBranchAddress("Channel0", channel[0]);
    t->SetBranchAddress("Channel1", channel[1]);
    t->SetBranchAddress("Channel2", channel[2]);
    t->SetBranchAddress("Channel3", channel[3]);
    t->SetBranchAddress("Channel4", channel[4]);
    t->SetBranchAddress("Channel5", channel[5]);
    t->SetBranchAddress("Channel6", channel[6]);

    TParameter<int> *rn = (TParameter<int>*) t->GetUserInfo()->FindObject("RunNumber");
    printf("Run number: %d\n", rn->GetVal());

    TParameter<int> *presamples = (TParameter<int>*) t->GetUserInfo()->FindObject("Presamples");
    printf("Presamples: %d\n", presamples->GetVal());

    // Take is the number of samples to take for pedestal. This needs tweaking, it seems to have a sizeable effect!
    // Calculate it
    if (take==-1) {
        // i.e. generate dynamically
        take = presamples->GetVal() - 2; // Skip the first two samples
    }

    t->GetEntry(0);
    signed int integrated_val; // Save into a ROOT file. Cannot be of type other than signed int.
    uint32_t pedestal;

    // Can also calculate min and max automatically (but would rather not).
    if (minx == -1 || maxx == -1) {
        bool findminx = true;
        bool findmaxx = true;
        // For the first loop, find the min and max values
        for (int i = 0; i < t->GetEntries(); i++) {
            for (int k=0;k<7;k++) {
                t->GetEntry(i);
                integrated_val = 0;
                // Get Pedastal
                pedestal = 0;
                for (int j=2;j<presamples->GetVal() && j < take + 2;j++) { // Start taking a pedestal after first 2 samples.
                    pedestal += channel[k][j];
                }
                pedestal /= take;
                // Integrate
                for (int j=presamples->GetVal();j<eh.Samples;j++) {
                    integrated_val += channel[k][j]-pedestal;
                }
                if (findminx == true && integrated_val < minx) minx = integrated_val;
                if (findmaxx == true && integrated_val > maxx) maxx = integrated_val;
            }
        }
    }
    TCanvas *c1 = new TCanvas("c1","Energy Histogram",eh.Samples,10,900,700);
    TH1F *th[7];
    // TODO: improve pedestal check with sigma check etc.
    for (int k=0;k<7;k++) {
        char channeln[16];
        sprintf(channeln, "Channel %i", k);
        th[k] = new TH1F("th", channeln, nbins, minx, maxx);
        th[k]->SetLineColor(k+1);
        th[k]->SetLineWidth(2);
    }
    for (int i = 0; i < t->GetEntries(); i++) {
        for (int k=0;k<7;k++) {
            t->GetEntry(i);
            integrated_val = 0;
            // Get Pedastal
            uint32_t pedestal;
            pedestal = 0;
            for (int j=2;j<presamples->GetVal() && j < take + 2;j++) { // Start taking a pedestal after first 2 samples.
                pedestal += channel[k][j];
            }
            pedestal /= take;
            // Integrate
            for (int j=presamples->GetVal();j<eh.Samples;j++) {
                integrated_val += channel[k][j]-pedestal;
            }
            th[k]->Fill(integrated_val); // Perhaps integrator has to be an int for this to work?
        }

    }
    char hsn[128];
    sprintf(hsn, "Energy Histograms for Run %i", rn->GetVal());
    THStack *s = new THStack("histstack", hsn);
    for (int k=0;k<l;k++) { // Accept which to plot as function args
        s->Add(th[displayChannelsArr[k]]);
    }
    s->Draw("nostack");
    s->GetYaxis()->SetTitle("Count"); //please add bin size (counts / ???? nVs)
    s->GetXaxis()->SetTitle("Area (nVs)"); // Is it really nVs?
    c1->BuildLegend(0.85,0.85,1,1);
    c1->Update();

    c1->SaveAs(histogramFileName);
    c1->SaveAs(histogramROOTFileName);
    printf("\n\nHistgoram stored in %s.\n", histogramFileName);

    input->Close();

    return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
	return 0;
}
