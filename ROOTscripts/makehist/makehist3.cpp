
Bitbucket

    Repositories
    Create

    owner/repository
    Help
    nberger

psi13/DAQ
DAQ

    User icon psi13
    Share

Clone Fork
Compare Pull request
Watch

    Overview
    Source
    Commits
    Pull requests
    Downloads

    Administration

Source Diff History
DAQ / ROOTScripts / makehist / makehist.cpp
Full commit
Blame Embed Raw

  1
  2
  3
  4
  5
  6
  7
  8
  9
 10
 11
 12
 13
 14
 15
 16
 17
 18
 19
 20
 21
 22
 23
 24
 25
 26
 27
 28
 29
 30
 31
 32
 33
 34
 35
 36
 37
 38
 39
 40
 41
 42
 43
 44
 45
 46
 47
 48
 49
 50
 51
 52
 53
 54
 55
 56
 57
 58
 59
 60
 61
 62
 63
 64
 65
 66
 67
 68
 69
 70
 71
 72
 73
 74
 75
 76
 77
 78
 79
 80
 81
 82
 83
 84
 85
 86
 87
 88
 89
 90
 91
 92
 93
 94
 95
 96
 97
 98
 99
100
101
102
103
104
105
106
107
108
109
110
111
112
113
114
115
116
117
118
119
120
121
122
123
124
125
126
127
128
129
130
131
132
133
134
135
136
137
138
139
140
141
142
143
144
145
146
147
148
149
150
151
152
153
154
155
156
157
158
159
160
161
162
163
164
165
166
167
168
169
170
171
172
173
174
175
176
177
178
179
180
181
182
183
184
185
186
187
188
189
190
191
192
193
194
195
196
197
198
199
200
201
202
203
204
205
206
207
208
209
210
211
212
213
214
215
216
217
218
219
220
221
222
223
224
225
226
227
228
229
230
231
232
233
234
235
236
237
238
239
240
241
242
243
244
245
246
247
248
249
250
251
252
253
254
255
256
257
258
259
260
261
262
263
264
265
266
267
268
269
270
271
272
273
274
275
276
277
278
279
280
281
282
283
284
285
286
287
288
289
290
291
292
293
294
295
296
297
298
299
300
301
302
303
304
305
306
307
308
309
310
311
312
313
314
315
316
317
318
319
320
321
322
323
324
325
326
327
328
329
330
331
332
333
334
335
336
337
338
339
340
341
342
343
344
345
346
347
348
349
350
351
352
353
354
355
356
357
358
359
360
361
362
363
364
365
366
367
368
369
370
371
372
373
374
375
376
377
378
379
380
381
382
383
384
385
386
387
388
389
390
391
392
393
394
395
396
397
398
399
400
401
402
403
404
405
406
407
408
409
410
411
412
413
414
415
416
417
418
419
420
421
422
423
424
425
426
427
428
429

	

#include <cstdio>
#include <cstdlib>
#include <stdio.h>
#include <ctime>

#ifndef _MSC_VER
    typedef char _TCHAR;
#else
        #if (_MSC_VER >= 1700) 
                #include <tchar.h>
        #else
        typedef char _TCHAR;
        #endif
#endif

#include <TTree.h>
#include <TFile.h>
#include <TParameter.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <THStack.h>

#include "..\common.h"

int makehist(const int runnum = -1, int nbins = 50, const char includeChannels[] = "0123456", int take = -1, int minx = -1, int maxx = -1, int onlyGoodEvents = 0)
{
    if (runnum < 0) {
        printf("Makehist plots histograms for all events in run #runnum.\n\n" \
               "Syntax:\n\nmakehist(int runnum[, int nbins = 50[, str channels[, int samplesForPedestal = -1]]])\n\nTo calculate nbins or samplesForPedastal automatically, enter -1.\n\n'Channels' allows control over which " \
               "channels to display in the histogram.\n\nFor example, passing \"014\" (in quotes) displays channels 0, 1, and 4 only. The default is to display all channels.\n\n" \
               "For run 255 with 250 bins displaying only channels 0,1,4: makehist(255, 250, \"014\").\n\n");
        return 1;
    }
    if (nbins < 0) nbins = 50;

    FILE* datainput;
    char datafiledir[256];
        memset(datafiledir, 0, 256);
    const char* configfilename = "makehist.cfg";

        // Take data files directory from a local config file
    FILE* configfile;
    configfile = fopen(configfilename,"r");
    if (configfile == NULL) {
        printf("Enter data file directory >");
        gets(datafiledir);
        configfile = fopen(configfilename, "w+");
        fwrite(datafiledir,255,1,configfile);
        fclose(configfile);
    } else {
        fread(datafiledir,255,1,configfile);
        fclose(configfile);
    }

    // Convert runnum to filename
    char filename[256];
    char ROOTFileName[256];
    char LogFileName[256];
    char histogramROOTFileName[256];

    uint32_t timeval = time(NULL);
    sprintf(filename, "%s/psi13_%06d.bin", datafiledir, runnum);
    sprintf(ROOTFileName,"%s/psi13_%06d.root", datafiledir, runnum);
    sprintf(LogFileName,"%s/psi13_%06d.log", datafiledir, runnum);
    sprintf(histogramROOTFileName,"%s/makehist_%06i_%i.root", datafiledir, runnum, timeval);

        FILE* LogFile = fopen(LogFileName,"w");

    // Check if ROOT file already exists, if it does skip the data storage.
        FILE* CROOTFile = fopen(ROOTFileName,"r");
    if(CROOTFile == NULL ) {
        // file doesn't exist
        
                //check if binary raw data file exists. To change the directory edit/delete the makehist.cfg
        datainput = fopen(filename,"rb");
        if (datainput==NULL) {fputs ("Couldn't open data file. Are you sure it actually exists?\n", stderr); return -8;}

                fprintf(LogFile,"Binary file: %s\n", filename);
                fprintf(LogFile,"Raw ROOT output: %s\n", ROOTFileName);
                fprintf(LogFile,"Histogram ROOT output: %s\n\n", histogramROOTFileName);

        FileHeader fh;
        fread((void*)&fh, sizeof(FileHeader), 1, datainput);
        printf("RunNumber = %i, SamplingFrequency = %i, Presamples = %i\nReading data...", fh.RunNumber , fh.SamplingFrequency, fh.Presamples);
                fprintf(LogFile, "RunNumber: %i\nSamplingFrequency: %i\nPresamples: %i\n", fh.RunNumber , fh.SamplingFrequency, fh.Presamples);
     
                EventHeader eh;
        uint16_t * eb;
        unsigned int elements;

        TFile *ROOTFile = new TFile(ROOTFileName, "RECREATE");
        TTree *T = new TTree("Tree","Run");

        TParameter<int> RunNumber("RunNumber", fh.RunNumber);
        T->GetUserInfo()->Add(&RunNumber);
        TParameter<int> Presamples("Presamples", fh.Presamples);
        T->GetUserInfo()->Add(&Presamples);
        TParameter<int> SamplingFrequency("SamplingFrequency", fh.SamplingFrequency);
        T->GetUserInfo()->Add(&SamplingFrequency);

        //Get information from first event and 
        uint32_t Samples, Channels;
                // Read in the event header
        fread((void*)&eh,sizeof(EventHeader),1,datainput);
        Samples = eh.Samples;
        Channels = eh.Channels;
        printf("\nChannels %d, Samples %d",Channels,Samples);
                fprintf(LogFile, "Channels: %i\nSamples: %i\n", Channels, Samples);
     
                //reset position in file
        fseek(datainput, sizeof(FileHeader), SEEK_SET);

                //Add ROOT branches by telling ROOT the pointers to the values to be written
        T->Branch("EventNumber",&eh.Number,"EventNumber/i");
        T->Branch("EventTime",&eh.Time,"Time/i");
        T->Branch("FPGANumber",&eh.FPGAEvent,"FPGANumber/i");
        T->Branch("FPGATime",&eh.FPGATime,"FPGATime/i");
        T->Branch("Type",&eh.Type,"Type/i");
        T->Branch("Channels",&eh.Channels,"Channels/i");
        T->Branch("Samples",&eh.Samples,"Samples/i");

                //Add branches for the up to 7 channels
        char channelname[256],channelname_t[256];
        eb = new uint16_t[Samples * Channels];
        for (unsigned int i=0;i<Channels;i++) {
            sprintf(channelname, "Channel%d",i); //name
            sprintf(channelname_t, "Channel%d[Samples]/s",i); //type
            T->Branch(channelname, eb + i * Samples, channelname_t);
        }

                uint32_t EventsRead = 0;

        while(!feof(datainput)) {
            elements = fread((void*)&eh, sizeof(EventHeader), 1, datainput);
            if (elements > 0) {
                                // Read the binary data directly into memory, structured with the event structure
                fread((void*)eb, sizeof(uint16_t), Samples * Channels, datainput);
                                // Populate the tree.
                T->Fill(); // Segmentation violation here!
                                EventsRead++;
            }
        }
                fprintf(LogFile, "Events read: ", EventsRead);
                fflush(LogFile);

        fclose(datainput);
        ROOTFile->Write();
        ROOTFile->Close();

                fprintf(LogFile, "ROOT file written.\n");
     
        printf("\n\nRaw data stored in ROOT file '%s'\n", ROOTFileName);
    } else {
        printf("\nROOT file already exists...Skipping.\n\n");
    }
        if (CROOTFile != NULL) fclose(CROOTFile);

    // Now make histograms.
    // This is for choosing the channels to display.
    int l = 0;
    int displayChannelsArr[7] = {-1};
    for (int k=0;k<7;k++) {
        for (int m=0; m<strlen(includeChannels); m++) {
            if(includeChannels[m] - '0' == k) {
                //printf("\nDisplaying %i.\n", k);
                displayChannelsArr[l++] = k;
            }
        }
    }
        fprintf(LogFile, "Channels to include in histogram: %s\n", includeChannels);

        // Load the tree of raw data we just created and saved.
    TFile* input;
    input = new TFile(ROOTFileName, "READ");
    TTree* t = (TTree*)input->Get("Tree");
    if (t == 0) return -6;
        fprintf(LogFile, "ROOT file openened\n");

    printf("%i entries\n", t->GetEntries());
        fprintf(LogFile, "Entries in tree: %i\n", t->GetEntries());

    EventHeader eh;
    t->SetBranchAddress("EventNumber", &eh.Number);
    t->SetBranchAddress("Type", &eh.Type);
    t->SetBranchAddress("EventTime", &eh.Time);
    t->SetBranchAddress("FPGANumber", &eh.FPGAEvent);
    t->SetBranchAddress("FPGATime", &eh.FPGATime);
    t->SetBranchAddress("Channels", &eh.Channels);
    t->SetBranchAddress("Samples", &eh.Samples);

    uint16_t channel[7][2048];
    t->SetBranchAddress("Channel0", channel[0]);
    t->SetBranchAddress("Channel1", channel[1]);
    t->SetBranchAddress("Channel2", channel[2]);
    t->SetBranchAddress("Channel3", channel[3]);
    t->SetBranchAddress("Channel4", channel[4]);
    t->SetBranchAddress("Channel5", channel[5]);
    t->SetBranchAddress("Channel6", channel[6]);

    TParameter<int> *rn = (TParameter<int>*) t->GetUserInfo()->FindObject("RunNumber");
    printf("Run number: %d\n", rn->GetVal());

    TParameter<int> *presamples = (TParameter<int>*) t->GetUserInfo()->FindObject("Presamples");
    printf("Presamples: %d\n", presamples->GetVal());

    // Take is the number of samples to take for pedestal. This needs tweaking, it seems to have a sizeable effect!
    // Calculate it
    if (take < 1) {
        take = 24; // presamples->GetVal() - 2 Skip the first two samples
    }

        if (take >= presamples->GetVal()) {
                 printf("\nWARNING: Number of samples for pedestal subtraction  %d >= %d number of presamples!!!!\n\n", take, presamples->GetVal());
                 fprintf(LogFile, "\nWARNING: Number of samples for pedestal subtraction  %d >= %d number of presamples!!!!\n\n", take, presamples->GetVal());
        }


    t->GetEntry(0);
    signed int integrated_val; // Save into a ROOT file. Cannot be of type other than signed int.
    uint32_t pedestal;

    // Can also calculate min and max automatically (but would rather not).
    if (minx == -1 || maxx == -1) {
        bool findminx = true;
        bool findmaxx = true;
        // For the first loop, find the min and max values
        for (int entryNumber = 0; entryNumber < t->GetEntries(); entryNumber++) {
            for (int k=0;k<7;k++) {
                t->GetEntry(entryNumber);
                integrated_val = 0;
                // Get Pedastal
                pedestal = 0;
                for (int j=0;j<presamples->GetVal() && j < take;j++) { // Start taking a pedestal
                    pedestal += channel[k][j];
                }
                pedestal /= take;
                // Integrate
                for (int j=presamples->GetVal();j<eh.Samples;j++) {
                    integrated_val += channel[k][j]-pedestal;
                }
                if (findminx == true && integrated_val < minx) minx = integrated_val;
                if (findmaxx == true && integrated_val > maxx) maxx = integrated_val;
            }
        }
    }

    TCanvas *c1 = new TCanvas("c1","Energy Histogram", eh.Samples, 10, 900, 700);
    TH1F *th[7];


    // TODO: improve pedestal check with sigma check etc.
    for (int k=0;k<7;k++) {
        char ChannelName[16];
        sprintf(ChannelName, "Channel %i", k);
        th[k] = new TH1F("th", ChannelName, nbins, minx, maxx);
        th[k]->SetLineColor(k+1);
        th[k]->SetLineWidth(2);
    }
        fprintf(LogFile, "EVENT\tCH\tINT\tPED\tENDPED\tCHNG\tSTD\tCLAMPED\tNEGATIVE\n");

        bool isBadEvent = false;
        uint32_t goodEventsCount = 0;
        uint32_t badEventsCount = 0;
        uint32_t uglyEventsCount = 0;
        uint32_t totalEventsCount = 0;

        const int NOISE_TOLERANCE = 10;

    for (int entryNumber = 0; entryNumber < t->GetEntries(); entryNumber++) {
        for (int k=0;k<7;k++) {
            t->GetEntry(entryNumber);
            integrated_val = 0;
                        int negativeCount = 0;
            // Get Pedestal
            uint32_t pedestal = 0, end_pedestal = 0;
            for (int j=0;j<take;j++) {
                pedestal += channel[k][j];
                end_pedestal += channel[k][eh.Samples - j - 1];
            }
            pedestal /= take; 
            end_pedestal /= take; 

                        //calculate standard deviation
                        uint32_t pedestalVarianceSum = 0;
                        float pedestalSTD = 0, pedestalChange = 0;
                        for (int j=0;j<take;j++) {
                pedestalVarianceSum += (channel[k][j] - pedestal)*(channel[k][j] - pedestal);
            }
                        pedestalSTD = sqrt((float)pedestalVarianceSum / (take + 1));
                        pedestalChange = ((float)end_pedestal-(float)pedestal)/pedestal;

                        //check CLAMPING
                        uint32_t ClampedValues = 0;
                        for (int j=0;j<eh.Samples;j++) {
                                if (channel[k][j] > 16380) ClampedValues++;
                                if (channel[k][j] < pedestal - NOISE_TOLERANCE) negativeCount++;
                        }

                        if (ClampedValues > 10 || abs(pedestalChange) > 0.001f || pedestalSTD > (float)NOISE_TOLERANCE) {
                                isBadEvent = true;
                                badEventsCount++;
                        } else {
                                isBadEvent = false;
                                goodEventsCount++;
                        }

            // Integrate
            for (int sampleNumber=presamples->GetVal();sampleNumber<eh.Samples;sampleNumber++) {
                integrated_val += channel[k][sampleNumber] - pedestal;
            }
                        
                        fprintf(LogFile, "%i\t%i\t%i\t%i\t%i\t%f\t%f\t%i\t%i", entryNumber, k, integrated_val, pedestal, end_pedestal, pedestalChange, pedestalSTD, ClampedValues, negativeCount);
                        
                        if (isBadEvent == false && integrated_val < 0) {
                                uglyEventsCount++;
                                fprintf(LogFile, "\t UGLY\n");
                        } else if (isBadEvent) {
                                fprintf(LogFile, "\t BAD\n");
                        } else {
                                fprintf(LogFile, "\t GOOD\n");
                        }

                        if (onlyGoodEvents == 0 || isBadEvent == false)  {
                                th[k]->Fill(integrated_val); // Perhaps integrator has to be an int for this to work?
                        }
                        totalEventsCount++;
        }
    }

        fprintf(LogFile, "\n\n\nGOOD = all checks fulfilled\nBAD = at least one not fulfilled\nUGLY = all checks fulfilled, but still eg. negative energy\n\n");
        fprintf(LogFile, "TOTAL:%d\nGOOD:%d %f %%\nBAD:%d %f %%\nUGLY:%d %f %%\n", totalEventsCount, goodEventsCount, (float)goodEventsCount/totalEventsCount*100, badEventsCount, (float)badEventsCount/totalEventsCount*100, uglyEventsCount, (float)uglyEventsCount/totalEventsCount*100);

        printf("\n\n\nGOOD = all checks fulfilled\nBAD = at least one not fulfilled\nUGLY = all checks fulfilled, but still eg. negative energy\n\n");
        printf("TOTAL:%d\nGOOD:%d %f %%\nBAD:%d %f %%\nUGLY:%d %f %%\n", totalEventsCount, goodEventsCount, (float)goodEventsCount/totalEventsCount*100, badEventsCount, (float)badEventsCount/totalEventsCount*100, uglyEventsCount, (float)uglyEventsCount/totalEventsCount*100);

    char hsn[128];
        // Histogram title
    sprintf(hsn, "Energy Histograms for Run %i", rn->GetVal());
        // This allows us to display multiple histograms on the same chart.
    THStack *s = new THStack("histstack", hsn);
    for (int k=0;k<l;k++) { // Only plot those passed to the function in the variable displayChannels
        s->Add(th[displayChannelsArr[k]]); // Add each histogram to the histogram stack
    }
    s->Draw("nostack"); // Draw the stack, but superimpose them: don't add them together
    // Note: this hasn't been tested -- might break compilation!!!
    char nt[256];
    sprintf(nt, "Counts / (%d nVs)", (maxx-minx)/nbins);
    s->GetYaxis()->SetTitle(nt);
    s->GetXaxis()->SetTitle("Area [nVs]"); // Is it really nVs??
    c1->BuildLegend(0.85,0.85,1,1);
    c1->Update();

        // Save the histogram in a ROOT file. Also, optionally as an image, eg png, just uncomment the relevant stuff.
    //c1->SaveAs(histogramFileName);
    c1->SaveAs(histogramROOTFileName);
    printf("\n\nHistgoram stored in %s.\n", histogramROOTFileName);

        // Suppress ROOT's otherwise-incessant whining.
    input->Close();
        fclose(LogFile);

    return 0;
}


int _tmain(int argc, _TCHAR* argv[])
{
        if (argc == 1) {
                int RunNumber,Bins=500,Take=24,Minx=-1,Maxx=-1,OnlyGoodEvents=0;
                char input[100];
                char Channels[100];

                printf("Run number>");
                memset(input, 0, 100);
                fgets(input, 100, stdin);
                if (strlen(input) < 2) {
                        return -1;
                }
                RunNumber = atoi(input);

                printf("Bins>");
                memset(input, 0, 100);
                fgets(input, 100, stdin);
                Bins = atoi(input);
                if (strlen(input) < 2) Bins = 500; //there is always at least the \n character in the input

                printf("Channel>");
                memset(input, 0, 100);
                fgets(input, 100, stdin);
                if (strlen(input) < 2) {
                        strcpy(Channels, "0123456");
                } else {
                        strcpy(Channels, input);
                }

                printf("Samples for pedestal>");
                memset(input, 0, 100);
                fgets(input, 100, stdin);
                Take = atoi(input);
                if (strlen(input) < 2) Take = 24;

                printf("Min>");
                memset(input, 0, 100);
                fgets(input, 100, stdin);
                Minx = atoi(input);
                if (strlen(input) < 2) Minx = -1;

                printf("Max>");
                memset(input, 0, 100);
                fgets(input, 100, stdin);
                Maxx = atoi(input);
                if (strlen(input) < 2) Maxx = -1;

                printf("Only good events (1)>");
                memset(input, 0, 100);
                fgets(input, 100, stdin);
                OnlyGoodEvents = atoi(input);
                if (strlen(input) < 2) OnlyGoodEvents = 0;

                fflush(stdin);
                makehist(RunNumber,Bins,Channels,Take,Minx,Maxx,OnlyGoodEvents);
                printf("[Press any key to continue]\n");
                getchar();
        }
        return 0;
}

    Blog
    Support
    Plans & pricing
    Documentation
    API
    Server status
    Version info
    Terms of service
    Privacy policy

    JIRA
    Confluence
    Bamboo
    Stash
    SourceTree

Atlassian
