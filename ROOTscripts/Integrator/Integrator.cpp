#include <cstdio>
#include <cstdlib>
#include <stdio.h>

#ifndef _MSC_VER
    typedef char _TCHAR;
#else
    #if (_MSC_VER >= 1700)
        #include <tchar.h>
    #else
        typedef char _TCHAR;
    #endif
#endif

#include <TTree.h>
#include <TFile.h>
#include <TParameter.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <THStack.h>

//#include <cstdint>

typedef signed char        int8_t;
typedef short              int16_t;
typedef int                int32_t;
//typedef long long          int64_t;
typedef unsigned char      uint8_t;
typedef unsigned short     uint16_t;
typedef unsigned int       uint32_t;
typedef unsigned long long uint64_t;

typedef struct eventHeaderStruct {
    uint32_t Number;
    uint32_t Type;
    uint32_t Time;
    uint32_t reserved;
    uint32_t FPGAEvent;
    uint32_t FPGATime;
    uint32_t Channels;
    uint32_t Samples;
} EventHeader;

int Integrator(const char* filename = "none", int nbins = 50, int take = -1, int minx = -1, int maxx = -1, const char includeChannels[] = "0123456")
{
    // This is for choosing the channels to display.
    int l = 0;
    int displayChannelsArr[7] = {-1};
    for (int k=0;k<7;k++) {
        for (int i=0; i<strlen(includeChannels); i++) {
            if(includeChannels[i] - '0' == k) {
                //printf("\nDisplaying %i.\n", k);
                displayChannelsArr[l++] = k;
            }
        }
    }

    if (filename == "none") {
        printf("Integrator integrates events for each channel of each run and plots them on one histogram.\n\n" \
               "Syntax:\n\nIntegrator(str input_ROOTfile_filename, int nbins = 50, int samplesForPedestal = -1, int minx = -1, int maxx = -1, str includeChannels)\n\n-1 means calculate automatically from data.\n\nIncludeChannels allows control over which " \
               "channels to display in the histogram, e.g. \"014\" for channels 0, 1, and 4 only. Defaults to displaying all channels.\n\n");
        return 1;
    }
    TFile* input;
    input = new TFile(filename, "READ");
    TTree* t = (TTree*)input->Get("Tree");
    if (t==0) return -6;

    printf("%i entries\n",t->GetEntries());

    EventHeader eh;
    t->SetBranchAddress("EventNumber", &eh.Number);
    t->SetBranchAddress("Type", &eh.Type);
    t->SetBranchAddress("EventTime", &eh.Time);
    t->SetBranchAddress("FPGANumber", &eh.FPGAEvent);
    t->SetBranchAddress("FPGATime", &eh.FPGATime);
    t->SetBranchAddress("Channels", &eh.Channels);
    t->SetBranchAddress("Samples", &eh.Samples);

    uint16_t channel[7][2048];
    t->SetBranchAddress("Channel0", channel[0]);
    t->SetBranchAddress("Channel1", channel[1]);
    t->SetBranchAddress("Channel2", channel[2]);
    t->SetBranchAddress("Channel3", channel[3]);
    t->SetBranchAddress("Channel4", channel[4]);
    t->SetBranchAddress("Channel5", channel[5]);
    t->SetBranchAddress("Channel6", channel[6]);

    TParameter<int> *rn = (TParameter<int>*) t->GetUserInfo()->FindObject("RunNumber");
    printf("Run number: %d\n", rn->GetVal());

    TParameter<int> *presamples = (TParameter<int>*) t->GetUserInfo()->FindObject("Presamples");
    printf("PS: %d\n", presamples->GetVal());

    // Take is the number of samples to take for pedestal. This needs tweaking, it seems to have a sizeable effect!
    // Calculate it
    if (take==-1) {
        // i.e. generate dynamically
        take = presamples->GetVal() - 2; // Skip the first two samples
    }

    t->GetEntry(0);


    signed int integrated_val; // Save into a ROOT file. Cannot be of type other than unsigned int.
    uint32_t pedestal;

    // Can also calculate min and max automatically (but would rather not).
    if (minx == -1 || maxx == -1) {
        bool findminx = true;
        bool findmaxx = true;
        // For the first loop, find the min and max values
        for (int i = 0; i < t->GetEntries(); i++) {
            for (int k=0;k<7;k++) {
                t->GetEntry(i);
                integrated_val = 0;
                // Get Pedastal
                pedestal = 0;
                for (int j=2;j<presamples->GetVal() && j < take + 2;j++) { // Start taking a pedestal after first 2 samples.
                    pedestal += channel[k][j];
                }
                pedestal /= take;
                // Integrate
                for (int j=presamples->GetVal();j<eh.Samples;j++) {
                    integrated_val += channel[k][j]-pedestal;
                }
                if (findminx == true && integrated_val < minx) minx = integrated_val;
                if (findmaxx == true && integrated_val > maxx) maxx = integrated_val;
            }
        }
    }
    printf("\nSo we have minx=%i and maxx=%i.\n", minx, maxx);
    TCanvas *c1 = new TCanvas("c1","Energy Histogram",eh.Samples,10,900,700);
    TH1F *th[7];
    // TODO: improve pedestal check with sigma check etc. (possibly use function parameters)
    for (int k=0;k<7;k++) {
        char channeln[16];
        sprintf(channeln, "Channel %i", k);
        th[k] = new TH1F("th", channeln, nbins, minx, maxx);
        th[k]->SetLineColor(k+1);
        th[k]->SetLineWidth(2);
    }
    for (int i = 0; i < t->GetEntries(); i++) {
        //printf("\nEvent number: %i", i);//eh.Number);
        for (int k=0;k<7;k++) {
            t->GetEntry(i);
            integrated_val = 0;
            // Get Pedastal
            uint32_t pedestal;
            pedestal = 0;
            for (int j=2;j<presamples->GetVal() && j < take + 2;j++) { // Start taking a pedestal after first 2 samples.
                pedestal += channel[k][j];
            }
            pedestal /= take;
            // Integrate
            for (int j=presamples->GetVal();j<eh.Samples;j++) {
                integrated_val += channel[k][j]-pedestal;
            }
            th[k]->Fill(integrated_val); // Perhaps integrator has to be an int for this to work?
            //printf("\nIntegrated value: %i at %i samples for channel %i", integrator, eh.Samples, k);
        }

    }
    char hsn[128];
    sprintf(hsn, "Energy Histograms for Run %i", rn->GetVal());
    THStack *s = new THStack("histstack", hsn);
    for (int k=0;k<l;k++) { // Accept which to plot as function args
        s->Add(th[displayChannelsArr[k]]);
    }
    s->Draw("nostack");
    s->GetYaxis()->SetTitle("Count");
    s->GetXaxis()->SetTitle("Area (nVs)"); // Is it really nVs?
    c1->BuildLegend(0.85,0.85,1,1);
    c1->Update();

    input->Close();

    return 0;
}
