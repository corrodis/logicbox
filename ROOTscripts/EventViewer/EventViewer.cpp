#include <cstdio>
#include <cstdlib>
#include <stdio.h>

#ifndef _MSC_VER
    typedef char _TCHAR;
#else
    #if (_MSC_VER >= 1700)
        #include <tchar.h>
    #else
        typedef char _TCHAR;
    #endif
#endif

#include <TTree.h>
#include <TFile.h>
#include <TParameter.h>
#include <TMath.h>
#include <TGraph.h>
#include <TMultiGraph.h>
#include <TCanvas.h>
#include <TH2F.h>

#include "../common.h"

int EventViewer(const int runnum = -1)
{
    if (runnum < 0) {
        printf("EventViewer shows plots of all signals in each event of run #runnum.\n\n");
        return 1;
    }


    FILE* datainput;
    char datafiledir[256]; // No trailing slash!
    const char* configfilename = "makehist.cfg";

    // Take data files directory from a local config file
    FILE* configfile;
    configfile = fopen(configfilename,"r");
    if (configfile == NULL) {
        printf("Enter data file directory >");
        gets(datafiledir);
        configfile = fopen(configfilename, "w+");
        fwrite(datafiledir,255,1,configfile);
        fclose(configfile);
    } else {
        fread(datafiledir,255,1,configfile);
        fclose(configfile);
    }


    // Convert runnum to filename
    char filename[256];
    char ROOTFileName[256];
    //char histogramFileName[256];
    char histogramROOTFileName[256];
    uint32_t timeval = time(0);
    sprintf(filename, "%s/psi13_%06d.bin", datafiledir, runnum);
    sprintf(ROOTFileName,"%s/psi13_%06d.root", datafiledir, runnum);

    //sprintf(histogramFileName,"%s/makehist_%06i_%i.png", datafiledir, runnum, timeval);
    sprintf(histogramROOTFileName,"%s/makehist_%06i_%i.root", datafiledir, runnum, timeval);

    bool stopSkipOverFirstEvent = true;



    // Check if ROOT file already exists, if it does skip the storage of the raw data.
    if(0){// access( ROOTFileName, F_OK ) == -1 ) {
        // Data file doesn't exist.

        datainput = fopen(filename,"rb");
        if (datainput==NULL) {fputs ("Couldn't open data file. Are you sure it actually exists?\n",stderr); return -8;}

        FileHeader fh;
        fread((void*)&fh,sizeof(FileHeader),1,datainput);
        printf("size = %d, RunNumber = %i, SamplingFrequency = %i, Presamples = %i", sizeof(FileHeader), fh.RunNumber , fh.SamplingFrequency, fh.Presamples);

        EventHeader eh; // Event header.
        uint16_t * eb; // Event buffer.
        unsigned int elements;

        TFile *ROOTFile = new TFile(ROOTFileName, "RECREATE");
        TTree *T = new TTree("Tree","Run");

        TParameter<int> RunNumber("RunNumber", fh.RunNumber);
        T->GetUserInfo()->Add(&RunNumber);
        TParameter<int> Presamples("Presamples", fh.Presamples);
        T->GetUserInfo()->Add(&Presamples);
        TParameter<int> SamplingFrequency("SamplingFrequency", fh.SamplingFrequency);
        T->GetUserInfo()->Add(&SamplingFrequency);

        // Get information from first event and reset position in file
        uint32_t Samples, Channels;
        // Read in the event header
        fread((void*)&eh,sizeof(EventHeader),1,datainput);
        Samples = eh.Samples;
        Channels = eh.Channels;
        printf("\nChannels %d, Samples %d",Channels,Samples);
        rewind(datainput);
        // Read in the file header
        fread((void*)&fh,sizeof(FileHeader),1,datainput);

        // Defining the event branch
        T->Branch("EventNumber",&eh.Number,"EventNumber/i");
        T->Branch("EventTime",&eh.Time,"Time/i");
        T->Branch("FPGANumber",&eh.FPGAEvent,"FPGANumber/i");
        T->Branch("FPGATime",&eh.FPGATime,"FPGATime/i");
        T->Branch("Type",&eh.Type,"Type/i");
        T->Branch("Channels",&eh.Channels,"Channels/i");
        T->Branch("Samples",&eh.Samples,"Samples/i");

        char channelname[256],channelname_t[256];
        eb = new uint16_t[Samples * Channels];
        for (unsigned int i=0;i<Channels;i++) {
            sprintf(channelname, "Channel%d",i);
            sprintf(channelname_t, "Channel%d[Samples]/s",i);
            // Creating a branch for each channel to store its event data
            T->Branch(channelname, eb + i * Samples, channelname_t);
        }

        while(!feof(datainput)) {
            elements = fread((void*)&eh,sizeof(EventHeader),1,datainput);
            if (elements > 0) {
                // Read the binary data directly into memory, structured with the event structure
                fread((void*)eb, Samples * Channels * sizeof(uint16_t), 1, datainput);
                // Populate the tree.
                T->Fill(); // Segmentation violation here!
            }
        }

        fclose(datainput);
        ROOTFile->Write();
        ROOTFile->Close();

        printf("\n\nRaw data stored in ROOT file '%s'\n", ROOTFileName);
    }else{
        printf("\nROOT file already exists...Skipping.\n\n");
    }

    bool alreadySavedImg = false;
    TFile* input;
    input = new TFile(ROOTFileName, "READ");
    TTree* t = (TTree*)input->Get("Tree");
    if (t==0) { return -6; }

    printf("%i entries\n",t->GetEntries());

    EventHeader eh;
    t->SetBranchAddress("EventNumber", &eh.Number);
    t->SetBranchAddress("Type", &eh.Type);
    t->SetBranchAddress("EventTime", &eh.Time);
    t->SetBranchAddress("FPGANumber", &eh.FPGAEvent);
    t->SetBranchAddress("FPGATime", &eh.FPGATime);
    t->SetBranchAddress("Channels", &eh.Channels);
    t->SetBranchAddress("Samples", &eh.Samples);

    uint16_t channel[7][2048];
    t->SetBranchAddress("Channel0", channel[0]);
    t->SetBranchAddress("Channel1", channel[1]);
    t->SetBranchAddress("Channel2", channel[2]);
    t->SetBranchAddress("Channel3", channel[3]);
    t->SetBranchAddress("Channel4", channel[4]);
    t->SetBranchAddress("Channel5", channel[5]);
    t->SetBranchAddress("Channel6", channel[6]);

    double xv[2048];
    double yv[2048];
    for (int i = 0; i<2048; i++) {
        xv[i] = i;
    }
    t->GetEntry(0);
    char canvasTitle[64];
    sprintf(canvasTitle, "EventViewer, Run #%d.", runnum);
    TCanvas *c1 = new TCanvas("c1",canvasTitle,eh.Samples,10,900,700);
    //TH2F *th = new TH2F("TH2F", "TH2F Title", 200, -0.5, eh.Samples-0.5, 200, 0, 17000);


    printf("Cycling through events.\n\nOptions:\ns: Save signal trace.\nt: Skip to event.\nb: Break and close.\n\n");
    char lastchar;


	bool isBadEvent = false, isUglyEvent = false;
	uint32_t goodEventsCount = 0;
	uint32_t badEventsCount = 0;
	uint32_t uglyEventsCount = 0;
	uint32_t totalEventsCount = 0;

    const int NOISE_TOLERANCE = 10;
	
	int signalStart = 0;
    uint32_t startPedestal = 0, endPedestal = 0;
	uint32_t pedestalVarianceSum = 0;
	uint32_t pedestalVariation = 0;
	uint32_t pedestalMaxVariation = 0;
	float pedestalSTD = 0, pedestalChange = 0;
	int negativeCount = 0;

    int take = 40;
    int presamples = 200;
    int integrated_val = 0;

    // For checking for cut-off
    int minSame = 4; // The minimum number of values to be the same.
    int dynSame = 80; // Divide samples/dynSame and use that if > minSame.
    int useSame = (int)eh.Samples / dynSame;
    int sameRange = 1.01; // To ensure best results (fewer false flags), have high dynSame, low (~1.01) sameRage.

    int lastNSamples[2056];
    int lastNSamplesPointer = 0;

    for (int i = 0; i < t->GetEntries(); i++) {
		 t->GetEntry(i);
        char mgtitle[32];
        //th->SetTitle(thtitle);
        sprintf(mgtitle, "Event %i", i);
        TMultiGraph * mg = new TMultiGraph(mgtitle, mgtitle);
        c1->Clear();
        printf("Event number: %i", i);
        TGraph *tgs[7];
        char tgsTitles[7][32];
        for (int k=0;k<7;k++) {
          

            integrated_val = 0;
			negativeCount = 0;
			signalStart = 0;
			pedestalSTD = 0;
			pedestalChange = 0;
			startPedestal = 0;
			endPedestal = 0;
			pedestalMaxVariation = 0;
			pedestalVarianceSum = 0;

            // Check CLAMPING
			if (take == -2) { //fixed
				startPedestal = fixed_pedestal[k];
				endPedestal = fixed_pedestal[k];
			} else { //take first n samples
				for (int j=0;j<take;j++) {
					startPedestal += channel[k][j];
					endPedestal += channel[k][eh.Samples - j - 1];
				}
				startPedestal /= take; 
				endPedestal /= take; 

				//calculate standard deviation
				for (int j=0;j<take;j++) {
					pedestalVariation = (channel[k][j] - startPedestal);
					if (abs((int)pedestalVariation) > pedestalMaxVariation) {
						pedestalMaxVariation = abs((int)pedestalVariation);
					}
					pedestalVarianceSum += pedestalVariation * pedestalVariation;
				}
				pedestalSTD = sqrt((float)pedestalVarianceSum / (take + 1));
				pedestalChange = ((float)endPedestal-(float)startPedestal)/startPedestal;
			}

			//check CLAMPING and SIGNAL START (signal > 110% of pedestal)
			uint32_t ClampedValues = 0;
			uint32_t maximum = 0;
			uint32_t maxbin =0;
			for (int j=0;j<eh.Samples;j++) {
				if (channel[k][j] > 16350) ClampedValues++;
				if (channel[k][j] < startPedestal - NOISE_TOLERANCE) negativeCount++;
				if (channel[k][j] > maximum){ 
					maximum = channel[k][j];
					maxbin = j;
				}
				if (signalStart == 0 && channel[k][j] > startPedestal * 1.1f) signalStart = j;
			}
			if (ClampedValues > 10 || abs(pedestalChange) > 0.001f || pedestalSTD > (float)NOISE_TOLERANCE) {
				isBadEvent = true;
				badEventsCount++;
			} else {
				isBadEvent = false;
				goodEventsCount++;
			}

            // Check cut-off
            bool cutoff = false;
            lastNSamplesPointer = 0;
            for (int sampleNumber = 0; sampleNumber < eh.Samples; sampleNumber++) {
                lastNSamples[lastNSamplesPointer++] = channel[k][sampleNumber];
                if (lastNSamplesPointer == useSame) lastNSamplesPointer = 0;
                if (sampleNumber >= useSame) {
                    int sum = 0;
                    int lnsp = 0;
                    while (lnsp != useSame) sum += lastNSamples[lnsp++];
                    float eta = sum / useSame;
                    if (eta <= sameRange*channel[k][sampleNumber] && eta >= channel[k][sampleNumber]/sameRange && eta / startPedestal > 1.6) {isBadEvent = true;cutoff=true;}
                }
            }

            // Integrate
            //for (int sampleNumber = presamples; sampleNumber < eh.Samples; sampleNumber++) {
            //    integrated_val += channel[k][sampleNumber] - pedestal;
            //}
			uint32_t maxheight = maximum -startPedestal;
			uint32_t index = maxbin;
			uint32_t nintegralsamples = 0;
			uint32_t startpoint = 0;
			uint32_t endpoint =0;
			while(index > 0 && channel[k][index]-startPedestal > maxheight * 0.1){
				integrated_val += channel[k][sampleNumber] - startPedestal;
				nintegralsamples++;
				index--;
				startpoint = index;
			}

			index = maxbin+1;
			while(index < eh.Samples && channel[k][index]-startPedestal > maxheight * 0.1){
				integrated_val += channel[k][sampleNumber] - startPedestal;
				nintegralsamples++;
				index++;
				endpoint = index;
			}

			


            if (isBadEvent == false && integrated_val < 0) {
                uglyEventsCount++;
                isUglyEvent = true;
            } else if (isBadEvent) {
                isUglyEvent = false;
            } else {
                isUglyEvent = false;
            }

            totalEventsCount++;
            char gbu = 'g';
            if (isBadEvent) gbu = 'b'; else if (isUglyEvent) gbu = 'u';
            printf("\nIntegrated value ch. %d: %i; pedestal: %d; endpedestal: %d; (%c)\n", k, integrated_val, startPedestal, endPedestal, gbu);
            if (cutoff) printf(" -- Channel %i: cut-off error! (%i samples).", k, useSame);
            // -----

			printf("Integral %i with %i samples from %i to %i\n",integrated_val, nintegralsamples, startpoint, endpoint);

            for (int j=0;j<eh.Samples;j++) {
                yv[j] = channel[k][j];//+k*channel[k][j]*0.1;
            }
            tgs[k] = new TGraph(eh.Samples, xv, yv);
            tgs[k]->SetLineColor(k+1);
            tgs[k]->SetDrawOption("AL"); // Drawing options: http://root.cern.ch/root/html/TGraphPainter.html
            tgs[k]->SetLineWidth(2);
            sprintf(tgsTitles[k], "Channel %i", k);
            tgs[k]->SetTitle(tgsTitles[k]);
            mg->Add(tgs[k]);
            //tgs[k]->Draw();
        }
        printf("\n");
        //th->Draw();
        //mg->GetXaxis()->SetTitle("Sample No.");
        //mg->GetYaxis()->SetTitle("Amplitude");
        mg->Draw("AL");
        c1->BuildLegend(0.85,0.85,1,1);
        c1->Update();
		printf("Updating?\n");
        char option = getchar();
        if (option == '\n' && lastchar != '\n' || stopSkipOverFirstEvent) { option = getchar(); stopSkipOverFirstEvent = false; }
        if (option=='b') {
            printf("Breaking...");
            break;
        }else if (option == 's') {
            // Save
            //char oimgfname[256];
            //sprintf(oimgfname, "hist_%i_entry%i.png", eh.FPGATime, i);
            char oROOTfname[256];
            sprintf(oROOTfname, "%s/run_%i_event_%i.root", datafiledir, runnum, i);
            c1->SaveAs(oROOTfname);
            printf("Saved event as '%s'.\n", oROOTfname);
            getchar();getchar();lastchar='\n';
        }else if (option == 't') {
            i--;
            int n;
            printf("Which event number do you want to skip to? ");
            scanf("%d",&n);
            if (i < t->GetEntries()) {
                i = n-1;
            }else{
                printf("That's higher than the number of events we have in this run.\n");
            }
        }
        lastchar = option;

    }

    input->Close();

    return 0;
}
